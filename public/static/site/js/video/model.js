/**
 * Model层
 *
 **/

'use strict';

var model = {};

(function(context,$) {

    'use strict';

    var root = context;
    var taskCommon = root.taskCommon = function() {};

    $.extend(taskCommon.prototype, {
        //静态模板
        Tpl: '',
        // 数据仓库
        Store: {},
        //执行任务队列
        task: {},
        //初始化
        init: function() {},
        //添加任务队列
        // @param {String} taskName 任务名称
        // @param {Function} callback 回调函数
        addTask: function(taskName, callback) {
            var calls = this.task || (this.task = {});
            (this.task[taskName] || (this.task[taskName] = [] ) ).push(callback);
            return this;
        },
        //执行队列
        triggerTask: function() {
            var args = Array.prototype.slice.call(arguments, 0);
            //拿出第一个参数，即事件名称
            var taskName = args.shift();

            var list,calls,i,l;
            if(!(calls = this.task)) return this;
            if(!(list = this.task[taskName])) return this;

            for(i = 0,l = list.length; i < l; i++ ){
                list[i].apply(this,args);
            }
            return this;
        },
        //批量执行任务
        // @param {Array} array 执行队列
        runSequence: function(array) {
            if($.isArray(array)) {
                if(array.length) {
                    for(var i=0; i< array.length; i++) {
                        this.triggerTask(array[i]);
                    }
                }
            } else {
                throw "参数必须是数组";
            }
        },
        //移除某个任务队列
        //@param {Number} index 要删除的队列下标
        removeTask: function(index) {
            this.task.splice(index,1);
        },
        /**
         *@method ajax
         *@parame command
         *@parame suffix
         *@parame type
         *@parame data
         *@parame dataType
         *@parame successCallBack
         *@parame errorCallBack
         *@parame context
         **/
        ajax : function(url, type, data, dataType, successCallBack, errorCallBack, context, timeOut) {
            context = context || null;
            var defer = $.Deferred();
            $.ajax({
                timeout: timeOut || 15000,
                url: url,
                type: type || 'post',
                data: data,
                dataType: dataType || 'json',
                beforeSend:function(xhr){
                    if(csrftoken){
                        xhr.setRequestHeader('csrftoken', csrftoken);
                    }
                },
                success:function (response, status) {
                    successCallBack && successCallBack.call(context, response, status);
                    var options = {
                        response : response,
                        status : status
                    };
                    defer.resolve(options);
                },
                error:function (xhr, status, error) {
                    errorCallBack && errorCallBack.call(context, xhr, status, error);
                    defer.resolve(undefined);
                }
            });

            var promise = defer.promise();
            return promise;
        },
        //发布/订阅(Pub/Sub)模式
        on: function(ev, callback) {
            var calls = this._callbacks || (this._callbacks = {});
            (this._callbacks[ev] || (this._callbacks[ev] = [] ) ).push(callback);
            return this;
        },
        trigger: function() {
            var args = Array.prototype.slice.call(arguments, 0);
            var ev = args.shift();

            var list,calls,i,l;
            if(!(calls = this._callbacks)) return this;
            if(!(list = this._callbacks[ev])) return this;

            for(i = 0,l = list.length; i < l; i++ ){
                list[i].apply(this,args);
            }
            return this;
        },
        // 获取URL中的参数值
        getQuery: function(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        },
        //遍历
        /**
         *@method each
         *@parame loopable 要遍历的对象
         *@parame callback 回调函数
         *@parame self  上下文
         **/
        each: function(loopable,callback,self) {
            var additionalArgs = Array.prototype.slice.call(arguments,3);
            if(loopable){
                if(loopable.length === +loopable.length){
                    var i;
                    for(var i=0;i<loopable.length;i++){
                        callback.apply(self,[loopable[i],i].concat(additionalArgs));
                    }
                } else {
                    for(var item in loopable){
                        callback.apply(self,[loopable[item],item].concat(additionalArgs));
                    }
                }
            }
        },
        //集合中查找，返回结果（集合）
        /**
         *@method where
         *@parame collection 集合
         *@parame filterCallback 查找方法
         *@return filtered 返回查找的集合
         **/
        where: function(collection,filterCallback) {
            var filtered = [];
            this.each(collection,function(item){
                if(filterCallback(item)){
                    filtered.push(item);
                }
            });
            return filtered;
        },
        htmlEncode:function (html){
            var temp = document.createElement ("div");
            (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
            var output = temp.innerHTML;
            temp = null;
            return output;
        },
        htmlDecode:function (text){
            var temp = document.createElement("div");
            temp.innerHTML = text;
            var output = temp.innerText || temp.textContent;
            temp = null;
            return output;
        },
        formatTime : function(time, type) {        //时间格式化 type = date 日
            var formatTime = '';
            var time = new Date(time);
            var year = time.getFullYear();
            var month = time.getMonth() + 1;
            var date = time.getDate();
            var hours = time.getHours();
            var minutes = time.getMinutes();
            var seconds = time.getSeconds();

            if(month < 10) {
                month = '0' + month;
            }
            if(date < 10) {
                date = '0' + date;
            }
            if(hours < 10 ) {
                hours = '0' + hours;
            }
            if(minutes < 10) {
                minutes = '0' + minutes;
            }
            if(seconds < 10) {
                seconds = '0' + seconds;
            }
            if(type === 'date'){ //精确到日
                formatTime = year + '-' + month + '-' + date;
            }else if(type === 'month'){
                formatTime = year + '-' + month;
            }else{  //精确到s
                formatTime = year + '-' + month + '-' + date + ' ' + hours + ':' + minutes + ':' + seconds;
            }
            return formatTime;
        }
    });
})(model,$);

// 模板
model.template = function( tmp, config) {
    return tmp.replace(/\$\{?([A-Za-z_0-9]+)\}?/g, function($1,$2){ return (config[$2] != undefined ? config[$2] : '') });
};

model.url = {

};

/**
 * @class ajax
 * @parame {obj} {obj.action} {obj.method} {obj.data} {obj.context} {obj.callback} {obj.timeOut}
 * @return {obj} parmise
 **/
model.ajax = function(obj) {
    var parame = {
        action: '',
        method: 'post',
        data: '',
        context: null,
        callback: function() {},
        timeOut: 15000
    };
    var url = this.url[obj.action];
    if(url) {

    } else {
        throw '请求的接口未注册';
    }
};


/**
 * 执行 model.DEBUG() 开启调试
 * 执行 model.DEBUG(0) 关闭调试
 * @param close {boolean}
 *
 */
model.DEBUG = function(num){
    if(0 === num){
        $.removeCookie('DEBUG', { path: '/' });
        return '关闭调试';
    }else{
        $.cookie('DEBUG', 'true', {path: '/'});
        return '开启调试';
    }
};

/**
 * dclog数据统计
 */
model.dclog = function(opt){
    var dclogUrl = 'http://log.ys7.com/statistics.do';
    var params = '?';
    if(typeof opt === 'object'){
        for(var i in opt){
            params += i + '=' + opt[i] + '&';
        }
        params = params.replace(/&$/, '');
    }else if(typeof opt === 'string'){
        params += opt;
    }else{
        return false;
    }
    var img = new Image();
    img.src = dclogUrl + params;
    return true;
};

/**
 * 获取url参数
 */
model.getQueryString = function(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]); return null;
};

/**
 * 全局ajax请求头添加csrftoken字段
 */
if(csrftoken){
    console.log('csrftoken:' + csrftoken);
    $.ajaxSetup({
        beforeSend: function(xhr){
            xhr.setRequestHeader('csrftoken', csrftoken);
        }
    })
}

window.model = model;