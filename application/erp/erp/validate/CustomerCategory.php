<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class CustomerCategory extends Validate
{
    //定义验证规则
    protected $rule = [
        'customer_category_name|客户分类名称' => 'require',
//        'customer_category_code|商品编码' => 'require',
    ];
}