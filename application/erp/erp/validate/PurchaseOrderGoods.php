<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class PurchaseOrderGoods extends Validate
{
    //定义验证规则
    protected $rule = [
        'p_order_id|采购单id' => 'require',
        'goods_id|商品id' => 'require'
    ];
}