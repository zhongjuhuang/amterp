<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class PurchaseOrderGoods extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'p_goods_id';


    public static function getByOrderId($p_order_id){
        $list = self::where('p_order_id','=',$p_order_id)
            ->column('p_goods_id,p_order_id,goods_id,goods_name,goods_number,goods_spec,goods_unit,p_goods_buy_num,p_goods_price,p_goods_tax,p_goods_amount,p_goods_info');
        return $list;
    }



}