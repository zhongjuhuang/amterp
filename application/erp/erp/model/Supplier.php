<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class Supplier extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'supplier_id';

    public function category()
    {
        return $this->hasOne('SupplierCategory','supplier_category_id','supplier_category_id');
    }

    public static function getAll($where = []){
        $data = self::where($where)->column('supplier_id,supplier_category_id,supplier_name,supplier_contacts,supplier_phone,supplier_telephone');
        return array_values($data);
    }
}