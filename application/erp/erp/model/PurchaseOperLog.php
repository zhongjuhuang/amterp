<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/11
 * Time: 11:10 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class PurchaseOperLog extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'p_order_id';

    /**
     * 添加采购操作记录
     * @param $orderState
     * @param $orderId
     * @param string $time
     */
    public static function addPurchaseOperLog($orderState, $orderId, $oper, $time = '')
    {
        $log = new PurchaseOperLog();
        return $log->save(
            [
                'p_order_id' => $orderId,
                'order_state' => $orderState,
                'oper' => $oper,
                'oper_user_id'    => ADMIN_ID,
                'oper_user'      => session('admin_user.nick'),
                'oper_time'      => empty($time) ? time() : $time
            ]
        );
    }
}