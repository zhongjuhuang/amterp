<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class GoodsCategory extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'goods_category_id';



    /**
     * 将数据集格式化成父子结构
     * @param int $id 选中的ID
     * @param int $status 状态值
     * @param int $level 层级
     * @param int $data  要格式化的数据集
     * @author 橘子俊 <364666827@qq.com>
     * @return string
     */
    public static function getChilds($pid = 0,  $level = 0, $data = [])
    {
        $trees = [];
        if (empty($data)) {
            $map = [];

            $data = self::where($map)->column('goods_category_id,goods_category_name,goods_category_top_id,goods_category_code');
            $data = array_values($data);
        }

        foreach ($data as $k => $v) {
            if ($v['goods_category_top_id'] == $pid) {
                unset($data[$k]);
                $v['childs'] = self::getChilds($v['goods_category_id'], $level+1, $data);
                $trees[] = $v;
            }
        }
        return $trees;
    }

    /**
     * 将数据集格式化成下拉选项
     * @param int $id 选中的ID
     * @author zhongjuhuang
     * @return string
     */
    public static function getSelect($data = [], $id = 0, $level = 0)
    {
        if (empty($data)) {
            return '';
        }
        $str = $separ = '';
        if ($level > 0) {
            for ($i=0; $i < $level; $i++) {
                $separ .= '&nbsp;&nbsp;&nbsp;';
            }
            $separ .= '┣&nbsp;';
        }

        foreach ($data as $k => $v) {
            if ($id == $v['goods_category_id']) {
                $str .= '<option value="'.$v['goods_category_id'].'" selected>'.$separ.$v['goods_category_name'].'</option>';
            } else {
                $str .= '<option value="'.$v['goods_category_id'].'">'.$separ.$v['goods_category_name'].'</option>';
            }
            if (isset($v['childs'])) {
                $str.= self::getSelect($v['childs'], $id, $level+1);
            }
        }
        return $str;
    }
}