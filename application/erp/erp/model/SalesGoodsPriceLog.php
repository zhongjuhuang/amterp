<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/29
 * Time: 1:25 PM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class SalesGoodsPriceLog extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'price_log_id';
}