<?php
/**
 * 资金模块
 * User: zhongjuhuang
 * Date: 2020/5/27
 * Time: 3:47 PM
 */

namespace app\erp\admin;


use app\erp\model\AccountsReceivable;
use app\erp\model\FinancePayable;
use app\erp\model\FinancePayableLog;
use app\erp\model\PurchaseOrder;
use app\erp\model\PurchaseWarehouseOrder;
use app\system\admin\Admin;
use app\system\model\SystemUser;
use think\Db;

class Finance extends Admin
{
    protected $hisiModel = 'FinancePayableLog';
//    protected $hisiValidate = 'Goods';
    protected $payment_type = [
        1 => '应付账款',
        2 => '现金付款',
        3 => '预付款'
    ];//付款方式：1、应付账款2、现金付款3、预付款
    public function index(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['goods_name', 'like', '%'.$keyword.'%'];
            }
            $goods = FinancePayable::where($where)->page($page)->limit($limit)->order('payable_id','desc')->select();
            foreach($goods as &$item){
                $item['payment_type'] = $this->payment_type[$item['payment_code']];
            }
            $data['data']   = $goods;
            $data['count']  = FinancePayable::where($where)->count('payable_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    /**
     * 应付账款详情
     */
    public function detail(){
        $payable_id = $this->request->param('payable_id');
        $finance_payable = FinancePayable::with('logs')->get($payable_id);

        if($finance_payable){
            $purchase_order = PurchaseOrder::with(['goods','supplier'])->get($finance_payable['p_order_id']);
            $purchase_warehouse_order = PurchaseWarehouseOrder::get($finance_payable['warehouse_order_id']);
            //采购单//采购商品
            $this->assign('purchase_order', $purchase_order);
            //入库单
            $this->assign('purchase_warehouse_order', $purchase_warehouse_order);

            //付款记录
        }
        if($finance_payable['logs']){
            $admin_ids = array_column($finance_payable['logs']->toArray(),'admin_id');
            $admin_id_names = SystemUser::where('id','in', $admin_ids)->column('id,nick');
            foreach ($finance_payable['logs'] as &$log){
                $log['admin_name'] = $admin_id_names[$log['admin_id']];
            }
        }
        $this->assign('finance_payable', $finance_payable);
        $this->assign('payment_type', config('erp.payment_type'));
        return $this->fetch();
    }

    /**
     * 付款
     */
    public function pay(){
        $id = $this->request->param('payable_id');
        $finance_pay = FinancePayable::get($id);
        if(!$this->request->isPost()){
            if($finance_pay){
                $this->assign('finance_pay', $finance_pay);
            }
            return parent::add();
        }else{
            //1、加log记录 2、更新金额
            Db::startTrans();
            $pay_log_amount = $this->request->param('pay_log_amount');
            if($pay_log_amount > $finance_pay['payment_amount']-$finance_pay['finish_amount']){
                return $this->error('付款金额不能大于未付金额');
            }
            $rs = FinancePayable::where('payable_id','=',$id)
                ->setInc('finish_amount', $pay_log_amount);
            if(!$rs){
                Db::rollback();
                return $this->error('金额更新错误');
            }
            $postData = $this->request->post();
            $model = new FinancePayableLog();
            $rs = $model->save($postData);
            if(!$rs){
                Db::rollback();
                return $this->error($model->getError());
            }else{
                Db::commit();
                return $this->success('保存成功');
            }
        }

    }

    public function receivable(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['goods_name', 'like', '%'.$keyword.'%'];
            }
            $goods = AccountsReceivable::where($where)->page($page)->limit($limit)->order('receivable_id','desc')->select();
            $data['data']   = $goods;
            $data['count']  = AccountsReceivable::where($where)->count('payable_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }
}