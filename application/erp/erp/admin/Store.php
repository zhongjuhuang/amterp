<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:05 AM
 */

namespace app\erp\admin;


use app\erp\model\Brand;
use app\erp\model\Goods;
use app\erp\model\GoodsCategory;
use app\erp\model\PurchaseOrderGoods;
use app\erp\model\Unit;
use app\erp\model\Warehouse;
use app\erp\model\WarehouseGoods;
use app\system\admin\Admin;
use think\Db;

class Store extends Admin
{
    protected $hisiModel = 'Goods';
    protected $hisiValidate = 'Goods';
    //商品列表
    public function goods(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['goods_name', 'like', '%'.$keyword.'%'];
            }
            $goods = Goods::with(['brand','category'])->where($where)->page($page)->limit($limit)->select();
//            echo Db::getLastSql();die;
//            dump($goods);
            foreach ($goods as &$good){
                $good['brand_name'] = $good['brand']['brand_name'];
                $good['category_name'] = $good['category']['goods_category_name'];
            }
            $data['data']   = $goods;
            $data['count']  = Goods::alias('g')->where($where)->count('g.goods_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function add(){
        if (!$this->request->isPost()) {
            $category = GoodsCategory::getSelect(GoodsCategory::getChilds());
            $this->assign('category', $category);
            $units = Unit::getSelect(Unit::getAll());
            $this->assign('units', $units);
            $brands = Brand::getSelect(Brand::getAll());
            $this->assign('brands', $brands);
        }
        return parent::add();
    }

    public function edit(){
        if (!$this->request->isPost()) {
            $category = GoodsCategory::getSelect(GoodsCategory::getChilds());
            $this->assign('category', $category);
            $units = Unit::getSelect(Unit::getAll());
            $this->assign('units', $units);
            $brands = Brand::getSelect(Brand::getAll());
            $this->assign('brands', $brands);
        }
        return parent::edit();
    }
    //商品分类
    public function goodsCategory(){
        $data_list = GoodsCategory::getChilds(0, 0);

        $this->assign('data_list', $data_list);


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        // tab切换数据
        // $hisiTabData = [
        //     ['title' => '后台首页', 'url' => 'system/index/index'],
        // ];
        // current 可不传
        // $this->assign('hisiTabData', ['menu' => $hisiTabData, 'current' => 'system/index/index']);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function addGoodsCategory(){
        if (!$this->request->isPost()) {
            $category = GoodsCategory::getSelect(GoodsCategory::getChilds());
            $this->assign('category', $category);
        }
        $this->hisiModel = 'GoodsCategory';
        $this->hisiValidate = 'GoodsCategory';
        $this->template = 'goods_category_form';
        return parent::add();
    }

    public function editGoodsCategory(){
        if (!$this->request->isPost()) {
            $category = GoodsCategory::getSelect(GoodsCategory::getChilds());
            $this->assign('category', $category);
        }
        $this->hisiModel = 'GoodsCategory';
        $this->hisiValidate = 'GoodsCategory';
        $this->template = 'goods_category_form';
        return parent::edit();
    }

    public function delGoodsCategory(){
        $this->hisiModel = 'GoodsCategory';
        return parent::del();
    }

    //计量单位
    public function units(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['unit_name', 'like', '%'.$keyword.'%'];
            }
            $goods = Unit::where($where)->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = Unit::where($where)->count('unit_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function addUnit(){
        $this->hisiModel = 'Unit';
        $this->hisiValidate = 'Unit';
        $this->template = 'unit_form';
        return parent::add();
    }

    public function editUnit(){
        $this->hisiModel = 'Unit';
        $this->hisiValidate = 'Unit';
        $this->template = 'unit_form';
        return parent::edit();
    }

    public function delUnit(){
        $this->hisiModel = 'Unit';
        return parent::del();
    }
    //商品品牌
    public function brands(){

        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['brand_name', 'like', '%'.$keyword.'%'];
            }
            $goods = Brand::where($where)->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = Brand::where($where)->count('brand_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function addBrand(){
        $this->hisiModel = 'Brand';
        $this->hisiValidate = 'Brand';
        $this->template = 'brand_form';
        return parent::add();
    }

    public function editBrand(){
        $this->hisiModel = 'Brand';
        $this->hisiValidate = 'Brand';
        $this->template = 'brand_form';
        return parent::edit();
    }

    public function delBrand(){
        $this->hisiModel = 'Brand';
        return parent::del();
    }

    //仓库
    public function warehouses(){

        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['warehouse_name', 'like', '%'.$keyword.'%'];
            }
            $goods = Warehouse::where($where)->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = Warehouse::where($where)->count('warehouse_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function addWarehouse(){
        $this->hisiModel = 'Warehouse';
        $this->hisiValidate = 'Warehouse';
        $this->template = 'warehouse_form';
        return parent::add();
    }

    public function editWarehouse(){
        $this->hisiModel = 'Warehouse';
        $this->hisiValidate = 'Warehouse';
        $this->template = 'warehouse_form';
        return parent::edit();
    }

    public function delWarehouse(){
        $this->hisiModel = 'Warehouse';
        return parent::del();
    }

    /**
     * 产品仓库分布图
     */
    public function goodsWarehouse(){
        //通过产品搜仓库
        $goods_id = $this->request->param('goods_id');
        $warehouse_goods = WarehouseGoods::with('warehouse')
            ->where('goods_id','=',$goods_id)
            ->select();
        $this->assign('warehouse_goods',$warehouse_goods);
        $goods = Goods::get($goods_id);
        $this->assign('goods',$goods);
        return $this->fetch();
    }

    /**
     * 价格走势
     */
    public function priceTrend(){
        $goods_id = $this->request->param('goods_id');
        $purchase_order_goods = PurchaseOrderGoods::where('goods_id','=',$goods_id)->select();
        $this->assign('purchase_order_goods',$purchase_order_goods);
        $goods = Goods::get($goods_id);
        $this->assign('goods',$goods);
        return $this->fetch();
    }
}