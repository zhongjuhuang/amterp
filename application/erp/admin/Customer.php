<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/8
 * Time: 9:05 AM
 */

namespace app\erp\admin;


use app\erp\model\CustomerCategory;
use app\system\admin\Admin;
use app\erp\model\Customer as CustomerModel;

class Customer extends Admin
{
    protected $hisiModel = 'Customer';
    protected $hisiValidate = 'Customer';
    public function index(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['customer_name', 'like', '%'.$keyword.'%'];
            }
            $goods = CustomerModel::where($where)->page($page)->limit($limit)->select();
            foreach ($goods as &$good){
                $good['customer_category_name'] = $good['category']['customer_category_name'];
            }
            $data['data']   = $goods;
            $data['count']  = CustomerModel::where($where)->count('customer_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function add(){
        if (!$this->request->isPost()) {
            $category = CustomerCategory::getSelect(CustomerCategory::getAll());
            $this->assign('category', $category);
        }
        return parent::add();
    }

    public function edit()
    {
        if (!$this->request->isPost()) {
            $category = CustomerCategory::getSelect(CustomerCategory::getAll());
            $this->assign('category', $category);
        }
        return parent::edit();
    }

    public function category(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['customer_category_name', 'like', '%'.$keyword.'%'];
            }
            $goods = CustomerCategory::where($where)->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = CustomerCategory::where($where)->count('customer_category_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function addCategory(){
        $this->hisiModel = 'CustomerCategory';
        $this->hisiValidate = 'CustomerCategory';
        $this->template = 'category_form';
        return parent::add();
    }

    public function editCategory(){
        $this->hisiModel = 'CustomerCategory';
        $this->hisiValidate = 'CustomerCategory';
        $this->template = 'category_form';
        return parent::edit();
    }

    public function delCategory(){
        $this->hisiModel = 'CustomerCategory';
        return parent::del();
    }
}