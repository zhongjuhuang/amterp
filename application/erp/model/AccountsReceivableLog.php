<?php
/**
 * 收款记录
 * User: zhongjuhuang
 * Date: 2020/5/27
 * Time: 3:50 PM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class AccountsReceivableLog extends Model
{
// 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'receivable_log_id';

    public function user(){
        return $this->hasOne('app\system\model\SystemUser','id','admin_id');
    }
}