<?php
/**
 * 应收账款表
 * User: zhongjuhuang
 * Date: 2020/5/27
 * Time: 3:48 PM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class AccountsReceivable extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'receivable_id';

    public function logs(){
        return $this->hasMany('AccountsReceivableLog', 'receivable_id', 'receivable_id');
    }

    public function getReceivableAmountAttr($value){
        return number_format($value,2,".","");
    }
    public function getFinishAmountAttr($value){
        return number_format($value,2,".","");
    }
}