<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class Unit extends Validate
{
    //定义验证规则
    protected $rule = [
        'unit_name|单位名称' => 'require',
    ];
}