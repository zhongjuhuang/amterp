<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class Warehouse extends Validate
{
    //定义验证规则
    protected $rule = [
        'warehouse_name|仓库名称' => 'require',
        'warehouse_code|仓库编号' => 'require',
    ];
}