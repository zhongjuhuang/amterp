<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class SupplierCategory extends Validate
{
    //定义验证规则
    protected $rule = [
        'supplier_category_name|供应商名称' => 'require',
//        'supplier_category_code|商品编码' => 'require',
    ];
}