<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class Goods extends Validate
{
    //定义验证规则
    protected $rule = [
        'goods_name|商品名称' => 'require',
        'goods_number|商品编码' => 'require',
        'goods_recommend_price|建议售价' => 'require',
        'unit_id|商品单位' => 'require|number',
        'goods_category_id|商品分类' => 'require|number',
    ];
}