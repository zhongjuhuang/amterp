<?php
/**
 * 海康摄像头的一些请求接口
 * User: zhongjuhuang
 * Date: 2020/3/3
 * Time: 10:11 AM
 */

namespace app\common\model;


//header('Content-type:text/html; Charset=utf-8');
//date_default_timezone_set('PRC');
use hisi\Http;

class Haikang
{
    public $pre_url = "http://你的ip";
    protected $app_key = "f1b0efd65fb54783aefcc8f8de178414";
    protected $app_secret = "d63a6d21e4994bf72bb35cf4bed0d5ca";
    protected $domain = "https://open.ys7.com/api/lapp";
    public $token = "";
    public $token_expire_time = 0;

    public $error;//错误提示
//    public $time;//时间戳
    public $content_type="application/json";//类型
    public $accept="*/*" ;//accept

    public $person_list_url = "/artemis/api/resource/v1/person/personList";//人员列表url

    public function __construct($app_key='', $app_secret='', $token='', $token_expire_time=0)
    {
        if($app_key!='') $this->app_key = $app_key;
        if($app_secret!='') $this->app_secret = $app_secret;
        $this->token = $token;
        $this->token_expire_time = $token_expire_time;
        $this->charset = 'utf-8';
//        list($msec, $sec) = explode(' ', microtime());
//        $this->time = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);

    }

    public function getAccessToken(){
        $token_key = 'token_data';
//        $token = Cache::get($token_key);
        $token = false;
        $current_time = getMillisecond();
        if($this->token && $this->token_expire_time>$current_time){
            $token = $this->token;
        }
        if(!$token){
            $url = $this->domain.'/token/get';
            $data = [
                'appKey' => $this->app_key,
                'appSecret' => $this->app_secret,
            ];
            $response = Http::post($url, $data);
            /*$response = Curl::to($url)
                ->withData($data)
                ->post();*/
            $response = json_decode($response,true);
            /*array:3 [▼
              "data" => array:2 [▼
                "accessToken" => "at.bo6avwg744hoxsxs9qbop8qp1nju99dn-4g2mqjzrup-1jc9oym-rnngtfni8"
                "expireTime" => 1583808434881
              ]
              "code" => "200"
              "msg" => "操作成功!"
            ]*/
            if($response['code'] == 200){
                $this->token = $response['data']['accessToken'];
                $this->token_expire_time = $response['data']['expireTime'];
//                Cache::put($token_key, $response['data'], $response['data']['expireTime']-time());
                return $this->token;
            }else{
                $this->error = $response['msg'];
                return false;
            }
        }else{
            return $token;
        }

    }

    /**
     * 获取直播视频列表
     * @param int $page_start
     * @param int $page_size
     */
    public function getLiveVideoList($page_start = 0, $page_size = 10){
        $url = $this->domain.'/live/video/list';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'pageStart' => $page_start,
            'pageSize' => $page_size,
        ];
        /*$response = Curl::to($url)
            ->withData($data)
            ->post();*/
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }
    /**
     * 获取视频列表
     * @param int $page_start
     * @param int $page_size
     */
    public function getVideoList($page_start = 0, $page_size = 10){
        $url = $this->domain.'/device/list';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'pageStart' => $page_start,
            'pageSize' => $page_size,
        ];
        /*$response = Curl::to($url)
            ->withData($data)
            ->post();*/
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }


    /**
     * 获取录像回放列表
     */
    public function getPlaybackList($device_serial, $start_time='', $end_time='', $channel_no=1, $rec_type=2){
        $url = $this->domain.'/video/by/time';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'deviceSerial' => $device_serial,
            'channelNo' => $channel_no,//通道号，非必选，默认为1
            'startTime' => $start_time,//起始时间，时间格式为：1378345128000。非必选，默认为当天0点
            'endTime' => $end_time,//结束时间，时间格式为：1378345128000。非必选，默认为当前时间
            'recType' => $rec_type,//回放源，0-系统自动选择，1-云存储，2-本地录像。非必选，默认为0
        ];
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }

    /**
     * 添加设备
     * @param $device_serial
     * @param $validate_code
     * @return bool|mixed
     */
    public function addDevice($device_serial, $validate_code){
        $url = $this->domain.'/device/add';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'deviceSerial' => $device_serial,
            'validateCode' => $validate_code,
        ];
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }

    /**
     * 删除设备
     * @param $device_serial
     * @return bool|mixed
     */
    public function delDevice($device_serial){
        $url = $this->domain.'/device/delete';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'deviceSerial' => $device_serial,
        ];
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }


    public function getDeviceInfo($device_serial, $expire_time = ''){
        $url = $this->domain.'/device/info';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'deviceSerial' => $device_serial,
        ];
        if(empty($expire_time)){
            $data['expireTime'] = $expire_time;
        }
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }

    /**
     * 关闭视频加密
     */
    public function offEncrypt($device_serial, $validate_code){
        $url = $this->domain.'/device/encrypt/off';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'deviceSerial' => $device_serial,
            'validateCode' => $validate_code,
        ];
//        dump($data);
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }
    /**
     * 打开视频加密
     */
    public function onEncrypt($device_serial){
        $url = $this->domain.'/device/encrypt/on';
        $token = $this->getAccessToken();
        if(!$token){
            return false;
        }
        $data = [
            'accessToken' => $token,
            'deviceSerial' => $device_serial,
        ];
        $response = Http::post($url, $data);
        return json_decode($response, true);
    }


}