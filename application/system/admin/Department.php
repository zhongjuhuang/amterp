<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/6/5
 * Time: 2:03 PM
 */

namespace app\system\admin;


use app\system\model\SystemDepartment;

class Department extends Admin
{
    protected $hisiModel = 'SystemDepartment';
    public function index(){
        $list = SystemDepartment::getAllChild();
        $this->assign('list', $list);
        $this->assign('hisiTabData', '');
        $this->assign('hisiTabType', 1);

        return $this->fetch();
    }

    public function add()
    {
        if (!$this->request->isPost()) {
            $category = SystemDepartment::getSelect(SystemDepartment::getAllChild());
            $this->assign('category', $category);
            $this->assign('formData', ['pid'=>$this->request->param('pid')]);
        }
        return parent::add();
    }

    public function edit()
    {
        if (!$this->request->isPost()) {
            $category = SystemDepartment::getSelect(SystemDepartment::getAllChild());
            $this->assign('category', $category);
        }
        return parent::edit();
    }

}