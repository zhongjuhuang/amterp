<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/6/5
 * Time: 2:21 PM
 */

namespace app\system\model;


use think\Model;

class SystemDepartment extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;

    /**
     * 获取指定节点下的所有子节点(不含快捷收藏的菜单)
     * @param int $pid 父ID
     * @param string $cache_tag 缓存标签名
     * @author zjh
     * @return array
     */
    public static function getAllChild($pid = 0, $field = 'id,pid,name,level', $level = 0, $data = [])
    {

        $trees = [];
        if (empty($data)) {
            $map = [];

            $data = self::where($map)->column($field);
            $data = array_values($data);
        }

        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                unset($data[$k]);
                $v['children'] = self::getAllChild($v['id'], $field,$level+1, $data);
                $trees[] = $v;
            }
        }
        return $trees;
    }

    /**
     * 将数据集格式化成下拉选项
     * @param int $id 选中的ID
     * @author zhongjuhuang
     * @return string
     */
    public static function getSelect($data = [], $id = 0, $level = 0)
    {
        if (empty($data)) {
            return '';
        }
        $str = $separ = '';
        if ($level > 0) {
            for ($i=0; $i < $level; $i++) {
                $separ .= '&nbsp;&nbsp;&nbsp;';
            }
            $separ .= '┣&nbsp;';
        }

        foreach ($data as $k => $v) {
            if ($id == $v['id']) {
                $str .= '<option value="'.$v['id'].'" selected>'.$separ.$v['name'].'</option>';
            } else {
                $str .= '<option value="'.$v['id'].'">'.$separ.$v['name'].'</option>';
            }
            if (isset($v['children'])) {
                $str.= self::getSelect($v['children'], $id, $level+1);
            }
        }
        return $str;
    }
}