<?php
// +----------------------------------------------------------------------
// | HisiPHP框架[基于ThinkPHP5.1开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2021 http://www.hisiphp.com
// +----------------------------------------------------------------------
// | HisiPHP承诺基础框架永久免费开源，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 橘子俊 <364666827@qq.com>，开发者QQ群：50304283
// +----------------------------------------------------------------------

// 为方便系统升级，二次开发中用到的公共函数请写在此文件，禁止修改common.php文件
// ===== 系统升级时此文件永远不会被覆盖 =====

/**
 *字符串转十六进制函数
 *@pream string $str='abc';
 */
 function strToHex($str){
    $hex="";
    for($i=0;$i<strlen($str);$i++)
        $hex.=dechex(ord($str[$i]));
    $hex=strtoupper($hex);
    return $hex;
}

//16进制转字符串
function hexToStr($hex){
    $str="";
    $hex = str_replace(' ','',$hex);
    for($i=0;$i<strlen($hex)-1;$i+=2)
        $str.=chr(hexdec($hex[$i].$hex[$i+1]));
    return $str;
}
//十六进制转浮点数
function hexToDecFloat($strHex) {
    $v = hexdec($strHex);
    $flag = $v >> 31;
    if($v >= (1 << 31)){
        $v =$v & ((1 << 31) - 1);
    }
    $x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
    $exp = ($v >> 23 & 0xFF) - 127;
    $res = round($x * pow(2, $exp - 23),3);//四舍五入取3位小数点
    if($flag < 0 && $res){
        $res = -$res;
    }
    return $res;
}

//十六进制转32位浮点数
function hexTo32Float($strHex) {
    $v = hexdec($strHex);
    $x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
    $exp = ($v >> 23 & 0xFF) - 127;
    return $x * pow(2, $exp - 23);
}

/**
 * 生成变量名配置：头尾不变、第二部为二进制布尔型、第三部分为int型、第四部分为float
 * @param $hex_str
 * @param int $bool_len 10
 * @param int $int_len 19
 * @param int $float_len 10
 * @return string
 */
function creatVarConfig($bool_len = 10, $int_len = 16, $float_len = 14)//按要求转码:16进制字符串
{//55 41 45 70 a4 96 c3 aa
    $var_array = [];
//    $hex_str = '';
//    $hex_arr = explode(' ', $hex_str);
    for($i=0; $i<(2+$bool_len+$int_len+$float_len);$i++){
        $hex_arr[] = '00';
    }
    $total_len = count($hex_arr);
    //1、头尾保留原样
    $var_array['LB0'] = $first = array_shift($hex_arr);
    $last = array_pop($hex_arr);
    // $last = array_slice($hex_arr, 0, $second_len);
    //2、解析成二进制
    $index = 1;

    $bool_arr = array_slice($hex_arr, 0, $bool_len);
    $bool_str = '';
    foreach ($bool_arr as $key => $value) {
        // $third_str .= sprintf("%08d", decbin(hexdec($value))).'';//16->10->2
        $tmp = sprintf("%08b",  hexdec($value));//二进制
        $lenth = strlen($tmp);
        for($i=0; $i<$lenth; $i++){
            $var_array['L'.$index.'.'.$i] = $tmp[$i];
        }
        $bool_str .= $tmp;//二进制
        $index++;
    }
    //3、解析成word型：int型，两个字节：19个 LW
    $int_slice_num = 2;
    $int_str = '';
    for($i=0; $i<$int_len; $i++){
        $tmp_index = $bool_len;
        $var_array['LW'.$index] = hexToDecInt(implode(array_slice($hex_arr, $tmp_index, $int_slice_num), ''));
//            var_dump($var_array['LW'.$index]);
        $int_str .= $var_array['LW'.$index];
        $index = $index+$int_slice_num;
    }
    //4、固定位数解析成浮点型：10个：LD
//        $second_str = implode(array_slice($hex_arr, 0, $second_len), '');
    $float_slice_num = 4;
    $float_str = '';
    $float_offset = $bool_len+$int_len*$int_slice_num;
    for($i=0; $i<$float_len; $i++){
        $tmp_index = $float_offset+$i*$float_slice_num;
        $var_array['LD'.$index] = hexToDecFloat(implode(array_slice($hex_arr, $tmp_index, $float_slice_num), ''));
        $float_str .= $var_array['LD'.$index];
        $index = $index+$float_slice_num;
    }
    $var_array['LB'.$index] = $last;
//    echo $first.$bool_str.$int_str.$float_str.$last;
    return array_keys($var_array);
}

//十六进制转int型
function hexToDecInt($strHex){
    //方法1
    return unpack("s",pack("s", hexdec($strHex)))[1];
    //方法2
    $tmp_bin = sprintf("%016b",  hexdec($strHex));//decbin(hexdec("016d"));
    if($tmp_bin[0] == 1){
        $fan_bin = '';
        for($i=0;$i<16;$i++){
            $fan_bin .= intval(!$tmp_bin[$i]);
        }
        $tmpsum = bindec($fan_bin) + bindec(1);

        return -$tmpsum;
    }else{
        return bindec($tmp_bin);
    }
//        return hexdec($strHex);
}
//float转16进制
function floatToHex($num){
    return strrev(unpack('h*',pack('f', $num))[1]);
}
//int转16进制
function intToHex($num){
    return strrev(unpack('h*',pack('s', $num))[1]);
}

/**
 * 处理接收的数据：头尾不变、第二部为二进制布尔型、第三部分为int型、第四部分为float
 * @param $hex_str
 * @param int $bool_len 10
 * @param int $int_len 19
 * @param int $float_len 10
 * @return string
 */
function handelMsg($hex_str, $bool_len = 10, $int_len = 19, $float_len = 10)//按要求转码:16进制字符串
{//55 41 45 70 a4 96 c3 aa
    $var_array = [];
    $hex_arr = explode(' ', $hex_str);
    $total_len = count($hex_arr);
    //1、头尾保留原样
    $var_array['LB0'] = $first = array_shift($hex_arr);
    $unique_code = array_shift($hex_arr);//设备标志符
    $var_array['unique_code'] = $unique_code;
    $last = array_pop($hex_arr);
    $check_code = array_pop($hex_arr);
    // $last = array_slice($hex_arr, 0, $second_len);
    //2、解析成二进制
    $index = 1;

    $bool_arr = array_slice($hex_arr, 0, $bool_len);
    $bool_str = '';
    foreach ($bool_arr as $key => $value) {
        // $third_str .= sprintf("%08d", decbin(hexdec($value))).'';//16->10->2
        $tmp = sprintf("%08b",  hexdec($value));//二进制
        $lenth = strlen($tmp);
        for($i=0; $i<$lenth; $i++){
            $var_array['L'.$index.'.'.$i] = $tmp[$i];
        }
        $bool_str .= $tmp;//二进制
        $index++;
    }
    //3、解析成word型：int型，两个字节：19个 LW
    $int_slice_num = 2;
    $int_str = '';
    for($i=0; $i<$int_len; $i++){
        $tmp_index = $bool_len;
        $var_array['LW'.$index] = hexToDecInt(implode(array_slice($hex_arr, $tmp_index, $int_slice_num), ''));
//            var_dump($var_array['LW'.$index]);
        $int_str .= $var_array['LW'.$index];
        $index = $index+$int_slice_num;
    }
    //4、固定位数解析成浮点型：10个：LD
//        $second_str = implode(array_slice($hex_arr, 0, $second_len), '');
    $float_slice_num = 4;
    $float_str = '';
    $float_offset = $bool_len+$int_len*$int_slice_num;
    for($i=0; $i<$float_len; $i++){
        $tmp_index = $float_offset+$i*$float_slice_num;
        $var_array['LD'.$index] = hexToDecFloat(implode(array_slice($hex_arr, $tmp_index, $float_slice_num), ''));
        $float_str .= $var_array['LD'.$index];
        $index = $index+$float_slice_num;
    }
    $var_array['check_code'] = $check_code;
    $var_array['LB'.$index] = $last;
    echo $first.$bool_str.$int_str.$float_str.$last;
    return $var_array;
}

function getMillisecond() {
    list($s1, $s2) = explode(' ', microtime());
    return sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
}

