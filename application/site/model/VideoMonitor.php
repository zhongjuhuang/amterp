<?php
namespace app\site\model;

use think\Model;
use think\model\concern\SoftDelete;

class VideoMonitor extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';


}