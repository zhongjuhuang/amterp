<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/18
 * Time: 6:07 PM
 */

namespace app\site\model;


use think\Db;
use think\Model;
//use think\model\concern\SoftDelete;

class DeviceDatHistory extends Model
{
// 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
//    use SoftDelete;
//    protected $deleteTime = 'dtime';


    /**
     * 设置表格：根据当前时间设置
     */
    public function setTable()
    {

        //这个月
        $newmot_time = date("Ym");
        $table_name = config('database.prefix')."device_dat_history_$newmot_time";
        //验证表是否存在
        $is_table = db()->query('SHOW TABLES LIKE '."'".$table_name."'");
        if($is_table){
            //表存在
            return $this->table($table_name);
        }else{
            //表不存在
            //创建数据表
            $sql = "CREATE TABLE IF NOT EXISTS `$table_name`(
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `var_name` varchar(8) NOT NULL DEFAULT '',
              `value` varchar(32) NOT NULL DEFAULT '' COMMENT '变量值',
              `refreshtime` datetime DEFAULT NULL,
              `contorl_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '设备ID',
              `point_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属设备线路点ID',
              `data_eq` tinyint(1) NOT NULL DEFAULT '' COMMENT '数据质量:0、正常 1、低低报警2、低报警3、高报警4、高高报警',
              `data_scale` varchar(16) NOT NULL DEFAULT '' COMMENT '数据变化率',
              `record_type` tinyint(1) DEFAULT NULL COMMENT '记录方式：0、定时记录1、突变记录2、突变点记录',
              `ctime` int(10) NOT NULL DEFAULT '0',
              `mtime` int(10) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;";
            Db::execute($sql);
            return $this->table($table_name);
        }

    }

    /**
     * 根据时间获取对应表格
     * @param string $date：Ym格式
     */
    public static function getTableByDate($date = ''){

    }

    public function subTable($start_y_month, $end_y_month, $where_sql='')
    {
        if($end_y_month == $start_y_month){
//            $this->query->options['table'] = $this->getTable().'_'.$start_y_month;
            $table_name = $this->getTable().'_'.$start_y_month;
            return $this->table($table_name);
        }else{
            $start_y = substr($start_y_month,0,4);
            $end_y = substr($end_y_month,0,4);
            $start_month = substr($start_y_month,4,2);
            $end_month = substr($end_y_month,4,2);
            $tableName = [];
            if($start_y == $end_y){
                $table_num = $end_month - $start_month + 1;
                for ($i = 0; $i < $table_num; $i++) {
                    $tableName[] = 'SELECT * FROM '.$this->getTable().'_'.$start_y.sprintf('%02s',($start_month + $i)).$where_sql;
                }

            }else{
                for ($i = 0; $i < 12-$start_month+1; $i++) {
                    $tableName[] = 'SELECT * FROM '.$this->getTable().'_'.$start_y.sprintf('%02s',($start_month + $i)).$where_sql;
                }
                for($year=1; $year < $end_y-$start_y; $year++){
                    for ($i = 1; $i <= 12; $i++) {//几年
                        $tableName[] = 'SELECT * FROM '.$this->getTable().'_'.($start_y+$year).sprintf('%02s',$i).$where_sql;
                    }
                }

                for ($i = 0; $i < $end_month; $i++) {
                    $tableName[] = 'SELECT * FROM '.$this->getTable().'_'.$end_y.sprintf('%02s',$i+1).$where_sql;
                }

//                $this->query->options['table'] = ['( ' . implode(" UNION ", $tableName) . ' )' => $this->query->name];
//                $this->query->options['table'] = ['( ' . implode(" UNION ", $tableName) . ' )' => $this->query->name];
            }

            return $this->union($tableName);
        }
    }

    public function countSubTable($start_y_month, $end_y_month, $where_sql=''){

    }

}