<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/18
 * Time: 4:39 PM
 */

namespace app\site\model;


use think\Model;
//use think\model\concern\SoftDelete;

class DeviceVarConfig extends Model
{
// 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
//    use SoftDelete;
//    protected $deleteTime = 'dtime';

    public function site()
    {
        return $this->hasOne('Site','id','site_id');
    }

    /**
     * 判断是否重复
     */
    public function checkVarname($var_name, $partition_type, $type = 1, $id = 0){
        if(empty($var_name) || !$type){
            $this->error = '参数错误';
            return false;
        }
        if($type == 2 && (!is_numeric($var_name) || $var_name > 65535)){
            $this->error = '寻址编码只能为数字且不能大于65535';
            return false;
        }
        $rs = DeviceVarConfig::where('var_name','=',$var_name)
            ->where('type','=',$type)
            ->where('partition_type','=',$partition_type);
        if($id > 0){
            $rs = $rs->where('id','<>',$id);
        }
        $rs = $rs->limit(1)->count();
        if($rs){
            if($type == 2){
                $this->error = '寻址编码重复';
            }else{
                $this->error = '变量符号重复';
            }
            return false;
        }else{
            return true;
        }
    }
}