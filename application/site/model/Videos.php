<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/6/8
 * Time: 3:28 PM
 */

namespace app\site\model;


use think\Model;

class Videos extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;

    public function site()
    {
        return $this->hasOne('Site','id','site_id');
    }

    public function device()
    {
        return $this->hasOne('DeviceMonitor','id','device_id');
    }
}