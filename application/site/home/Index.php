<?php
namespace app\site\home;
use app\common\controller\Common;
use app\common\model\Haikang;
use app\site\model\DeviceControlPoint;
use app\site\model\DeviceMonitor;
use app\site\model\DeviceVarConfig;
use app\site\model\RealData;
use app\site\model\Site;
use app\site\model\Videos;
use app\system\model\SystemConfig;
use GatewayClient\Gateway;
require_once __DIR__.'/../../../vendor/GatewayClient/Gateway.php';

/**
 * 移动站点
 * @package app\site\home
 */
class Index extends Common
{
    public function welcome(){

        $this->assign('title','首页');
        return $this->fetch();
    }
    public function index()
    {

        $site_id = $this->request->param('site_id/d');
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if($site_id){
                $where[] = ['site_id','=',$site_id];

            }
            if ($keyword) {
                $where[] = ['contorl_name', 'like', '%'.$keyword.'%'];
            }

            $data['list']   = DeviceMonitor::with('site')->where($where)->page($page)->limit($limit)->select();
            $data['count']  = DeviceMonitor::where($where)->count('id');
            $data['code']   = 0;
            return json($data);

        }
        $formData = [];
        if($site_id){
            $formData['site_id'] = $site_id;

        }
        //所有站点
        $sites = Site::where('is_show','=',1)->column('id,name');
        $this->assign('sites', $sites);
        $this->assign('formData', $formData);
        $this->assign('title', '设备列表');

        return $this->fetch();
    }

    public function deviceList(){

        return $this->fetch();
    }
    public function checkPass(){
        $password = $this->request->param('password');
        $device_config = SystemConfig::where('group','=','device')->column('name,value');
        $pass_set = [
            'password' => isset($device_config['password'])?md5($device_config['password']):md5('ymn1234'),
            'cycle_time' => isset($device_config['cycle_time'])?$device_config['cycle_time']:3,//失效周期设置，单位（分）
//            'timeout' => 1592898531,
        ];
        if(md5($password) == $pass_set['password']){
            $pass_set['timeout'] = time()+$pass_set['cycle_time']*60;
            session('device_pass',$pass_set);
            return json([
                'code' => 1,
                'data' => $pass_set['timeout'],
                'msg' => "密码正确",
                'wait' => 3]);
            $this->success('密码正确');
        }else{
            $this->error('密码错误');
        }
    }

    public function show(){

        $id = $this->request->param('id');
        $point_model = new DeviceControlPoint();
        $data_list = $point_model->where(['contorl_id'=>$id])->select();
        $model = new DeviceMonitor();
        $formData = $model->where(['id'=>$id])->find();
        $this->assign('formData', $formData);
        $this->assign('data_list', $data_list);
        $var_name_list = array_column($data_list->toArray(),'var_name');
        $this->assign('var_name_list', $var_name_list);
        if(!empty($var_name_list)){
            $var_config = DeviceVarConfig::where('type','=',1)
                ->where('var_name','in', $var_name_list)
                ->column('var_name,data_type');
        }else{
            $var_config = [];
        }
        $this->assign('name_type', $var_config);
        //写变量列表
        $write_var_list = DeviceVarConfig::where('type','=',2)
            ->where('partition_type','=',$formData['partition_type'])
            ->select();
        $not_btn = [];
        foreach ($write_var_list as $value){
            if($value['gateway_name'] != '按钮'){
                $not_btn[] = [
                    'title' => $value['var_alias'],
                    'value' => $value['id'],
                ];
            }
        }
        $this->assign('write_text', $not_btn);
        $this->assign('write_var_list', $write_var_list);
        $this->assign('title', $formData['contorl_name']);
        return $this->fetch();
    }
    //测试向对应的客户端发送消息
    public function addControl(){
        /*=== 如果GatewayClient和GatewayWorker不在同一台服务器需要以下步骤 ===
        * 1、需要设置start_gateway.php中的lanIp为实际的本机内网ip(如不在一个局域网也可以设置成外网ip)，设置完后要重启GatewayWorker
        * 2、GatewayClient这里的Gateway::$registerAddress的ip填写填写上面步骤1lanIp所指定的ip，端口
        * 3、需要开启GatewayWorker所在服务器的防火墙，让以下端口可以被GatewayClient所在服务器访问，
        *    端口包括Rgister服务的端口以及start_gateway.php中lanIp与startPort指定的几个端口*/
        Gateway::$registerAddress = config('device.GATEWAY_REGISTER_ADDRESS');//'127.0.0.1:1238';//
        $device_id = $this->request->param('id');
        $post_data = $this->request->post();
        $model = new DeviceMonitor();
        $device = $model->where(['id'=>$device_id])->find();
        $client_id = $device['client_id'];
        $control_name = $post_data['control_value'];//*1
//        $var_name = $post_data['var_name'];
        $var_id = $post_data['var_id'];
        $write_code = config('device.WRITE_CODE');
        if($client_id && Gateway::isOnline($client_id)){
            $msg = $write_code['start_code'];//$device['start_code'];
//            $msg .= strToHex($var_name);//两个字节的地址
            $var_config = DeviceVarConfig::where('type','=',2)
                ->where('id','=',$var_id)
                ->find();
            $var_name = str_replace(' ','',$var_config['var_name']);
            $msg .= sprintf('%04X',$var_name);
            if($var_config['data_type'] == 'Float'){//4字节
                $msg .= '04';
                $msg .= floatToHex($control_name);
            }else{//if($var_config['data_type'] == 'Word')//其余均为2字节
//            if(is_int($control_name)){
                $msg .= '02';//字节长度
                //对应长度字节的操作
                $msg .= intToHex($control_name);
            }
            $msg .= $write_code['end_code'];
//            $msg .= $device['end_code'];
            $ascii = '';
            $msg = strtoupper($msg);//全部转大写
            for($i=0; $i<strlen($msg); $i=$i+2){
                $ascii .= chr(hexdec($msg[$i].$msg[$i+1]));//对方实际接收的是$ascii
            }
//            echo $msg.':'.$ascii;
            $result = Gateway::sendToClient($client_id, $ascii);
//            dump($result);
            $this->success('操作成功');
        }else{
            return $this->error('设备不在线，请检查后再操作');
        }

    }

    //获取各个线路点的数据并显示
    public function realdata_select(){
        $contorl_id = $this->request->param('control_id');
        $var_names = $this->request->param('var_name');
        //var_code[]: in
        $model = new RealData();
        //select * from mod_device_control_point where control_id in (xxx,xx) group by
        $list = $model->where('contorl_id','=',$contorl_id)
            ->where('var_name','in',$var_names)
            ->select();//怎么查询最新一批
        return json(['status'=>1,'message'=>'','result'=>$list]);
    }

    //视频展示
    public function videoList(){
        $site_id    = $this->request->param('site_id/d');
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if($site_id){
                $where[] = ['site_id','=',$site_id];

            }
            if ($keyword) {
                $where[] = ['name', 'like', '%'.$keyword.'%'];
            }

            $data['list']   = Videos::with('site')->where($where)->page($page)->limit($limit)->select();
            $data['count']  = Videos::where($where)->count('id');
            $data['code']   = 0;
            return json($data);

        }
        $formData = [];
        if($site_id){
            $formData['site_id'] = $site_id;

        }
        //所有站点
        $sites = Site::where('is_show','=',1)->column('id,name');
        $this->assign('sites', $sites);
        $this->assign('formData', $formData);
        $this->assign('title', '视频列表');

        return $this->fetch();
    }

    public function showVideo(){
        $id = $this->request->param('id');
        if($id){
            $info = Videos::get($id);
            $current_time = getMillisecond();
            if($info['expire_time'] <= $current_time){
                $haikang = new Haikang($info['app_key'],$info['app_secret'], $info['access_token'], $info['expire_time']);
                $access_token = $haikang->getAccessToken();
                if(!$access_token){
                    return $this->error($haikang->error);
                }
                $info['access_token'] = $post_data['access_token'] = $access_token;
                $info['expire_time'] = $post_data['expire_time'] = $haikang->token_expire_time;
                $model = new Videos();
                $count = $model->save($post_data,['id'=>$id]);
                if(!$count){
                    return $this->error($model->getError());
                }
            }
            $url = '?autoplay=1&accessToken='.$info['access_token'].'&'.($info['is_encrypt']==1?'validCode='.$info['device_code'].'&':'').'deviceSerial='.$info['device_serial'].'&channelNo='.$info['channel_no'].'&hd=1';
            $param_arr = [
                'autoplay' => 1,
//                'autoplay' => 0,
                'accessToken' => $info['access_token'],
                'validCode' => $info['device_code'],
                'deviceSerial' => $info['device_serial'],
                'channelNo' => $info['channel_no'],
                'hd' => 1,
            ];
        }
        $this->assign('info', $info);
        $this->assign('url', $url);
        $this->assign('url', $url);
        $this->assign('url_param_arr', $param_arr);
        $formData = [];
        //关联设备控制按钮
        $not_btn = [];
        if($info['device_id'] > 0){
            $id = $info['device_id'];
            $model = new DeviceMonitor();
            $formData = $model->where(['id'=>$id])->find();
            /*$point_model = new DeviceControlPoint();
            $data_list = $point_model->where(['contorl_id'=>$id])->select();
            $this->assign('data_list', $data_list);
            $var_name_list = array_column($data_list->toArray(),'var_name');
            $this->assign('var_name_list', $var_name_list);
            if(!empty($var_name_list)){
                $var_config = DeviceVarConfig::where('type','=',1)
                    ->where('var_name','in', $var_name_list)
                    ->column('var_name,data_type');
            }else{
                $var_config = [];
            }
            $this->assign('name_type', $var_config);*/
            //写变量列表
            //写变量列表
            $write_var_list = DeviceVarConfig::where('type','=',2)
                ->where('partition_type','=',$formData['partition_type'])
                ->select();

            foreach ($write_var_list as $value){
                if($value['gateway_name'] != '按钮'){
                    $not_btn[] = [
                        'title' => $value['var_alias'],
                        'value' => $value['id'],
                    ];
                }
            }
            $this->assign('write_var_list', $write_var_list);
        }
        $this->assign('write_text', $not_btn);
        $this->assign('formData', $formData);
        $this->assign('title', $info['name']);
        return $this->fetch();
    }

    public function test(){
        $this->assign('title', 'test');
        return $this->fetch();
    }
}