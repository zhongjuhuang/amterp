<?php
/**
 * 监控管理
 * User: zhongjuhuang
 * Date: 2020/4/24
 * Time: 1:40 PM
 */

namespace app\site\admin;


use app\common\model\Haikang;
use app\site\model\Site;
use app\site\model\Videos;
use app\system\admin\Admin;

class VideoMonitor extends Admin
{
    protected $hisiModel = 'Videos';//模型名称[通用添加、修改专用]
    protected $hisiTable = '';//表名称[通用添加、修改专用]
    protected $hisiAddScene = '';//添加数据验证场景名
    protected $hisiEditScene = '';//更新数据验证场景名
    protected $hisiValidate = 'Videos';
    //Device Monitor

    public function index(){
        $site_id    = $this->request->param('site_id/d');
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if($site_id){
                $where[] = ['site_id','=',$site_id];

            }
            if ($keyword) {
                $where[] = ['name', 'like', '%'.$keyword.'%'];
            }

            $data['data']   = Videos::with(['site','device'])->where($where)->page($page)->limit($limit)->select();
            $data['count']  = Videos::where($where)->count('id');
            $data['code']   = 0;
            return json($data);

        }
        $formData = [];
        if($site_id){
            $formData['site_id'] = $site_id;

        }
        //所有站点
        $sites = Site::where('is_show','=',1)->column('id,name');
        $this->assign('sites', $sites);
        $this->assign('formData', $formData);

        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }


    public function add()
    {
        if ($this->request->isPost()) {
            $post_data = $this->request->post();
            $haikang = new Haikang($post_data['app_key'],$post_data['app_secret']);
            $access_token = $haikang->getAccessToken();
            if(!$access_token){
                return $this->error($haikang->error);
            }
            $post_data['access_token'] = $access_token;
            $post_data['expire_time'] = $haikang->token_expire_time;
            $rs = $haikang->getDeviceInfo($post_data['device_serial']);
            if($rs['code'] == 200 || $rs['code'] == 20018){
                if($rs['code'] == 20018){//该用户不拥有该设备
                    $rs = $haikang->addDevice($post_data['device_serial'], $post_data['device_code']);
                    if($rs['code'] != 200){
                        return $this->error($rs['msg']);
                    }else{
                        $rs = $haikang->getDeviceInfo($post_data['device_serial']);
                        if($rs['code'] != 200){
                            return $this->error($rs['msg']);
                        }
                    }
                }
                $post_data['device_name'] = $rs['data']['deviceName'];
                $post_data['device_status'] = $rs['data']['status'];
                $post_data['is_encrypt'] = $rs['data']['isEncrypt'];
                $model = new Videos();
                $count = $model->save($post_data);
                if($count){
                    return $this->success('添加成功',url('index'));
                }else{
                    return $this->error($model->getError());
                }
            }else{
                return $this->error($rs['msg']);
            }
        }else{
            //所有站点
            $sites = Site::where('is_show','=',1)->column('id,name');
            $this->assign('sites', $sites);
            $device = \app\site\model\DeviceMonitor::column('id,contorl_name');
            $this->assign('device_list', $device);
            return parent::add();
        }
    }
    public function edit()
    {
        if ($this->request->isPost()) {
            $post_data = $this->request->post();
            $model = new Videos();
            $id = $post_data['id'];
            $info = $model->get($id);
            if($info){
                $haikang = new Haikang($post_data['app_key'],$post_data['app_secret'], $info['access_token'], $info['expire_time']);

                $access_token = $haikang->getAccessToken();
                if(!$access_token){
                    return $this->error($haikang->error);
                }
                $post_data['access_token'] = $access_token;
                $post_data['expire_time'] = $haikang->token_expire_time;
                $rs = $haikang->getDeviceInfo($post_data['device_serial']);
                if($rs['code'] == 200 || $rs['code'] == 20018){
                    if($rs['code'] == 20018){//该用户不拥有该设备
                        $rs = $haikang->addDevice($post_data['device_serial'], $post_data['device_code']);
                        if($rs['code'] != 200){
                            return $this->error($rs['msg']);
                        }else{
                            $rs = $haikang->getDeviceInfo($post_data['device_serial']);
                            if($rs['code'] != 200){
                                return $this->error($rs['msg']);
                            }
                        }
                    }
                    $post_data['device_name'] = $rs['data']['deviceName'];
                    $post_data['device_status'] = $rs['data']['status'];
                    $post_data['is_encrypt'] = $rs['data']['isEncrypt'];

                    $count = $model->save($post_data,['id'=>$id]);
                    if($count){
                        return $this->success('编辑成功',url('index'));
                    }else{
                        return $this->error($model->getError());
                    }
                }else{
                    return $this->error($rs['msg']);
                }
            }else{
                return $this->error('ID错误');
            }

        }else{
            //所有站点
            $sites = Site::where('is_show','=',1)->column('id,name');
            $this->assign('sites', $sites);
            $device = \app\site\model\DeviceMonitor::column('id,contorl_name');
            $this->assign('device_list', $device);
            return parent::edit();
        }
    }

}