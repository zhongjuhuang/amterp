<?php
namespace app\site\admin;
use app\site\model\Site;
use app\system\admin\Admin;

class Index extends Admin
{
    protected $hisiModel = 'Site';//模型名称[通用添加、修改专用]
    protected $hisiTable = '';//表名称[通用添加、修改专用]
    protected $hisiAddScene = '';//添加数据验证场景名
    protected $hisiEditScene = '';//更新数据验证场景名
    protected $hisiValidate = 'Site';

    public function index()
    {
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['name', 'like', '%'.$keyword.'%'];
            }

            $data['data']   = Site::where($where)->page($page)->limit($limit)->select();
            $data['count']  = Site::where($where)->count('id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        // tab切换数据
        // $hisiTabData = [
        //     ['title' => '后台首页', 'url' => 'system/index/index'],
        // ];
        // current 可不传
        // $this->assign('hisiTabData', ['menu' => $hisiTabData, 'current' => 'system/index/index']);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    /**
     * [通用方法]状态设置
     * 禁用、启用都是调用这个内部方法
     * @author 橘子俊 <364666827@qq.com>
     * @return mixed
     */
    public function isShow()
    {
        $val        = $this->request->param('val/d');
        $id         = $this->request->param('id/a');
        $field      = $this->request->param('field/s', 'is_show');

        if (empty($id)) {
            return $this->error('缺少id参数');
        }

        if ($this->hisiModel) {

            $obj = $this->model();

        } else if ($this->hisiTable) {

            $obj = db($this->hisiTable);

        } else {

            return $this->error('当前控制器缺少属性（hisiModel、hisiTable至少定义一个）');

        }

        $pk     = $obj->getPk();

        $where  = [];
        $where[]= [$pk, 'in', $id];
        $where  = $this->getRightWhere($where);

        $result = $obj->where($where)->setField($field, $val);
        if ($result === false) {
            return $this->error('状态设置失败');
        }

        return $this->success('状态设置成功', '');
    }



}