<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/6/23
 * Time: 5:17 PM
 */

namespace app\site\admin;


use app\common\controller\Common;
use app\site\model\RealData;
use GatewayClient\Gateway;
use app\site\model\DeviceMonitor as Monitor;
use app\site\model\DeviceControlPoint;
use app\site\model\DeviceVarConfig;
use app\system\model\SystemConfig;
require_once __DIR__.'/../../../vendor/GatewayClient/Gateway.php';

class Device extends Common
{
    public function checkPass(){
        $password = $this->request->param('password');
        $device_config = SystemConfig::where('group','=','device')->column('name,value');
        $pass_set = [
            'password' => isset($device_config['password'])?md5($device_config['password']):md5('ymn1234'),
            'cycle_time' => isset($device_config['cycle_time'])?$device_config['cycle_time']:3,//失效周期设置，单位（分）
//            'timeout' => 1592898531,
        ];
        if(md5($password) == $pass_set['password']){
            $pass_set['timeout'] = time()+$pass_set['cycle_time']*60;
            session('device_pass',$pass_set);
            return json([
                'code' => 1,
                'data' => $pass_set['timeout'],
                'msg' => "密码正确",
                'wait' => 3]);
            $this->success('密码正确');
        }else{
            $this->error('密码错误');
        }
    }

    public function show(){
        $id = $this->request->param('id');
        $point_model = new DeviceControlPoint();
        $data_list = $point_model->where(['contorl_id'=>$id])->select();
        $model = new Monitor();
        $formData = $model->where(['id'=>$id])->find();
        $this->assign('formData', $formData);
        $this->assign('data_list', $data_list);
        $var_name_list =  array_column($data_list->toArray(),'var_name');
        $this->assign('var_name_list', $var_name_list);
        /* $btn_list = [//寻址->变量名称
             '1' => '上升',
             '2' => '下降',
             '3' => '打开',
             '4' => '关闭',
         ];
         $this->assign('control_list', $btn_list);//todo 需跟沈工约定操作的操作符字节是多少*/
        if(!empty($var_name_list)){
            $var_config = DeviceVarConfig::where('type','=',1)
                ->where('var_name','in', $var_name_list)
                ->where('partition_type','=',$formData['partition_type'])
                ->column('var_name,data_type');
        }else{
            $var_config = [];
        }
        $this->assign('name_type', $var_config);
        //写变量列表
        $write_var_list = DeviceVarConfig::where('type','=',2)
            ->where('partition_type','=',$formData['partition_type'])
            ->select();
        $this->assign('write_var_list', $write_var_list);
        return $this->fetch();
    }


//测试向对应的客户端发送消息
    public function addControl(){
        /*=== 如果GatewayClient和GatewayWorker不在同一台服务器需要以下步骤 ===
        * 1、需要设置start_gateway.php中的lanIp为实际的本机内网ip(如不在一个局域网也可以设置成外网ip)，设置完后要重启GatewayWorker
        * 2、GatewayClient这里的Gateway::$registerAddress的ip填写填写上面步骤1lanIp所指定的ip，端口
        * 3、需要开启GatewayWorker所在服务器的防火墙，让以下端口可以被GatewayClient所在服务器访问，
        *    端口包括Rgister服务的端口以及start_gateway.php中lanIp与startPort指定的几个端口*/
        Gateway::$registerAddress = config('device.GATEWAY_REGISTER_ADDRESS');//'127.0.0.1:1238';//

        //{"7f0000010c1f00000001":{"17uid3":"7f0000010c1f00000001"},"7f0000010c2200000001":{"8uid3":"7f0000010c2200000001"}}
        /*$all_client_session = Gateway::getAllClientSessions();
        $uid_clientid_arr = [];
        foreach($all_client_session as $client_id => $session){
            $uid_clientid_arr[$session['uid']] = $client_id;
        }
        dump($uid_clientid_arr);
        echo json_encode($all_client_session);
        $client_id = $uid_clientid_arr[$device_id];//Gateway::getClientIdByUid($device['id']);
        */
        $device_id = $this->request->param('id');
        $post_data = $this->request->post();
        $model = new Monitor();
        $device = $model->where(['id'=>$device_id])->find();
        $client_id = $device['client_id'];
        $control_name = $post_data['control_value'];//*1
//        $var_name = $post_data['var_name'];
        $var_id = $post_data['var_id'];
        $write_code = config('device.WRITE_CODE');
        if($client_id && Gateway::isOnline($client_id)){
            $msg = $write_code['start_code'];//$device['start_code'];
//            $msg .= strToHex($var_name);//两个字节的地址
            $var_config = DeviceVarConfig::where('type','=',2)
                ->where('id','=',$var_id)
                ->find();
            $var_name = str_replace(' ','',$var_config['var_name']);
            $msg .= sprintf('%04X',$var_name);
            if($var_config['data_type'] == 'Float'){//4字节
                $msg .= '04';
                $msg .= floatToHex($control_name);
            }else{//if($var_config['data_type'] == 'Word')//其余均为2字节
//            if(is_int($control_name)){
                $msg .= '02';//字节长度
                //对应长度字节的操作
                $msg .= intToHex($control_name);
            }
            $msg .= $write_code['end_code'];
//            $msg .= $device['end_code'];
            $ascii = '';
            $msg = strtoupper($msg);//全部转大写
            for($i=0; $i<strlen($msg)/2; $i++){
                $ascii .= chr(hexdec($msg[$i].$msg[$i+1]));//对方实际接收的是$ascii
            }

            $result = Gateway::sendToClient($client_id, $ascii);
//            dump($result);
//            $this->success('操作成功');
            return json([
                'code' => 1,
                'data' => $msg,
                'msg' => "操作成功",
                'url' => "http://admin.ymnhisi.com/admin.php/site/device_monitor/show.html?id=3&hisi_iframe=yes",
                'wait' => 3,
            ]);
        }else{
            return $this->error('设备不在线，请检查后再操作');
        }

    }

    //获取各个线路点的数据并显示
    public function realdata_select(){
        $contorl_id = $this->request->param('control_id');
        $var_names = $this->request->param('var_name');
        //var_code[]: in
        $model = new RealData();
        //select * from mod_device_control_point where control_id in (xxx,xx) group by
        $list = $model->where('contorl_id','=',$contorl_id)
            ->where('var_name','in',$var_names)
            ->select();//怎么查询最新一批

        return json(['status'=>1,'message'=>'','result'=>$list]);
        return json_decode('{"status":1,"result":[{"id":134,"var_code":"SS_AI2","value":"16384","refreshtime":"2020-05-06 08:27:23","data_eq":"0","data_scale":"0"},{"id":132,"var_code":"AI2","value":"0","refreshtime":"2020-05-18 09:14:21","data_eq":"1","data_scale":"0"},{"id":131,"var_code":"AI1","value":"1.813931","refreshtime":"2020-05-18 18:03:56","data_eq":"0","data_scale":"0.001822"},{"id":107,"var_code":"S.V.TJC_FQ1","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":106,"var_code":"S.V.GFJ2_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":105,"var_code":"S.V.HXXJSF_OT","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":102,"var_code":"S.V.HXF_OT","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":99,"var_code":"S.V.CSF_OT","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":97,"var_code":"S.V.HXB_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":95,"var_code":"S.V.GFJ_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":93,"var_code":"S.V.WNB2_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":91,"var_code":"S.V.WNB1_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":87,"var_code":"S.V.CXB1_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":85,"var_code":"S.V.TSB2_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":83,"var_code":"S.V.TSB1_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"}],"count":15,"total":15,"message":"查询成功！","url":"http:\/\/zijin.demo.zhaota8.com\/admin\/login.html"}', true);
    }
}