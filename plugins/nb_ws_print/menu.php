<?php
// +----------------------------------------------------------------------
// | HisiPHP框架[基于ThinkPHP5.1开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2021 http://www.hisiphp.com
// +----------------------------------------------------------------------
// | HisiPHP承诺基础框架永久免费开源，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 橘子俊 <364666827@qq.com>，开发者QQ群：50304283
// +----------------------------------------------------------------------
/**
 * 插件后台菜单
 * 字段说明
 * url【链接地址】如果不是外链请设置为默认：system/plugins/run
 * param【扩展参数】格式：_p=nb_ws_print&_c=控制器&_a=方法&aaa=自定义参数
 * 当url值为：system/plugins/run时，param必须录入，且_p=nb_ws_print是必传参数
 */
return [
    [
        'title'         => '简单打印',
        'icon'          => 'fa fa-cog',
        'url'           => 'system/plugins/run',
        'param'         => '_p=nb_ws_print&_c=index&_a=index',
        'target'        => '_self',
        'sort'          => 0,
        'nav'           => 1,
        'childs'        => [
            [
                'title'         => '打印示例',
                'icon'          => 'aicon ai-shezhi',
                'url'           => 'system/plugins/run',
                'param'         => '_p=nb_ws_print&_c=index&_a=index',
                'target'        => '_self',
                'sort'          => 0,
            ],
        ],
    ],
];