<?php
// +----------------------------------------------------------------------
// | HisiPHP框架[基于ThinkPHP5.1开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2021 http://www.hisiphp.com
// +----------------------------------------------------------------------
// | HisiPHP承诺基础框架永久免费开源，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 橘子俊 <364666827@qq.com>，开发者QQ群：50304283
// +----------------------------------------------------------------------
namespace plugins\payment;
use app\common\controller\Plugins;
use plugins\payment\model\PluginsPayment as PaymentModel;
use plugins\payment\model\PluginsPaymentLog as PaymentLogModel;
defined('IN_SYSTEM') or die('Access Denied');
/**
 * 网银配置插件
 * @package plugins\payment
 */
class payment extends Plugins
{
    /**
     * @var array 插件钩子清单
     */
    public $hooks = [
        'payment_add'=>'支付发起',
    ];

    /**
     * payment_add
     * @param $params
     */
    public function paymentAdd($params)
    {        $res = PaymentModel::where(['code' => 'mianpay', 'status' => 1])->find();
        if (!$res) {
            return $this->error('支付方式不存在！');
        }
        if (!$res['config']) {
            return $this->error('获取'.$res['title'].'配置失败！');
        }
        $this->config = json_decode($res['config'], 1);
        //支付方式
        $data['type'] = $params['type'];
        //商户订单号，商户网站订单系统中唯一订单号，必填
        $data['out_trade_no'] = trim(order_number());

        //订单名称，必填
        $data['subject'] = trim('在线充值');

        //付款金额，必填
        $data['total_amount'] = trim($params['money']);

        //商品描述，可空
        $data['body']= trim('在线充值');

        if (!(new PaymentLogModel)->insertGetId(array(
            'member_id'=>$params['user_id'],
            'out_trade_no'=>$data['out_trade_no'],
            'code'=>'mianpay',
            'note'=>$data['body'],
            'value'=>$data['total_amount'],
            'status'=>1,
            'ctime'=>time()
            ))) {
            return $this->error('配置失败！');
        }
        //构造参数
        $class = "\\plugins\\payment\\driver\\mianpay\\mianpay";
        $class=new $class($this->config);
        $class->pay($data);
    }
    /**
     * 安装前的业务处理，可在此方法实现，默认返回true
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 安装后的业务处理，可在此方法实现，默认返回true
     * @return bool
     */
    public function installAfter()
    {
        return true;
    }

    /**
     * 卸载前的业务处理，可在此方法实现，默认返回true
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }

    /**
     * 卸载后的业务处理，可在此方法实现，默认返回true
     * @return bool
     */
    public function uninstallAfter()
    {
        return true;
    }

}