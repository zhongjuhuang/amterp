<?php
namespace plugins\payment\admin;
use app\common\controller\Common;
use plugins\payment\model\PluginsPayment as PaymentModel;
use plugins\payment\model\PluginsPaymentLog as PaymentLogModel;
use hisi\Dir;
defined('IN_SYSTEM') or die('Access Denied');

class Index extends Common
{

	protected function initialize()
    {
        parent::initialize();
        $this->driver_path = ROOT_PATH.'plugins/payment/driver/';
    }

    public function index()
    {
        $rows = PaymentModel::column('code,id,title,applies,intro,status,sort', 'code');
        $dlist = Dir::getList($this->driver_path);
        $login = [];
        foreach ($dlist as $k => $v) {
            if (!is_file($this->driver_path.$v.'/config.xml')) {
                continue;
            }
            $xml = file_get_contents($this->driver_path.$v.'/config.xml');
            $config = xml2array($xml);
            $config['install'] = 0;
            $config['id'] = 0;
            $config['sort'] = 100;
            $config['status'] = 0;
            if (array_key_exists($v, $rows)) {
                $config['status'] = $rows[$v]['status'];
                $config['install'] = 1;
                $config['id'] = $rows[$v]['id'];
                $config['sort'] = $rows[$v]['sort'];
            }
            if (isset($login[$config['sort']])) {
                $login[$k] = $config;
            } else {
                $login[$k] = $config;
            }
        }
        ksort($login);
        $this->assign('data_list', $login);
        return $this->fetch();
    }
    /**
     * 支付配置
     * @author 橘子俊 <364666827@qq.com>
     * @return mixed
     */
    public function config()
    {
        $code = input('param.code');
        if (empty($code)) {
            return $this->error('参数传递错误！');
        }
        $map = [];
        $map['code'] = $code;
        $row = PaymentModel::where($map)->find();
        $xml = file_get_contents($this->driver_path.$code.'/config.xml');
        $xml_data = xml2array($xml);
        if ($this->request->isPost()) {
            $config = $this->request->post();
            $sqlmap = [];
            unset($config['code']);
            if (empty($config['app_id'])) {
                return $this->error('应用ID为必填项！');
            }
            if (isset($config['icon']) && !empty($config['icon'])) {
                $sqlmap['icon'] = $config['icon'];
            } else {
                $config['icon'] = $sqlmap['icon'] = '/static/plugins/payment/'.$xml_data['code'].'.png';
            }
            $sqlmap['code'] = $xml_data['code'];
            $sqlmap['title'] = $xml_data['title'];
            $sqlmap['intro'] = $xml_data['intro'];
            $sqlmap['applies'] = $xml_data['applies'];
            $sqlmap['sort'] = 100;
            $sqlmap['status'] = 1;
            $sqlmap['config'] = json_encode($config, 1);
            if ($row) {
                $sqlmap['id'] = $row->id;
                if (!PaymentModel::update($sqlmap)) {
                    return $this->error('配置失败！');
                }
            } else {
                if (!PaymentModel::create($sqlmap)) {
                    return $this->error('配置失败！');
                }
            }
            return $this->success('配置成功', plugins_url('payment/index/index'));
        }
        if ($row && $row->config) {
            $config = json_decode($row->config, 1);
            foreach ($xml_data['config'] as $k => &$v) {
                if (array_key_exists($k, $config)) {
                    $v['value'] = $config[$k];
                }
            }
        }
        $this->assign('data_info', $xml_data);
        return $this->fetch();
    }
    public function log()
    {
        if ($this->request->isAjax()) {
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $out_trade_no=$this->request->param('out_trade_no');
            $trade_no   = $this->request->param('trade_no');
            $member_id   = $this->request->param('member_id');
            $status   = $this->request->param('status/d',0);
            $code   = $this->request->param('code');
            $where=[];
            if ($out_trade_no) {
                $where['out_trade_no']=$out_trade_no;
            }
            if ($trade_no) {
                $where['trade_no']=$trade_no;
            }
            if ($member_id) {
                $where['member_id']=$member_id;
            }
            if ($status) {
                $where['status']=$status;
            }
            if ($code) {
                $where['code']=$code;
            }
            $data = [];
            $data['data'] = PaymentLogModel::with('hasPayment')
                            ->where($where)
                            ->page($page)
                            ->limit($limit)
                            ->order('id desc')
                            ->select();

            $data['count'] = PaymentLogModel::where($where)->count('id');
            $data['code'] = 0;

            return json($data);

        }
        return $this->fetch();
    }
    public function del($id)
    {
        if (PaymentLogModel::where(['id'=>$id])->delete()) {
            return $this->success('删除成功');
        }
        return $this->error('删除失败！');
    }

    /**
     * 状态设置
     * @return mixed
     */
    public function status()
    {
        $id = get_num();
        $val = input('param.val/d', 0);
        if (!PaymentModel::where('id', $id)->setField('status', $val)) {
            return $this->error('设置失败');
        }
        return $this->success('设置成功');
    }
}