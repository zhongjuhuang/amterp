<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace think\worker;

use GatewayWorker\Lib\Gateway;
use Workerman\Worker;

/**
 * Worker 命令行服务类
 */
class Events
{
    /**
     * onWorkerStart 事件回调
     * 当businessWorker进程启动时触发。每个进程生命周期内都只会触发一次
     *
     * @access public
     * @param  \Workerman\Worker    $businessWorker
     * @return void
     */
    public static function onWorkerStart(Worker $businessWorker)
    {
        $app = new Application;
        $app->initialize();
    }

    /**
     * onConnect 事件回调
     * 当客户端连接上gateway进程时(TCP三次握手完毕时)触发
     *
     * @access public
     * @param  int       $client_id
     * @return void
     */
    public static function onConnect($client_id)
    {
        Gateway::sendToCurrentClient("Your client_id is $client_id");
    }

    /**
     * onWebSocketConnect 事件回调
     * 当客户端连接上gateway完成websocket握手时触发
     *
     * @param  integer  $client_id 断开连接的客户端client_id
     * @param  mixed    $data
     * @return void
     */
    public static function onWebSocketConnect($client_id, $data)
    {
        echo 'onWebSocketConnect';
        var_export($data);
    }

    /**
     * onMessage 事件回调
     * 当客户端发来数据(Gateway进程收到数据)后触发
     *
     * @access public
     * @param  int       $client_id
     * @param  mixed     $data
     * @return void
     */
    public static function onMessage($client_id, $message)
    {
        $ascii = '';
        $hex = '';
        for($i=0;$i<strlen($message);$i++){
            $tmp_ascii = ord($message[$i]);//字符转10进制
            $ascii .= $tmp_ascii.' ';
            $hex .= sprintf("%02x", $tmp_ascii).' ';//10进制转16进制
        }
        $result = self::handelMsg(trim($hex));//
        echo "ascii:".$ascii."\r\n hex:$hex\r\n source:$message \r\n handle:$result\r\n";
//        Gateway::sendToClient($client_id, "ascii:".$ascii."\r\n hex:$hex\r\n source:$message \r\n handle:$result\r\n");
        // 向所有人发送
        // Gateway::sendToAll("$client_id said $message\r\n");
        $cache_file_path = '././gateway.log';
        $content = date('Y-m-d H:i:s').": ascii:".$ascii."\r\n hex:$hex\r\n source:$message \r\n handle:$result\r\n";
        file_put_contents($cache_file_path, $content, FILE_APPEND);
    }

    public static function handelMsg($hex_str, $second_len = 4, $third_len = 2)//按要求转码:16进制字符串
    {//55 41 45 70 a4 96 c3 aa
        $hex_arr = explode(' ', $hex_str);
        //1、头尾保留原样
        $first = array_shift($hex_arr);
        $last = array_pop($hex_arr);
        // $last = array_slice($hex_arr, 0, $second_len);
        //2、固定位数解析成数值等
        $second_str = implode(array_slice($hex_arr, 0, $second_len), '');
        $second_value = hexToDecFloat($second_str);
//        echo $second_value;
        //3、倒数固定位数解析成二进制
        $third_arr = array_slice($hex_arr, $second_len, $third_len);
        $third_str = '';
        foreach ($third_arr as $key => $value) {
            $third_str .= sprintf("%08b",  hexdec($value));//二进制
        }

        return $first.$second_value.$third_str.$last;
    }
    /**
     * onClose 事件回调 当用户断开连接时触发的方法
     *
     * @param  integer $client_id 断开连接的客户端client_id
     * @return void
     */
    public static function onClose($client_id)
    {
        GateWay::sendToAll("client[$client_id] logout\n");
    }

    /**
     * onWorkerStop 事件回调
     * 当businessWorker进程退出时触发。每个进程生命周期内都只会触发一次。
     *
     * @param  \Workerman\Worker    $businessWorker
     * @return void
     */
    public static function onWorkerStop(Worker $businessWorker)
    {
        echo "WorkerStop\n";
    }
}
