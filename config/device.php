<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/21
 * Time: 3:33 PM
 */
return [
    //变量分类
    'class_type' => [
        'A' => '趋势 ',
        'B' => '设置 ',
        'C' => '操作 ',
        'D' => 'PCL动作 ',
        'E' => '设备动作 ',
        'F' => '故障报警 ',
        'H' => '短信事件',
    ],
    'READ_CODE' => [
        'start_code' => '55',//
        'end_code' => 'AA',
    ],
    'WRITE_CODE' => [
        'start_code' => 'AA',//
        'end_code' => 'BB',
    ],
    //Gateway::$registerAddress = '127.0.0.1:1238'
    'GATEWAY_REGISTER_ADDRESS' => '127.0.0.1:1238',
];