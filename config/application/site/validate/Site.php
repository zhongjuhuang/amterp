<?php

/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/4/23
 * Time: 5:00 PM
 */
namespace app\site\validate;
class Site extends \think\Validate
{
    //定义验证规则
    protected $rule = [
        'name|站点名称' => 'require|unique:site',
        'name_alias|站点全称' => 'require',
//        'cid|新闻分类' => 'require|number',
    ];
}