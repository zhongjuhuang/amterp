<?php

/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/4/23
 * Time: 5:00 PM
 */
namespace app\site\validate;
class DeviceMonitor extends \think\Validate
{
    //定义验证规则
    protected $rule = [
        'unique_code|设备标识符' => 'require|unique:device_monitor',
    ];
}