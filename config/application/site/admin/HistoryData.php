<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/21
 * Time: 1:32 PM
 */

namespace app\site\admin;


use app\site\model\DeviceDatHistory;
use app\site\model\DeviceVarConfig;
use app\system\admin\Admin;
use think\Db;

class HistoryData extends Admin
{
    public function index(){
        if ($this->request->isAjax()) {
            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');
            $start_time    = $this->request->param('start_time/s');
            $end_time    = $this->request->param('end_time/s');
            $partition_type    = $this->request->param('partition_type');
            $gateway_name    = $this->request->param('gateway_name');
            $dev_name   = $this->request->param('dev_name');
            $var_name   = $this->request->param('var_name');
            $where_sql = '';
            if($start_time && $end_time){
                //根据时间确定查询的表格
                $start_time = strtotime($start_time);
                $end_time = strtotime($end_time);
                $where[] = ['ctime','between',"$start_time,$end_time"];
                $where_sql .= " `ctime` between $start_time and $end_time";
                $start_time_str = date('Ym',$start_time);
                $end_time_str = date('Ym',$end_time);
            }else{
                $data['data'] = [];
                $data['count']  = 0;
                $data['code']   = 0;
                return json($data);
            }
            if ($keyword) {
                $where[] = ['contorl_name', 'like', '%'.$keyword.'%'];
            }

            $c_where = [];
            if($var_name){
                $c_where[] = ['var_name','=',$var_name];
            }/*else{
                $var_names = array_column($data['data']->toArray(),'var_name');
                if($var_names){
                    $c_where[] = ['var_name','in',$var_names];
                }
            }*/
            if($partition_type){
                $c_where[] = ['partition_type','in',explode(',',$partition_type)];
            }
            if($gateway_name){
                $c_where[] = ['gateway_name','in',explode(',',$gateway_name)];
            }
            if($dev_name){
                $c_where[] = ['dev_name','in',explode(',',$dev_name)];
            }


            $configs = DeviceVarConfig::where($c_where)
                ->where('type','=',1)
                ->column('var_name,var_alias,gateway_name,dev_name,partition_type,min,max,trigtime,sc_trig,lim_LL,lim_L,lim_H,lim_HH');
            $history = new DeviceDatHistory();
            /*$list = $history->subTable($start_time_str, $end_time_str)
                ->where($where)
//                ->fetchSql()
                ->select();*/
//            dump($list);
//            echo $model->getLastSql();die;

            $orderby = ' order by ctime desc';
            if(empty($configs)){
                $data['data']   = [];
                $data['count'] = 0;
            }else{
                $where[] = ['var_name','in', array_keys($configs)];
                $where_sql .= ($where_sql?' and ':'')."`var_name` IN ('".implode("','",array_keys($configs))."')";
                $sub_tables = $this->getSubTables($history->getTable(), $start_time_str, $end_time_str);
                $list = new DeviceDatHistory();
                $count = new DeviceDatHistory();
                $real_count = 0;
                foreach($sub_tables as $key => $value){
                    if($key == 0){
                        $list = $list->table($value);

                    }else{
                        $list = $list->union('select * from '.$value.' where '.$where_sql.$orderby);
//                        $count = $count->union('select COUNT(`id`) AS tp_count from '.$value.' where '.$where_sql);
//                        $list = $list->union('select * from '.$value);
//                        $count = $count->union('select COUNT(`id`) AS tp_count from '.$value);
//                        $count = $count->table($value);
                    }
                    $count = $count->table($value);
                    $real_count += $count->where($where)->count('id');
                }
                $data['data']   = $list//$history->subTable($start_time_str, $end_time_str, $where_sql)
                    ->where($where)
                    ->page($page)
                    ->order('ctime','desc')
                    ->limit($limit)
//                    ->fetchSql()
                    ->select();
//                dump($data['data']);die;
                foreach ($data['data'] as &$datum){
                    if(isset($configs[$datum['var_name']])){
                        $config = $configs[$datum['var_name']];
                        $datum['var_alias'] = $config['var_alias'];
                        $datum['gateway_name'] = $config['gateway_name'];
                        $datum['dev_name'] = $config['dev_name'];
                        $datum['partition_type'] = $config['partition_type'];
                    }

                }
                $count = $real_count;//$count->where($where)->count('id');//->fetchSql()
//            dump($count);die;

                $data['count']  = $count;
            }


            $data['code']   = 0;
            return json($data);

        }else{
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d 23:59:59');
            $this->assign('formData', ['start_time'=>$start_time,'end_time'=>$end_time]);
        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function getSubTables($table_name, $start_y_month, $end_y_month)
    {
        if($end_y_month == $start_y_month){
//            $this->query->options['table'] = $this->getTable().'_'.$start_y_month;
            $table_name = $table_name.'_'.$start_y_month;
            return [$table_name];
        }else{
            $start_y = substr($start_y_month,0,4);
            $end_y = substr($end_y_month,0,4);
            $start_month = substr($start_y_month,4,2);
            $end_month = substr($end_y_month,4,2);
            $tableName = [];
            if($start_y == $end_y){
                $table_num = $end_month - $start_month + 1;
                for ($i = 0; $i < $table_num; $i++) {
                    $tableName[] = $table_name.'_'.$start_y.sprintf('%02s',($start_month + $i));
                }

            }else{
                for ($i = 0; $i < 12-$start_month+1; $i++) {
                    $tableName[] = $table_name.'_'.$start_y.sprintf('%02s',($start_month + $i));
                }
                for($year=1; $year < $end_y-$start_y; $year++){
                    for ($i = 1; $i <= 12; $i++) {//几年
                        $tableName[] = $table_name.'_'.($start_y+$year).sprintf('%02s',$i);
                    }
                }

                for ($i = 0; $i < $end_month; $i++) {
                    $tableName[] = $table_name.'_'.$end_y.sprintf('%02s',$i+1);
                }

            }

            return $tableName;
        }
    }


    public function echart(){
        if ($this->request->isAjax()) {
            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');
            $start_time    = $this->request->param('start_time/s');
            $end_time    = $this->request->param('end_time/s');
            $partition_type    = $this->request->param('partition_type');
            $gateway_name    = $this->request->param('gateway_name');
            $dev_name   = $this->request->param('dev_name');
            $var_name   = $this->request->param('var_name');
            $monitor_id   = $this->request->param('monitor_id');
            $where_sql = '';
            if($monitor_id){
                $where[] = ['contorl_id','=',$monitor_id];
                $where_sql .= " `contorl_id`=$monitor_id ";
            }
            if($start_time && $end_time){
                //根据时间确定查询的表格
                $start_time = strtotime($start_time);
                $end_time = strtotime($end_time);
                $where[] = ['ctime','between',"$start_time,$end_time"];
                $where_sql .= ($where_sql?" and ":"")." `ctime` between $start_time and $end_time";
                $start_time_str = date('Ym',$start_time);
                $end_time_str = date('Ym',$end_time);
            }else{
                $data['data'] = [];
                $data['count']  = 0;
                $data['code']   = 0;
                return json($data);
            }
            if ($keyword) {
                $where[] = ['contorl_name', 'like', '%'.$keyword.'%'];
            }

            $c_where = [];
            if($var_name){
                $c_where[] = ['var_name','=',$var_name];
            }/*else{
            $var_names = array_column($data['data']->toArray(),'var_name');
            if($var_names){
                $c_where[] = ['var_name','in',$var_names];
            }
        }*/
            if($partition_type){
                $c_where[] = ['partition_type','in',explode(',',$partition_type)];
            }
            if($gateway_name){
                $c_where[] = ['gateway_name','in',explode(',',$gateway_name)];
            }
            if($dev_name){
                $c_where[] = ['dev_name','in',explode(',',$dev_name)];
            }


            $configs = DeviceVarConfig::where($c_where)
                ->where('type','=',1)
                ->column('var_name,var_alias,gateway_name,dev_name,partition_type,min,max,trigtime,sc_trig,lim_LL,lim_L,lim_H,lim_HH');
            $history = new DeviceDatHistory();

            if(empty($configs)){
                $data['data']   = [];
                $data['count'] = 0;
            }else{
                if($var_name || $partition_type || $gateway_name || $dev_name){
                    $where[] = ['var_name','in', array_keys($configs)];
                    $where_sql .= ($where_sql?' and ':'')."`var_name` IN ('".implode("','",array_keys($configs))."')";
                }

                $sub_tables = $this->getSubTables($history->getTable(), $start_time_str, $end_time_str);
                $list = new DeviceDatHistory();
                $count = new DeviceDatHistory();
                $real_count = 0;
                foreach($sub_tables as $key => $value){
                    if($key == 0){
                        $list = $list->table($value);

                    }else{
                        $list = $list->union('select * from '.$value.' where '.$where_sql);
//                        $count = $count->union('select COUNT(`id`) AS tp_count from '.$value.' where '.$where_sql);
//                        $list = $list->union('select * from '.$value);
//                        $count = $count->union('select COUNT(`id`) AS tp_count from '.$value);
//                        $count = $count->table($value);
                    }
                    $count = $count->table($value);
                    $real_count += $count->where($where)->count('id');
                }
                $data['data']   = $list//$history->subTable($start_time_str, $end_time_str, $where_sql)
                ->where($where)
//                    ->page($page)
//                    ->order('ctime','desc')
//                    ->limit($limit)
//                    ->fetchSql()
                    ->select();
//                dump($data['data']);die;
                foreach ($data['data'] as $key => &$datum){
                    if(isset($configs[$datum['var_name']])){
                        $config = $configs[$datum['var_name']];
                        $datum['var_alias'] = $config['var_alias'];
                        $datum['gateway_name'] = $config['gateway_name'];
                        $datum['dev_name'] = $config['dev_name'];
                        $datum['partition_type'] = $config['partition_type'];
                    }else{
                        unset($data['data'][$key]);
                    }

                }
                $count = $real_count;//$count->where($where)->count('id');//->fetchSql()
//            dump($count);die;

                $data['count']  = $count;
            }


            $data['code']   = 0;

            /*series: [
                    {
                        name: '邮件营销',
                        type: 'line',
                        stack: '总量',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },]*/

            //折线图：1条线代表一个变量 一张图只代表一个监控点
            $data['x_data'] = [];//时间
            $data['legend'] = [];//线条
            $y_data = [];//y轴数据
            $index = 0;
            foreach ($data['data'] as $datum){
                if(!in_array($datum['refreshtime'],$y_data)){
                    $data['x_data'][] = $datum['refreshtime'];
                }
                if(!in_array($datum['var_alias'],$data['legend'])){
                    $data['legend'][] = $datum['var_alias'];
                    $y_data[$datum['var_name']] = [
                        'name' => $datum['var_alias'],
                        'type' => 'line',
                        'data' => [$datum['refreshtime'] => $datum['value']]
                    ];
                }else{
                    $y_data[$datum['var_name']]['data'][$datum['refreshtime']] = $datum['value'];
                }

            }//组装成了纵轴格式，但是时间可能对不上号
            foreach($data['x_data'] as $refreshtime){//给每个时间点都赋值
                foreach ($y_data as $var_name => $y_datum){
                    if(!isset($y_datum['data'][$refreshtime])){
                        $y_data[$var_name]['data'][$refreshtime] = 0;//默认值
                    }
                }
            }
            $data['y_data'] = [];
            foreach ($y_data as $var_name => $y_datum){//去掉键值
                $y_data[$var_name]['data'] = array_values($y_datum['data']);
                $data['y_data'][] =  $y_data[$var_name];
            }
            if($data['data']){
                $x_data = array_keys(array_flip(array_column($data['data']->toArray(),'refreshtime')));
                $data['x_data'] = $x_data;
            }
            return json($data);

        }else{
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d 23:59:59');
            $this->assign('formData', ['start_time'=>$start_time,'end_time'=>$end_time]);
        }

        //查询所有监控点
        $device_monitors = \app\site\model\DeviceMonitor::column('id,contorl_name');
        $this->assign('device_monitors', $device_monitors);
        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }


    public function export(){
        $is_excel = $this->request->param('is_excel',0,'intval');
        if($is_excel){
            $lists = $data['data'];
            $data = [];
            $head_arr = [
                'ID',
                '变量名',
                '记录值',
                '数据质量',
                '数据变化率',
                '时间',
                '记录方式',
                '区名',
                '组名',
                '设备名'
            ];
            foreach ($lists as $key=>$value){
                $data[] = [
                    'var_alias'=>$value['var_alias'],
                    'value'=>$value['value'],
                    'data_eq'=>$value['data_eq'],
                    'data_scale'=>$value['data_scale'],
                    'refreshtime'=>$value['refreshtime'],
                    'record_type' => $value['record_type']==1?'突变记录':($value['record_type']==2?'突变点记录':'定时记录'),
                    'partition_type'=>$value['partition_type'],
                    'gateway_name'=>$value['gateway_name'],
                    'dev_name'=>$value['dev_name']
                ];

            }
            $this->excelExport('用户使用情况',$head_arr,$data);
            exit;
        }else{
            $data['count']  = $history->where($where)->count('id');
            $data['code']   = 0;
            return json($data);
        }
    }

    /**
     * 自定义分表规则
     *
     * @param int $num
     * @return string
     */
    function myMod($start_time, $end_time)
    {
        if($start_time == $end_time){
            return '_'.$start_time;
        }else{
            return '';
        }
    }
}