<?php
/**
 * 监控管理
 * User: zhongjuhuang
 * Date: 2020/4/24
 * Time: 1:40 PM
 */

namespace app\site\admin;


use app\site\model\DeviceControlPoint;
use app\site\model\DeviceDatHistory;
use app\site\model\DeviceVarConfig;
use app\site\model\RealData;
use app\site\model\Site;
use app\system\admin\Admin;
use app\site\model\DeviceMonitor as Monitor;
use think\cache\driver\Redis;
use think\Db;
use GatewayClient\Gateway;
require_once __DIR__.'/../../../vendor/GatewayClient/Gateway.php';

class DeviceMonitor extends Admin
{
    protected $hisiModel = 'DeviceMonitor';//模型名称[通用添加、修改专用]
    protected $hisiTable = '';//表名称[通用添加、修改专用]
    protected $hisiAddScene = '';//添加数据验证场景名
    protected $hisiEditScene = '';//更新数据验证场景名
    protected $hisiValidate = 'DeviceMonitor';

    public function index(){
        $site_id    = $this->request->param('site_id/d');
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if($site_id){
                $where[] = ['site_id','=',$site_id];

            }
            if ($keyword) {
                $where[] = ['contorl_name', 'like', '%'.$keyword.'%'];
            }

            $data['data']   = Monitor::with('site')->where($where)->page($page)->limit($limit)->select();
            $data['count']  = Monitor::where($where)->count('id');
            $data['code']   = 0;
            return json($data);

        }
        $formData = [];
        if($site_id){
            $formData['site_id'] = $site_id;

        }
        //所有站点
        $sites = Site::where('is_show','=',1)->column('id,name');
        $this->assign('sites', $sites);
        $this->assign('formData', $formData);

        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }
    //组名查询：查所有
    public function gateway_select()
    {
        $partition_type =  $this->request->param('partition_type');
        $model = new DeviceVarConfig();
        $list = $model->where('type','=',1)->field('id,gateway_name,dev_name,var_name,var_alias,partition_type');
        if(!empty($partition_type)){
            $where['partition_type'] = $partition_type;
            $list = $list->where($where);
        }else{
            $list = $list->where('partition_type' , '<>','');
        }
        $list = $list->group('partition_type,gateway_name')
            ->select();
        return json(['status'=>1,'message'=>'','result'=>$list]);
    }
    //区名查询：查所有
    public function select_partition_type()
    {
        $model = new DeviceVarConfig();
        $list = $model->field('id,gateway_name,dev_name,var_name,var_alias,partition_type')
            ->where('type','=',1)
            ->where('partition_type','<>','')
            ->group('partition_type')
            ->select();
        return json(['status'=>1,'message'=>'','result'=>$list]);


    }
    //设备名查询：根据partition_type、gateway_name查询
    public function instrument_select()
    {
        $partition_type =  $this->request->param('partition_type');
        $gateway_name =  $this->request->param('gateway_name');
        $model = new DeviceVarConfig();
        $list = $model->field('id,gateway_name,dev_name,var_name,var_alias,partition_type');
        $list = $list->where('type','=',1)->where('partition_type' , '<>','');
        if(!empty($partition_type)){
//            $where['partition_type'] = $partition_type;
            $list = $list->where('partition_type',array_shift($partition_type),explode(',',$partition_type[0]));
        }
        if(!empty($gateway_name)){
//            $where['gateway_name'] = $gateway_name;
            $list = $list->where('gateway_name',array_shift($gateway_name),explode(',',$gateway_name[0]));
        }
        $list = $list->group('dev_name')
            ->select();
//        echo $model->getLastSql();die;
        return json(['status'=>1,'message'=>'','result'=>$list]);

    }
    //变量名查询：根据：partition_type、gateway_name、dev_name查询
    public function varlist_select(){
        //partition_type[] in 、gateway_name[]: in、dev_name[]: in查询
        $var_name =  $this->request->param('var_name');
        $partition_type =  $this->request->param('partition_type');
        $gateway_name =  $this->request->param('gateway_name');
        $dev_name =  $this->request->param('dev_name');
        $model = new DeviceVarConfig();
        $list = $model->field('id,gateway_name,dev_name,var_name,var_alias,partition_type');
        $list = $list->where('type','=',1)->where('partition_type' , '<>','');
        if(!empty($partition_type)){
            $list = $list->where('partition_type',$partition_type[0],explode(',',$partition_type[1]));
        }
        if(!empty($gateway_name)){
            $list = $list->where('gateway_name',$gateway_name[0],explode(',',$gateway_name[1]));
        }
        if(!empty($dev_name)){
            $list = $list->where('dev_name',$dev_name[0],explode(',',$dev_name[1]));
        }
        if($var_name){
            $list = $list->where('var_name','=',$var_name);
        }
        $list = $list//->group('partition_type,gateway_name,partition_type,dev_name,')
            ->select();
//        echo $model->getLastSql();die;
        return json(['status'=>1,'message'=>'','result'=>$list]);

    }
    //获取各个线路点的数据并显示
    public function realdata_select(){
        $contorl_id = $this->request->param('control_id');
        $var_names = $this->request->param('var_name');
        //var_code[]: in
        $model = new RealData();
        //select * from mod_device_control_point where control_id in (xxx,xx) group by
        $list = $model->where('contorl_id','=',$contorl_id)
            ->where('var_name','in',$var_names)
            ->select();//怎么查询最新一批

        return json(['status'=>1,'message'=>'','result'=>$list]);
        return json_decode('{"status":1,"result":[{"id":134,"var_code":"SS_AI2","value":"16384","refreshtime":"2020-05-06 08:27:23","data_eq":"0","data_scale":"0"},{"id":132,"var_code":"AI2","value":"0","refreshtime":"2020-05-18 09:14:21","data_eq":"1","data_scale":"0"},{"id":131,"var_code":"AI1","value":"1.813931","refreshtime":"2020-05-18 18:03:56","data_eq":"0","data_scale":"0.001822"},{"id":107,"var_code":"S.V.TJC_FQ1","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":106,"var_code":"S.V.GFJ2_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":105,"var_code":"S.V.HXXJSF_OT","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":102,"var_code":"S.V.HXF_OT","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":99,"var_code":"S.V.CSF_OT","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":97,"var_code":"S.V.HXB_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":95,"var_code":"S.V.GFJ_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":93,"var_code":"S.V.WNB2_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":91,"var_code":"S.V.WNB1_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":87,"var_code":"S.V.CXB1_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":85,"var_code":"S.V.TSB2_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"},{"id":83,"var_code":"S.V.TSB1_ING","value":"0","refreshtime":"2020-04-11 17:16:30","data_eq":"0","data_scale":"0"}],"count":15,"total":15,"message":"查询成功！","url":"http:\/\/zijin.demo.zhaota8.com\/admin\/login.html"}', true);
    }
    //静态
    public function contorlpoint_add(){
        return json_decode('{"status":1,"result":"32","message":"新增成功","url":"http:\/\/zijin.demo.zhaota8.com\/admin\/index.html"}', true);
    }
    //不需要
    public function contorlpoint_select(){
        $tmp = '{"status":1,"columns":[{"COLUMN_NAME":"id","ORDINAL_POSITION":1,"COLUMN_DEFAULT":null,"IS_NULLABLE":"NO","DATA_TYPE":"int","CHARACTER_MAXIMUM_LENGTH":null,"COLUMN_COMMENT":"","COLUMN_KEY":"PRI","EXTRA":"auto_increment"},{"COLUMN_NAME":"title","ORDINAL_POSITION":2,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":50,"COLUMN_COMMENT":"监控点-标题","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"contorl_id","ORDINAL_POSITION":3,"COLUMN_DEFAULT":"0","IS_NULLABLE":"YES","DATA_TYPE":"int","CHARACTER_MAXIMUM_LENGTH":null,"COLUMN_COMMENT":"监控主表ID","COLUMN_KEY":"MUL","EXTRA":""},{"COLUMN_NAME":"var_name","ORDINAL_POSITION":4,"COLUMN_DEFAULT":null,"IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":255,"COLUMN_COMMENT":"变量名称","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"x","ORDINAL_POSITION":5,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"X坐标 百分比","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"y","ORDINAL_POSITION":6,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"Y坐标 百分比","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_x","ORDINAL_POSITION":7,"COLUMN_DEFAULT":"0","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"数值的x轴坐标","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_y","ORDINAL_POSITION":8,"COLUMN_DEFAULT":"0","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"数值的y轴坐标","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_color","ORDINAL_POSITION":9,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"数值-字体颜色","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_size","ORDINAL_POSITION":10,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"数值-字体大小","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_img","ORDINAL_POSITION":11,"COLUMN_DEFAULT":"green","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":50,"COLUMN_COMMENT":"红绿图片","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"sj_val_x","ORDINAL_POSITION":12,"COLUMN_DEFAULT":"0","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":50,"COLUMN_COMMENT":"数据 X坐标 百分比","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"sj_val_y","ORDINAL_POSITION":13,"COLUMN_DEFAULT":"0","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":50,"COLUMN_COMMENT":"数据 Y标 百分比","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"sj_val_color","ORDINAL_POSITION":14,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":50,"COLUMN_COMMENT":"","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"sj_val_size","ORDINAL_POSITION":15,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":50,"COLUMN_COMMENT":"","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_critical","ORDINAL_POSITION":16,"COLUMN_DEFAULT":"0","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":25,"COLUMN_COMMENT":"红绿-临界值","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_judge","ORDINAL_POSITION":17,"COLUMN_DEFAULT":"<=","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":20,"COLUMN_COMMENT":"临界时判断方式","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"unit","ORDINAL_POSITION":18,"COLUMN_DEFAULT":"","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":5,"COLUMN_COMMENT":"数值单位","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"is_show","ORDINAL_POSITION":19,"COLUMN_DEFAULT":"1","IS_NULLABLE":"YES","DATA_TYPE":"tinyint","CHARACTER_MAXIMUM_LENGTH":null,"COLUMN_COMMENT":"是否显示","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"val_img_two","ORDINAL_POSITION":20,"COLUMN_DEFAULT":"red","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":50,"COLUMN_COMMENT":"红绿图片","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"deletetime","ORDINAL_POSITION":21,"COLUMN_DEFAULT":"0000-00-00 00:00:00","IS_NULLABLE":"YES","DATA_TYPE":"datetime","CHARACTER_MAXIMUM_LENGTH":null,"COLUMN_COMMENT":"","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"updatetime","ORDINAL_POSITION":22,"COLUMN_DEFAULT":null,"IS_NULLABLE":"YES","DATA_TYPE":"datetime","CHARACTER_MAXIMUM_LENGTH":null,"COLUMN_COMMENT":"","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"admin_id","ORDINAL_POSITION":23,"COLUMN_DEFAULT":null,"IS_NULLABLE":"YES","DATA_TYPE":"int","CHARACTER_MAXIMUM_LENGTH":null,"COLUMN_COMMENT":"","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"img_size","ORDINAL_POSITION":24,"COLUMN_DEFAULT":"10","IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"","COLUMN_KEY":"","EXTRA":""},{"COLUMN_NAME":"flo_num","ORDINAL_POSITION":25,"COLUMN_DEFAULT":null,"IS_NULLABLE":"YES","DATA_TYPE":"varchar","CHARACTER_MAXIMUM_LENGTH":10,"COLUMN_COMMENT":"小数位数","COLUMN_KEY":"","EXTRA":""}],"result":[{"id":32,"title":"2#鼓风机故障","contorl_id":33,"var_name":"GFJ2_GZ","x":"210","y":"280","val_x":"200","val_y":"200","val_color":"#000000","val_size":"12","sj_val_x":"200","sj_val_y":"240","sj_val_color":"#000000","sj_val_size":"12","flo_num":"2","val_img":"#FF0000","val_img_two":"#81D115","val_critical":"0","val_judge":"<","is_show":1,"unit":"","deletetime":"0000-00-00 00:00:00","updatetime":"2020-04-25 16:19:30","admin_id":26,"img_size":"10","var_alias":"2#鼓风机故障"}],"count":1,"total":1,"message":"查询成功！","url":"http:\/\/zijin.demo.zhaota8.com\/admin\/login.html"}';
        return json_decode($tmp,true);
    }

    public function editor(){

        return $this->fetch();
    }

    public function saveEdit(){
        $postData = $this->request->post();
        if(isset($postData['point_list'])){
            $point_list = $postData['point_list'];
            unset($postData['point_list']);
        }else{
            $point_list = [];
        }


        $model = $this->model();
        $result = $this->validate($postData, $this->hisiValidate);
        if ($result !== true) {
            return $this->error($result);
        }
        $pk = $model->getPk();
        $id = $this->request->param($pk);
        if($id){
            $where[]= [$pk, '=', $id];
            $where  = $this->getRightWhere($where);

            if ($this->request->isPost()) {

                if ($model->save($postData, $where) === false) {
                    return $this->error($model->getError());
                }
                if($point_list){

                    $new_list = [];
                    $old_list = [];
                    $point_model = new DeviceControlPoint();
                    $del_list = $point_model->where(['contorl_id'=>$id])->column('id,contorl_id,title');//->field('id,contorl_id,title')->select();//
                    foreach ($point_list as &$point){
                        $point['val_judge'] = htmlspecialchars_decode($point['val_judge']);
                        if(isset($point['id'])){
                            $old_list[] = $point;
                            unset($del_list[$point['id']]);
                        }else{
                            $point['contorl_id'] = $id;
                            $point['admin_id'] = ADMIN_ID;
                            $new_list[] = $point;
                        }

                    }

                    if($new_list){
                        $point_model = new DeviceControlPoint();
                        $rs = $point_model->saveAll($new_list);
                        if(!$rs){
                            return $this->error($point_model->getError());
                        }
                    }

                    if($old_list){
                        $point_model = new DeviceControlPoint();
                        foreach($old_list as $old_item){
                            unset($old_item['ctime']);
                            unset($old_item['mtime']);
                            unset($old_item['dtime']);
                            $rs = $point_model->update($old_item, ['id'=>$old_item['id']]);
                            if(!$rs){
                                return $this->error($point_model->getError());
                            }
                        }
                    }
                    return $this->error('','',$old_list);
                    if($del_list){
//                            $point_model = new DeviceControlPoint();
//                            $del_rs = $point_model->where('id','in', array_keys($del_list))->delete();
                        $del_rs = DeviceControlPoint::destroy(function($query)use($del_list){
                            $query->where('id','in', array_keys($del_list));
                        });
                        if(!$del_rs){
                            return $this->error($point_model->getError());
                        }
                    }
                    return $this->success('保存成功', '');
                }
//                    return $this->success('保存成功', '');
            }
        }else{
            if (!$model->allowField(true)->save($postData)) {
                return $this->error($model->getError());
            }
            $id = $model->id;
            if($point_list){//批量添加
                foreach ($point_list as &$point){
                    $point['contorl_id'] = $id;
                    $point['admin_id'] = ADMIN_ID;
                }
                $point_model = new DeviceControlPoint();
                $rs = $point_model->saveAll($point_list);
                if(!$rs){
                    return $this->error($point_model->getError());
                }
                return $this->success('保存成功', '');
            }
        }


//            $formData = $model->where($where)->find();

        return $this->success('保存成功', '');
    }

    /**
     * [通用方法]编辑页面展示和保存
     * @author 橘子俊 <364666827@qq.com>
     * @return mixed
     */
    public function edit()
    {
        $where = [];
        if ($this->hisiModel) {// 通过Model更新

            $model = $this->model();

            $pk = $model->getPk();
            $id = $this->request->param($pk);

            $where[]= [$pk, '=', $id];
//            $where  = $this->getRightWhere($where);


            $formData = $model->where($where)->find();

        }

        if (!$formData) {
            return $this->error('数据不存在或没有权限');
        }
        $point_model = new DeviceControlPoint();
        $data_list = $point_model->where(['contorl_id'=>$id])->select();

        $this->assign('formData', $formData);
        $this->assign('data_list', $data_list);

        $template = $this->request->param('template', 'form');
        //所有站点
        $sites = Site::where('is_show','=',1)->column('id,name');
        $this->assign('sites', $sites);
        return $this->fetch($template);
    }

    public function add()
    {
        if (!$this->request->isPost()) {
            //所有站点
            $sites = Site::where('is_show','=',1)->column('id,name');
            $this->assign('sites', $sites);
        }
        return parent::add();
    }

    public function show(){
        $id = $this->request->param('id');
        $point_model = new DeviceControlPoint();
        $data_list = $point_model->where(['contorl_id'=>$id])->select();
        $model = new Monitor();
        $formData = $model->where(['id'=>$id])->find();
        $this->assign('formData', $formData);
        $this->assign('data_list', $data_list);
        $var_name_list =  array_column($data_list->toArray(),'var_name');
        $this->assign('var_name_list', $var_name_list);
        $btn_list = [//寻址->变量名称
            '1' => '上升',
            '2' => '下降',
            '3' => '打开',
            '4' => '关闭',
        ];
        $this->assign('control_list', $btn_list);//todo 需跟沈工约定操作的操作符字节是多少
        if(!empty($var_name_list)){
            $var_config = DeviceVarConfig::where('type','=',1)
                ->where('var_name','in', $var_name_list)
                ->column('var_name,data_type');
        }else{
            $var_config = [];
        }
        $this->assign('name_type', $var_config);
        //写变量列表
        $write_var_list = DeviceVarConfig::where('type','=',2)->select();
        $this->assign('write_var_list', $write_var_list);
        return $this->fetch();
    }
    //测试向对应的客户端发送消息
    public function addControl(){
/*=== 如果GatewayClient和GatewayWorker不在同一台服务器需要以下步骤 ===
* 1、需要设置start_gateway.php中的lanIp为实际的本机内网ip(如不在一个局域网也可以设置成外网ip)，设置完后要重启GatewayWorker
* 2、GatewayClient这里的Gateway::$registerAddress的ip填写填写上面步骤1lanIp所指定的ip，端口
* 3、需要开启GatewayWorker所在服务器的防火墙，让以下端口可以被GatewayClient所在服务器访问，
*    端口包括Rgister服务的端口以及start_gateway.php中lanIp与startPort指定的几个端口*/
        Gateway::$registerAddress = config('device.GATEWAY_REGISTER_ADDRESS');//'127.0.0.1:1238';//

        //{"7f0000010c1f00000001":{"17uid3":"7f0000010c1f00000001"},"7f0000010c2200000001":{"8uid3":"7f0000010c2200000001"}}
        /*$all_client_session = Gateway::getAllClientSessions();
        $uid_clientid_arr = [];
        foreach($all_client_session as $client_id => $session){
            $uid_clientid_arr[$session['uid']] = $client_id;
        }
        dump($uid_clientid_arr);
        echo json_encode($all_client_session);
        $client_id = $uid_clientid_arr[$device_id];//Gateway::getClientIdByUid($device['id']);
        */
        $device_id = $this->request->param('id');
        $post_data = $this->request->post();
        $model = new Monitor();
        $device = $model->where(['id'=>$device_id])->find();
        $client_id = $device['client_id'];
        $control_name = $post_data['control_value'];//*1
//        $var_name = $post_data['var_name'];
        $var_id = $post_data['var_id'];
        $write_code = config('device.WRITE_CODE');
        if($client_id && Gateway::isOnline($client_id)){
            $msg = $write_code['start_code'];//$device['start_code'];
//            $msg .= strToHex($var_name);//两个字节的地址
            $var_config = DeviceVarConfig::where('type','=',2)
                ->where('id','=',$var_id)
                ->find();
            $var_name = str_replace(' ','',$var_config['var_name']);
            $msg .= sprintf('%04X',$var_name);
            if($var_config['data_type'] == 'Float'){//4字节
                $msg .= '04';
                $msg .= floatToHex($control_name);
            }else{//if($var_config['data_type'] == 'Word')//其余均为2字节
//            if(is_int($control_name)){
                $msg .= '02';//字节长度
                //对应长度字节的操作
                $msg .= intToHex($control_name);
            }
            $msg .= $write_code['end_code'];
//            $msg .= $device['end_code'];
            $ascii = '';
            $msg = strtoupper($msg);//全部转大写
            for($i=0; $i<strlen($msg)/2; $i++){
                $ascii .= chr(hexdec($msg[$i].$msg[$i+1]));//对方实际接收的是$ascii
            }

            $result = Gateway::sendToClient($client_id, $msg.':'.$ascii);
//            dump($result);
            $this->success('操作成功');
        }else{
            return $this->error('设备不在线，请检查后再操作');
        }

    }

    /**
     * 变量配置表
     */
    public function varConfigList(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');
            $type    = $this->request->param('type/d');
            if ($keyword) {
                $where[] = ['var_alias', 'like', '%'.$keyword.'%'];
            }
            if($type){
                $where[] = ['type','=', $type];
            }

            $data['data']   = DeviceVarConfig::with('site')->where($where)->page($page)->limit($limit)->select();
            $data['count']  = DeviceVarConfig::where($where)->count('id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function editConfig(){
        if (!$this->request->isPost()) {
            $site = Site::select();
            $this->assign('site', $site);
        }else{
            $postData = $this->request->post();
            foreach ($postData as $key => $value){
                if($value === ''){
                    $postData[$key] = null;
                }
            }
            $id = $postData['id'];
            $model = new DeviceVarConfig();
            $where[]= ['id', '=', $id];
            if(!$model->checkVarname($postData['var_name'], $postData['type'], $id)){
                return $this->error($model->getError());
            }
            if ($model->save($postData, $where) === false) {
                return $this->error($model->getError());
            }

            return $this->success('保存成功', '');
        }
        $this->hisiModel = 'DeviceVarConfig';
        $this->hisiValidate = 'DeviceVarConfig';
        $this->template = 'var_config_form';
        return parent::edit();
    }

    public function addConfig(){
        if (!$this->request->isPost()) {
            $site = Site::select();
            $this->assign('site', $site);
        }else{
            $model = new DeviceVarConfig();
            $postData = $this->request->post();
            foreach ($postData as $key => $value){
                if($value === ''){
                    $postData[$key] = null;
                }
            }
            if(!$model->checkVarname($postData['var_name'], $postData['type'])){
                return $this->error($model->getError());
            }
            if (!$model->save($postData)) {
                return $this->error($model->getError());
            }
            return $this->success('保存成功', '');
        }
        $this->hisiModel = 'DeviceVarConfig';
        $this->hisiValidate = 'DeviceVarConfig';
        $this->template = 'var_config_form';
        return parent::add();
    }

    //变量配置考虑到时候做一个excel导入功能

    public function testSave(){
        $message = '55 02 01 6d 41 bb 99 9a 3c 01 6d 3c 01 6d 41 bb 99 9a 3c 01 6d 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb 99 9a 41 bb  99 9a 41 bb  99 9a 41 bb  99 9a 41 bb  99 9a 41 bb  99 9a 41 8a 66';
        if(strlen($message)<2 ){//是否还要验证头尾
            return false;
        }
        $ascii = '';
        $hex = '';
        for($i=0;$i<strlen($message);$i++){
            $tmp_ascii = ord($message[$i]);//字符转10进制
            $ascii .= $tmp_ascii.' ';
            /*$tmp_hex = dechex($tmp_ascii);//10进制转16进制
            if( hexdec($tmp_hex)<=0x0f){
              $hex .= '0'.$tmp_hex.' ';
            }else{
              $hex .= $tmp_hex.' ';
            }*/
            $hex .= sprintf("%02x", $tmp_ascii).' ';//10进制转16进制

        }
        $hex = $message;//线上需注释 todo
        $result = handelMsg(trim($hex),18,19,10);//根据规则处理成对应的变量名和值
        //头尾用来做设备标识
//        $start_code = $result['LB0'];
//        $end_code = $result['LB'.(strlen($message)-1)];


        //将结果保存到数据库:保存变量名
//        $rs = self::saveVarConfig($result);//一次性保存
//        echo '数据库插入结果：'.$rs."\r\n";
        $client_id = '7f0000010c1f00000001';
        $rs = self::saveHistory($message, $hex, $result, $client_id);
    }

    //保存一次请求的记录
    public static function saveHistory($original_data, $original_hex_data, $result_data,$client_id){
        //1、更新mod_real_data记录
        //2、添加device_dat_history
        //3、原始数据保存：device_history


        //判断值是否跟最近一条一样，一样的化不插入，做时间更新
        $time = time();
        $history_data = [
            'original_data' => $original_data,
            'original_hex_data' => $original_hex_data,
            'data' => json_encode($result_data),
            'ctime' => $time,
            'mtime' => $time,
            'client_id' => $client_id,
//            'uid' => 0,//todo
            'start_code' => array_shift($result_data),//$result_data['LB0'],
            'end_code' => array_pop($result_data),
            'unique_code' => $original_hex_data,
            'check_code' => $result_data['check_code'],
            'unique_code' => $result_data['unique_code'],
        ];
        $device = Db::name('device_monitor')
//            ->where('start_code','=',$history_data['start_code'])
//            ->where('end_code','=',$history_data['end_code'])
            ->where('unique_code', '=', hexdec($history_data['unique_code']))
            ->find();//不使用头尾标识，使用端口标识
        if($device){
            if($device['client_id'] != $client_id){
                $rs = Db::name('device_monitor')->where('id','=',$device['id'])->update(['client_id'=>$client_id]);
                if(!$rs){
                    echo $client_id.'和设备'.$device['id'].'绑定失败,';
                }
            }
//            $_SESSION['device'] = $device;
//            Gateway::bindUid($client_id, $device['id']);//设备ID和客户端ID做绑定:绑定了在客户端那边也不能用

            //具体节点历史记录
            $data_list = Db::name('device_control_point')->where(['contorl_id'=>$device['id']])->select();
            $var_names = array_column($data_list,'var_name');
            echo json_encode($var_names);
            $configs = Db::name('device_var_config')
//                        ->where('site_id', '=', $device['site_id'])
                ->where('var_name','in',$var_names)
                ->where('type','=',1)//读变量
                ->column('var_name,var_alias,gateway_name,dev_name,partition_type,min,max,trigtime,sc_trig,lim_LL,lim_L,lim_H,lim_HH');
            //变化触发阈值\低低报警\低报警\高报警\高高报警
            $insert_point_list = [];
            $points = [];
            foreach ($data_list as $value){

                $insert_point_list[$value['var_name']] = [//还需区分是那条线路的，哪个
                    'var_name' => $value['var_name'],
                    'value' => $result_data[$value['var_name']],
                    'refreshtime' => date('Y-m-d H:i:s', $time),
                    'contorl_id'=> $value['contorl_id'],
                    'point_id' => $value['id'],
                    'data_eq' => 0,//数据质量
                    'data_scale' => '',//数据变化率
                    'record_type' => 0,//数据变化率
                    'ctime' => $time,
                    'mtime' => $time,
                ];
                $value['value'] = $result_data[$value['var_name']];
                $points[$value['var_name']] = $value;//$result_data[$value['var_name']];
            }

            $real_list = Db::name('real_data')
                ->where('contorl_id','=', $device['id'])
                ->where('var_name','in',array_keys($points))
                ->select();
            if($real_list){
                foreach($real_list as $old_data){
                    $new_val = $points[$old_data['var_name']]['value'];
                    $config = $configs[$old_data['var_name']];
                    $data_scale = 0;
                    $data_eq = 0;
                    if($config['lim_LL']!==null && $new_val<$config['lim_LL']){
                        $data_eq = 1;
                    }elseif($config['lim_L']!==null && $new_val<$config['lim_L']){
                        $data_eq = 2;
                    }elseif($config['lim_H']!==null && $new_val>$config['lim_H']){
                        $data_eq = 3;
                    }elseif($config['lim_HH']!==null && $new_val>$config['lim_HH']){
                        $data_eq = 4;
                    }
                    if($old_data['value'] != $new_val){
                        if($old_data['value'] != 0){
                            $data_scale = ($new_val-$old_data['value'])/$old_data['value'];
                        }else{
                            $data_scale = $new_val;
                        }
                    }
                    $record_type = 0;
                    $tmp_data = [
                        'value' => $new_val,
                        'refreshtime' => date('Y-m-d H:i:s', $time),
                        'data_eq' => $data_eq,//数据质量:0、正常 1、低低报警2、低报警3、高报警4、高高报警
                        'data_scale' => $data_scale,//数据变化率
                        'record_type' => $record_type,//记录方式：0、定时记录1、突变记录2、突变点记录
                    ];
                    //记录device_dat_history数据
                    $insert_point_list[$old_data['var_name']]['data_scale'] = $data_scale;
                    $insert_point_list[$old_data['var_name']]['data_eq'] = $data_eq;
                    $insert_point_list[$old_data['var_name']]['record_type'] = $record_type;
                    $rs = Db::name('real_data')->where('id','=',$old_data['id'])->update($tmp_data);
                    if(!$rs){
                        echo $client_id.'real_data数据更新失败,';
                    }
                    unset($points[$old_data['var_name']]);
                }
            }
            $model = new DeviceDatHistory();
            $table = $model->setTable();
            if(!empty($insert_point_list)){
                $point_list = array_values($insert_point_list);
                $rs = $table->insertAll($point_list);//Db::name('device_dat_history')
                if(!$rs){
                    echo $client_id.'device_dat_history数据更新失败,';
                }
            }
            if($points){
                $insert_data = [];
//                $var_names = array_keys($points);

                foreach ($points as $var_name => $value){
                    $config = $configs[$value['var_name']];
                    $insert_data[] = [
                        'site_id' => $device['site_id'],//
                        'contorl_id' => $value['contorl_id'],//
                        'group_name' => $config['gateway_name'],//
                        'dev_name' => $config['dev_name'],//
                        'var_alias' => $config['var_alias'],
                        'value' => $value['value'],
                        'var_name' => $var_name,
                        'refreshtime' => date('Y-m-d H:i:s', $time),
                        'data_eq' => '',//数据质量
                        'data_scale' => 0,//数据变化率
                    ];
                }
                $rs = Db::name('real_data')->insertAll($insert_data);
                if(!$rs){
                    echo $client_id.'real_data数据插入失败,';
                }
            }

            return $rs;

        }else{
            echo '设备找不到';
            return false;
        }

//        return Db::name('device_history')->insert($history_data);//这个不需要
    }

    public function test(){
        $mode = new DeviceDatHistory();
        $table = $mode->setTable();
        dump($table->select());
        /*$redis = new Redis();
        $redis->connect('127.0.0.1', 6379);
        $redis->lpush("tutorial-list", "Redis");
        $redis->set('gateway_worker_add_msg','update');
        $rs = $redis->get('gateway_worker_add_msg');
        dump($redis->lpop("tutorial-list"));
        dump($redis->lrange( 'tutorial-list',0,5 ));
        dump($rs);die;*/
    }
}