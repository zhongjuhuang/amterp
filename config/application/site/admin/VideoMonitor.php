<?php
/**
 * 监控管理
 * User: zhongjuhuang
 * Date: 2020/4/24
 * Time: 1:40 PM
 */

namespace app\site\admin;


use app\system\admin\Admin;

class VideoMonitor extends Admin
{
    protected $hisiModel = 'VideoMonitor';//模型名称[通用添加、修改专用]
    protected $hisiTable = '';//表名称[通用添加、修改专用]
    protected $hisiAddScene = '';//添加数据验证场景名
    protected $hisiEditScene = '';//更新数据验证场景名
    protected $hisiValidate = 'VideoMonitor';
    //Device Monitor


}