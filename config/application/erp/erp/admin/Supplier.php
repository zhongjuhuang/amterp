<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/8
 * Time: 9:05 AM
 */

namespace app\erp\admin;


use app\erp\model\SupplierCategory;
use app\system\admin\Admin;
use \app\erp\model\Supplier as SupplierModel;

class Supplier extends Admin
{
    protected $hisiModel = 'Supplier';
    protected $hisiValidate = 'Supplier';
    public function index(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['supplier_name', 'like', '%'.$keyword.'%'];
            }
            $goods = SupplierModel::with('category')->where($where)->page($page)->limit($limit)->select();
            foreach ($goods as &$good){
                $good['supplier_category_name'] = $good['category']['supplier_category_name'];
                $good['telephone_phone'] = $good['supplier_telephone'].'/'.$good['supplier_phone'];
            }
            $data['data']   = $goods;
            $data['count']  = SupplierModel::where($where)->count('supplier_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function add(){
        if (!$this->request->isPost()) {
            $category = SupplierCategory::getSelect(SupplierCategory::getAll());
            $this->assign('category', $category);
        }
        return parent::add();
    }

    public function edit()
    {
        if (!$this->request->isPost()) {
            $category = SupplierCategory::getSelect(SupplierCategory::getAll());
            $this->assign('category', $category);
        }
        return parent::edit();
    }

    //商品分类
    public function category(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['supplier_category_name', 'like', '%'.$keyword.'%'];
            }
            $goods = SupplierCategory::where($where)->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = SupplierCategory::where($where)->count('supplier_category_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function addCategory(){

        $this->hisiModel = 'SupplierCategory';
        $this->hisiValidate = 'SupplierCategory';
        $this->template = 'category_form';
        return parent::add();
    }

    public function editCategory(){
        $this->hisiModel = 'SupplierCategory';
        $this->hisiValidate = 'SupplierCategory';
        $this->template = 'category_form';
        return parent::edit();
    }

    public function delCategory(){
        $this->hisiModel = 'SupplierCategory';
        return parent::del();
    }
}