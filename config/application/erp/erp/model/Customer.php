<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class Customer extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'customer_id';

    public function category()
    {
        return $this->hasOne('CustomerCategory','customer_category_id','customer_category_id');
    }
}