<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class Brand extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'brand_id';

    public static function getAll(){
        $map = [

        ];
        $data = self::where($map)->column('brand_id,brand_name,brand_code');
        return array_values($data);;
    }

    /**
     * 将数据集格式化成下拉选项
     * @param int $id 选中的ID
     * @author zhongjuhuang
     * @return string
     */
    public static function getSelect($data = [], $id = 0)
    {
        if (empty($data)) {
            return '';
        }
        $str = $separ = '';

        foreach ($data as $k => $v) {
            if ($id == $v['brand_id']) {
                $str .= '<option value="'.$v['brand_id'].'" selected>'.$separ.$v['brand_name'].'</option>';
            } else {
                $str .= '<option value="'.$v['brand_id'].'">'.$separ.$v['brand_name'].'</option>';
            }
        }
        return $str;
    }

}