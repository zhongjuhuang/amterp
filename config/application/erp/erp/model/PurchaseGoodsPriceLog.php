<?php
/**
 * 采购商品价格记录
 * User: zhongjuhuang
 * Date: 2020/5/11
 * Time: 10:43 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class PurchaseGoodsPriceLog extends Model
{

    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'price_log_id';

    /**
     * 添加采购商品价格记录
     * @param array $data
     * @param int $pOrderId
     * @param $insertTime
     * @return bool
     */
    public static function addPurchaseGoodsPriceLog($data, $pOrderId, $insertTime)
    {
        if(empty($data)) return false;
        $insertData = [];
        foreach ($data as $goodsValue) {
            $tmp = [
                'goods_id' => $goodsValue['goods_id'],
                'goods_price' => $goodsValue['p_goods_price'],
                'p_order_id' => $pOrderId,
                'log_time' => $insertTime,
            ];
            $insertData[] = $tmp;
        }
        $model = new PurchaseGoodsPriceLog();
        return $model->saveAll($insertData);
    }
}