<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class Customer extends Validate
{
    //定义验证规则
    protected $rule = [
        'customer_name|客户名称' => 'require',
//        'customer_code|商品编码' => 'require',
        'province' => 'require',
        'city' => 'require',
        'customer_category_id|客户分类' => 'require|number',
    ];
}