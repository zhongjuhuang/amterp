<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class Supplier extends Validate
{
    //定义验证规则
    protected $rule = [
        'supplier_name|供应商名称' => 'require',
//        'supplier_code|商品编码' => 'require',
        'province' => 'require',
        'city' => 'require',
        'supplier_category_id|供应商分类' => 'require|number',
    ];
}