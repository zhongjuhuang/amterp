<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class PurchaseOrder extends Validate
{
    //定义验证规则
    protected $rule = [
        'p_order_sn|采购单号' => 'require',
        'supplier_id|供应商' => 'require|number',
        'payment_type|付款方式' => 'require',
        'supplier_contacts|供应商联系人' => 'require',
    ];
}