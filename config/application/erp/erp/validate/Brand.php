<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class Brand extends Validate
{
    //定义验证规则
    protected $rule = [
        'brand_name|品牌名称' => 'require',
        'brand_code|品牌编号' => 'require',
    ];
}