<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/28
 * Time: 3:30 PM
 */

namespace app\erp\admin;


use app\erp\model\AccountsReceivable;
use app\erp\model\Goods;
use app\erp\model\SalesGoodsPriceLog;
use app\erp\model\SalesOperLog;
use app\erp\model\SalesOrder;
use app\erp\model\SalesOrderGoods;
use app\erp\model\SalesSendOrder;
use app\erp\model\SalesSendWarehouseGoods;
use app\erp\model\Unit;
use app\erp\model\Warehouse;
use app\erp\model\WarehouseGoods;
use app\system\admin\Admin;
use think\Db;

class Sales extends Admin
{
    protected $hisiModel = 'SalesOrder';
//    protected $hisiValidate = 'SalesOrder';
    public function index(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['sales_order_sn', 'like', '%'.$keyword.'%'];
            }
            $goods = SalesOrder::with('customer')
                ->where($where)
                ->page($page)
                ->limit($limit)
                ->order('sales_order_id','desc')
                ->select();
            $data['data']   = $goods;
            $data['count']  = SalesOrder::where($where)->count('sales_order_id');
            $data['code']   = 0;
            return json($data);

        }



        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }

    public function add(){
        if ($this->request->isPost()) {
            $postData = $this->request->post();
            if(!isset($postData['goodsId'])){
                return $this->error('请选择商品');
            }
            $goodsId = $postData['goodsId'];
            $goodsPrice = $postData['goodsPrice'];
            $goodsBuyNum = $postData['goodsBuyNum'];
            $goodsTax = $postData['goodsTax'];
            $goodsAmount = $postData['goodsAmount'];
            $result = $this->validate($postData, $this->hisiValidate);
            if ($result !== true) {
                return $this->error($result);
            }
            Db::startTrans();

            $model = new SalesOrder();

            //加商品
            $goods_data = [];
            $p_order_goods_amount = 0;
            $p_order_tax_amount = 0;
            $p_order_amount = 0;
            foreach ($goodsId as $key => $gid){
                if($goodsBuyNum[$key] <= 0){
                    Db::rollback();
                    return $this->error('采购商品数量不能为0');
                }
                $goods = Goods::get($gid);
                $unit = Unit::get($goods['unit_id']);
                $tmp_data = [
                    'goods_id' => $gid,
                    'goods_name' => $goods['goods_name'],
                    'goods_number' => $goods['goods_number'],
                    'goods_spec' => $goods['goods_spec'],
                    'goods_unit' => $unit['unit_name'],
                    'sales_goods_sell_num' => $goodsBuyNum[$key],
                    'sales_goods_price' => $goodsPrice[$key],
                    'sales_goods_tax' => $goodsTax[$key],
                    'sales_goods_amount' => $goodsBuyNum[$key]*$goodsPrice[$key]+$goodsTax[$key],
                    'sales_goods_info' => ''
                ];
                $p_order_amount += $tmp_data['sales_goods_amount'];
                $p_order_tax_amount += $tmp_data['sales_goods_tax'];
                $p_order_goods_amount += $tmp_data['sales_goods_sell_num']*$tmp_data['sales_goods_price'];
                $goods_data[] = $tmp_data;
            }
            $up_data = $postData;
            $up_data['sales_order_amount'] = $p_order_amount;
            $up_data['sales_order_tax_amount'] = $p_order_tax_amount;
            $up_data['sales_order_goods_amount'] = $p_order_goods_amount;
            $rs = $model->save($up_data);
            if (!$rs) {
                Db::rollback();
                return $this->error($model->getError());
            }
            if(!empty($goods_data)){
                $g_model = new SalesOrderGoods();
                foreach ($goods_data as &$goods_datum){
                    $goods_datum['sales_order_id'] = $model->sales_order_id;
                }
                $rs = $g_model->saveAll($goods_data);
                if(!$rs){
                    Db::rollback();
                    return $this->error($g_model->getError());
                }
            }
            Db::commit();
            return $this->success('保存成功', '');
        }else{
            //供应商
            $customers = \app\erp\model\Customer::select();
            $this->assign('customers', $customers);
            $this->assign('goods', []);
            $this->assign('sales_order_sn', $this->_createNo('S'));
            $this->assign('receivable_code', config('erp.RECEIVABLE_CODE'));
            return parent::add();
        }
    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $postData = $this->request->post();
            $sales_order_id = $postData['sales_order_id'];
            $goodsId = $postData['goodsId'];
            $pGoodsId = $postData['pGoodsId'];
            $goodsPrice = $postData['goodsPrice'];
            $goodsBuyNum = $postData['goodsBuyNum'];
            $goodsTax = $postData['goodsTax'];

//            $goodsAmount = $postData['goodsAmount'];
            $result = $this->validate($postData, $this->hisiValidate);
            if ($result !== true) {
                return $this->error($result);
            }
            Db::startTrans();

            //加商品
            $old_data = SalesOrderGoods::where(['sales_order_id'=>$sales_order_id])->column('sales_goods_id');
            $new_data = [];//new
//            $update_data = [];
            foreach ($pGoodsId as $key => $gid){
                if($goodsBuyNum[$key] <= 0){
                    Db::rollback();
                    return $this->error('销售商品数量不能为0');
                }
                $goods = Goods::get($goodsId[$key]);
                $unit = Unit::get($goods['unit_id']);
                $temp_data = [
                    'goods_id' => $goodsId[$key],
                    'goods_name' => $goods['goods_name'],
                    'goods_number' => $goods['goods_number'],
                    'goods_spec' => $goods['goods_spec'],
                    'goods_unit' => $unit['unit_name'],
                    'sales_goods_sell_num' => $goodsBuyNum[$key],
                    'sales_goods_price' => $goodsPrice[$key],
                    'sales_goods_tax' => $goodsTax[$key],
                    'sales_goods_amount' => $goodsBuyNum[$key]*$goodsPrice[$key]+$goodsTax[$key],
                    'sales_goods_info' => ''
                ];
                $k = array_search($gid, $old_data);
                if($k !== false){
//                    $update_data[$gid] = $temp_data;
                    unset($old_data[$k]);
                    $g_model = new SalesOrderGoods();
                    $rs = $g_model->save($temp_data, ['sales_goods_id'=> $gid]);
                    if(!$rs){
                        Db::rollback();
                        return $this->error($g_model->getError());
                    }
                }else{
                    $temp_data['sales_order_id'] = $sales_order_id;
                    $new_data[] = $temp_data;
                }


            }
            if(!empty($new_data)){
                $model = new SalesOrderGoods();
                $rs = $model->saveAll($new_data);
                if(!$rs){
                    Db::rollback();
                    return $this->error($model->getError());
                }
            }
            if(!empty($old_data)){
                $del_rs = SalesOrderGoods::destroy(function($query)use($old_data){
                    $query->where('sales_goods_id','in', $old_data);
                });
                if(!$del_rs){
                    Db::rollback();
                    return $this->error('删除旧产品失败');
                }
            }
            $model = new SalesOrder();
            $goods_list = SalesOrderGoods::where(['sales_order_id'=>$sales_order_id])->column('sales_goods_id,sales_goods_sell_num,sales_goods_price,sales_goods_tax,sales_goods_amount');
            $p_order_amount = 0;
            $p_order_tax_amount = 0;
            $p_order_goods_amount = 0;
            foreach ($goods_list as $item){
                $p_order_amount += $item['sales_goods_amount'];
                $p_order_tax_amount += $item['sales_goods_tax'];
                $p_order_goods_amount += $item['sales_goods_sell_num']*$item['sales_goods_price'];
            }
            $where = ['sales_order_id'=>$sales_order_id];
            $up_data = $postData;
            $up_data['sales_order_amount'] = $p_order_amount;
            $up_data['sales_order_tax_amount'] = $p_order_tax_amount;
            $up_data['sales_order_goods_amount'] = $p_order_goods_amount;
            $rs = $model->save($up_data, $where);
            if (!$rs) {
                Db::rollback();
                return $this->error($model->getError());
            }
            Db::commit();
            return $this->success('编辑成功', '');
        }else {
            $customers = \app\erp\model\Customer::select();
            $this->assign('customers', $customers);
            $goods = SalesOrderGoods::where('sales_order_id','=',$this->request->param('sales_order_id'))->select();
            $this->assign('goods', $goods);
            $this->assign('sales_order_sn', $this->_createNo('S'));
            $this->assign('receivable_code', config('erp.RECEIVABLE_CODE'));
            return parent::edit();
        }
    }

    public function selectGoods(){
        if ($this->request->isAjax()) {
            $goods_id       = $this->request->param('goods_id');
            if($goods_id){//单个产品搜索
                $goods_id = explode(',', $goods_id);
                $list = Goods::with('unit')->where('goods_id','in',$goods_id)->select();
                return json(['status'=>1, 'data' => $list]);
            }
            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['goods_name', 'like', '%'.$keyword.'%'];
            }
            $goods = Goods::where($where)->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = Goods::alias('g')->where($where)->count('g.goods_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }
    //确认
    public function audit(){
        $id = $this->request->param('sales_order_id/d');
        $article = SalesOrder::with('goods')->get($id);
        if($article['sales_order_state'] === 0){
            if(empty($article['goods'])){
                return $this->error('该销售单还没添加商品');
            }
            $model = new SalesOrder();
            $rs = $model->save(['sales_order_state'=>1],['sales_order_id'=> $id]);
            if(!$rs){
                return $this->error($model->getError());
            }
            SalesOperLog::addSaleOperLog(1, $id,'已确认');//1、已审核
            return $this->success('确认成功', '');
        }else{
            return $this->error('状态错误');
        }
    }

    //撤销审核
    public function cacelAudit(){
        $id = $this->request->param('sales_order_id/d');
        $article = SalesOrder::get($id);
        if($article && $article['sales_order_state'] === 1){
            $model = new SalesOrder();
            $rs = $model->update(['sales_order_state'=>0],['sales_order_id'=> $id]);
            if(!$rs){
                return $this->error($model->getError());
            }
            SalesOperLog::addSaleOperLog(0, $id,'取消确认');//0、取消审核
            return $this->success('取消确认成功', '');
        }else{
            return $this->error('状态错误');
        }
    }

    public function stockout(){
        if(!$this->request->isPost()){
            //1、销售单2、发货单号：H 3、发货商品
            $id = $this->request->param('sales_order_id/d');
            $this->assign('send_order_sn', $this->_createNo('H'));
            $sales_order = SalesOrder::get($id, ['customer','goods'=>function($query){
                $query->with(['warehouseGoods'=>function($q){
                    $q->with('warehouse');
                }]);
            }]);
            $this->assign('sales_order', $sales_order);
            $this->assign('receivable_code', config('erp.RECEIVABLE_CODE'));
            return $this->fetch();
        }else{
/*send_order_sn: H20200529112707
warehouse_id[4]: 4
warehouse_id[5]: 3,5
sales_order_id: 2
admin_id: 1*/
            $postData = $this->request->post();
            $sales_order_id = $postData['sales_order_id'];
            $sales_goods_id_w_id = $postData['warehouse_id'];
            $model = new SalesOrder();
            $order = $model->with('goods')->get($sales_order_id);
            if(!$order){
                return $this->error('订单错误');
            }
            $model->startTrans();
            //1、销售订单更新状态
            $sales_order_state = 6;
            $rs = $model->update(['sales_order_state'=>$sales_order_state],['sales_order_id'=>$sales_order_id]);
            if(!$rs){
                $model->rollback();
                return $this->error($model->getError());
            }
            //2、添加发货单：mod_sales_send_order
            $send_order_data = [
                'send_order_sn' => $postData['send_order_sn'],
                'sales_order_id' => $postData['sales_order_id'],
                'return_state' => '0',
                'admin_id' => $postData['admin_id'],
            ];
            $send_model = new SalesSendOrder();
            $rs = $send_model->save($send_order_data);
            if(!$rs){
                $model->rollback();
                return $this->error($send_model->getError());
            }
            //3、添加订单发货仓库商品出库表：mod_sales_send_warehouse_goods
            $sales_send_warehouse_goods = [];
            $logs = [];
            $goods_model = new Goods();
            foreach ($order['goods'] as $item){
                $sales_goods_id = $item['sales_goods_id'];
                if(!isset($sales_goods_id_w_id[$sales_goods_id])){
                    $model->rollback();
                    return $this->error('未选择仓库');
                }
                $w_id_arr = explode(',', $sales_goods_id_w_id[$sales_goods_id]);
                $stock = $item['sales_goods_sell_num'];
                $warehouse_goods = WarehouseGoods::where('warehouse_id','in',$w_id_arr)
                    ->where('goods_id','=',$item['goods_id'])
                    ->select();
//                    ->column('warehouse_id,goods_id,warehouse_goods_stock');
                $w_g_model = new WarehouseGoods();
                foreach ($warehouse_goods as $warehouse_goods_item){ //检查并减少库存
                    $temp = [
                        'goods_id' => $item['goods_id'],
                        'warehouse_id' => $warehouse_goods_item['warehouse_id'],
                        'send_order_id' => $send_model->send_order_id,
                        'sales_order_id' => $sales_order_id,
                    ];
                    if($warehouse_goods_item['warehouse_goods_stock'] >= $stock){
                        $reduce_stock = $stock;
                        $temp['send_goods_stock'] = $stock;
                        $sales_send_warehouse_goods[] = $temp;
                        //库存够，减一个仓库的库存
                        $stock = 0;
                        $rs = $w_g_model->where('warehouse_goods_id','=',$warehouse_goods_item['warehouse_goods_id'])
                            ->setDec('warehouse_goods_stock',$reduce_stock);
                        if(!$rs){
                            $model->rollback();
                            return $this->error($w_g_model->getError());
                        }
                        break;
                    }else{ //库存不够，循环减库存
                        $temp['send_goods_stock'] = $warehouse_goods_item['warehouse_goods_stock'];
                        $sales_send_warehouse_goods[] = $temp;
                        $reduce_stock = $warehouse_goods_item['warehouse_goods_stock'];
                        $stock = $stock-$warehouse_goods_item['warehouse_goods_stock'];
                        $rs = $w_g_model->where('warehouse_goods_id','=',$warehouse_goods_item['warehouse_goods_id'])
                            ->setDec('warehouse_goods_stock',$reduce_stock);
                        if(!$rs){
                            $model->rollback();
                            return $this->error($w_g_model->getError());
                        }
                    }
                }
                if($stock > 0){
                    $model->rollback();
                    return $this->error('仓库库存不够，请确认');
                }

                $logs[] = [
                    'goods_id' => $item['goods_id'],
                    'goods_price' => $item['sales_goods_price'],
                    'sales_order_id' => $sales_order_id,
                ];
                //6、库存修改 mod_goods:goods_stock todo
                $rs = $goods_model->where('goods_id','=',$item['goods_id'])
                    ->setDec('goods_stock', $item['sales_goods_sell_num']);
                if(!$rs){
                    return $this->error('商品总库存更新失败');
                }
            }
            $s_w_g_model = new SalesSendWarehouseGoods();
            $rs = $s_w_g_model->insertAll($sales_send_warehouse_goods);
            if(!$rs){
                $model->rollback();
                return $this->error($s_w_g_model->getError());
            }
            //4、添加操作记录：mod_sales_oper_log
            $rs = SalesOperLog::addSaleOperLog($sales_order_state, $sales_order_id,'商品出库');
            if(!$rs){
                $model->rollback();
                return $this->error('操作日志添加失败');
            }
            //5、销售商品价格历史记录：mod_sales_goods_price_log
            $s_g_p_l = new SalesGoodsPriceLog();
            $rs = $s_g_p_l->saveAll($logs);
            if(!$rs){
                $model->rollback();
                return $this->error('销售商品价格历史记录添加失败');
            }
            $model->commit();
            return $this->success('发货出库成功',url('/erp/Sales/index'));
        }
    }


    public function detail(){
        //销售订单和商品
        $sales_order_id = $this->request->param('sales_order_id');
        $model = new SalesOrder();
        $order = $model->get($sales_order_id,['goods','customer','logs'=>function($q){
            $q->order('oper_log_id','asc');
        }]);

        $this->assign('sales_order', $order);
        return $this->fetch();
    }

    /**
     * 销售发货单
     */
    public function sendOrder(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['send_order_sn', 'like', '%'.$keyword.'%'];
            }
            $model = new SalesSendOrder();
            $goods = $model->with(['salesOrder'=>function($query){
                $query->with('customer');
            }])->where($where)->order(['send_order_id'=>'desc'])->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = SalesSendOrder::where($where)->count('send_order_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }
    //1、确认收货
    public function confirm(){
        $sales_order_id = $this->request->param('sales_order_id');
        $sales_order = SalesOrder::with('customer')->get($sales_order_id);
        if(!$sales_order || $sales_order['sales_order_state'] != 6){
            $this->error('订单状态错误');
        }
        Db::startTrans();
        //1、更改状态
        $rs = SalesOrder::update(['sales_order_state'=>12],['sales_order_id'=>$sales_order_id]);
        if(!$rs){
            Db::rollback();
            return $this->error('订单状态更新失败');
        }
        //2、添加应收账款
        $sales_send_order = SalesSendOrder::where('sales_order_id','=',$sales_order_id)->find();
        $accounts_receivable_data = [
            'sales_order_id' => $sales_order_id,
            'sales_order_sn' => $sales_order['sales_order_sn'],
            'send_order_id' => $sales_send_order['send_order_id'],
            'send_order_sn' => $sales_send_order['send_order_sn'],
            'customer_id' => $sales_order['customer_id'],
            'customer_name' => $sales_order['customer']['customer_name'],
            'receivable_code' => $sales_order['receivable_code'],
            'receivable_amount' => $sales_order['sales_order_amount'],
            'finish_amount' => $sales_order['receivable_code']==1?0:$sales_order['sales_order_amount'],
            'admin_id' => ADMIN_ID,
        ];
        $m = new AccountsReceivable();
        $rs = $m->save($accounts_receivable_data);
        if(!$rs){
            Db::rollback();
            return $this->error($m->getError());
        }
        Db::commit();
        return $this->success('操作成功');
    }

    public function sendDetail(){
        //1、销售单2、发货单号：H 3、发货商品
        $id = $this->request->param('sales_order_id/d');
        $this->assign('send_order_sn', $this->_createNo('H'));
        $sales_order = SalesOrder::get($id, ['customer','goods']);
        //发货仓库
        $sales_send_warehouse_goods = SalesSendWarehouseGoods::where('sales_order_id','=',$id)
            ->with('warehouse')
            ->select();
        $this->assign('sales_send_warehouse_goods',$sales_send_warehouse_goods);
        $sales_send_order = SalesSendOrder::where('sales_order_id','=',$id)->find();
        $this->assign('sales_send_order', $sales_send_order);
        $this->assign('sales_order', $sales_order);
        $this->assign('receivable_code', config('erp.RECEIVABLE_CODE'));
        return $this->fetch();
    }
}