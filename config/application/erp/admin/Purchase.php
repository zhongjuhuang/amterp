<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/8
 * Time: 9:05 AM
 */

namespace app\erp\admin;


use app\erp\model\FinancePayable;
use app\erp\model\Goods;
use app\erp\model\PurchaseGoodsPriceLog;
use app\erp\model\PurchaseOperLog;
use app\erp\model\PurchaseOrder;
use app\erp\model\PurchaseOrderGoods;
use app\erp\model\PurchaseWarehouseOrder;
use app\erp\model\PurchaseWarehouseOrderGoods;
use app\erp\model\Unit;
use app\erp\model\Warehouse;
use app\system\admin\Admin;
use app\erp\model\Supplier;
use think\Db;

class Purchase extends Admin
{
    protected $hisiModel = 'PurchaseOrder';
    protected $hisiValidate = 'PurchaseOrder';
    protected $p_order_state = [
        0 => '未审核',
        1 => '已审核',
        2 => '待入库',
        3 => '已入库',
        -1 => '退货',
        -2 => '退货完成'
    ];//采购单状态，0 未审核，1 已审核，2 已入库，-1 退货，-2 退货完成
    protected $payment_type = [
        1 => '应付账款',
        2 => '现金付款',
        3 => '预付款'
    ];//付款方式：1、应付账款2、现金付款3、预付款

    public function orders(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['p_order_sn', 'like', '%'.$keyword.'%'];
            }
            $goods = PurchaseOrder::with('supplier')->where($where)->page($page)->limit($limit)->order('p_order_id','desc')->select();
            foreach ($goods as &$good){
                $good['p_order_state'] = $this->p_order_state[$good['p_order_state']];
                $good['supplier_name'] = $good['supplier']['supplier_name'];
                $good['return_state'] = $good['return_state']==0?'无':'有';
                $good['payment_type'] = config('erp.payment_type')[$good['payment_type']];
            }
            $data['data']   = $goods;
            $data['count']  = PurchaseOrder::where($where)->count('p_order_id');
            $data['code']   = 0;
            return json($data);

        }



        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }
    
    

    public function getProducts(){
        if ($this->request->isAjax()) {
            $id = $this->request->param('p_order_id/d');
            $goods = [];
            $data['totalRow']   = ['p_goods_amount'=> 0.00];//采购金额合计
            if($id && $id > 0){
                $goods = PurchaseOrderGoods::where(['p_order_id'=>$id])->select()->toArray();
                $data['totalRow']   = ['p_goods_amount'=> array_sum(array_column($goods,'p_goods_amount'))];//采购金额合计

            }
            $data['data'] = $goods;
            $data['code']   = 0;

            return json($data);
        }
    }

    public function selectGoods(){
        if ($this->request->isAjax()) {
            $goods_id       = $this->request->param('goods_id');
            if($goods_id){//单个产品搜索
                $goods_id = explode(',', $goods_id);
                $list = Goods::with('unit')->where('goods_id','in',$goods_id)->select();
                /*$goods = Goods::get($goods_id);
                if($goods){
                    $unit = Unit::get($goods['unit_id']);
                    $goods['goods_unit'] = $unit['unit_name'];
                }*/
                return json(['status'=>1, 'data' => $list]);
            }
            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['goods_name', 'like', '%'.$keyword.'%'];
            }
            $goods = Goods::where($where)->page($page)->limit($limit)->select();
            $data['data']   = $goods;
            $data['count']  = Goods::alias('g')->where($where)->count('g.goods_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }



    public function add(){
        if ($this->request->isPost()) {
            $postData = $this->request->post();
            if(!isset($postData['goodsId'])){
                return $this->error('请选择商品');
            }
            $goodsId = $postData['goodsId'];
            $goodsPrice = $postData['goodsPrice'];
            $goodsBuyNum = $postData['goodsBuyNum'];
            $goodsTax = $postData['goodsTax'];
            $goodsAmount = $postData['goodsAmount'];
            $result = $this->validate($postData, $this->hisiValidate);
            if ($result !== true) {
                return $this->error($result);
            }
            Db::startTrans();

            $model = new PurchaseOrder();

            //加商品
            $goods_data = [];
            $p_order_goods_amount = 0;
            $p_order_tax_amount = 0;
            $p_order_amount = 0;
            foreach ($goodsId as $key => $gid){
                if($goodsBuyNum[$key] <= 0){
                    Db::rollback();
                    return $this->error('采购商品数量不能为0');
                }
                $goods = Goods::get($gid);
                $unit = Unit::get($goods['unit_id']);
                $tmp_data = [
                    'goods_id' => $gid,
                    'goods_name' => $goods['goods_name'],
                    'goods_number' => $goods['goods_number'],
                    'goods_spec' => $goods['goods_spec'],
                    'goods_unit' => $unit['unit_name'],
                    'p_goods_buy_num' => $goodsBuyNum[$key],
                    'p_goods_price' => $goodsPrice[$key],
                    'p_goods_tax' => $goodsTax[$key],
                    'p_goods_amount' => $goodsBuyNum[$key]*$goodsPrice[$key]+$goodsTax[$key],
                    'p_goods_info' => ''
                ];
                $p_order_amount += $tmp_data['p_goods_amount'];
                $p_order_tax_amount += $tmp_data['p_goods_tax'];
                $p_order_goods_amount += $tmp_data['p_goods_buy_num']*$tmp_data['p_goods_price'];
                $goods_data[] = $tmp_data;
            }
            $up_data = $postData;
            $up_data['p_order_amount'] = $p_order_amount;
            $up_data['p_order_tax_amount'] = $p_order_tax_amount;
            $up_data['p_order_goods_amount'] = $p_order_goods_amount;
            $rs = $model->save($up_data);
            if (!$rs) {
                Db::rollback();
                return $this->error($model->getError());
            }
            if(!empty($goods_data)){
                $g_model = new PurchaseOrderGoods();
                foreach ($goods_data as &$goods_datum){
                    $goods_datum['p_order_id'] = $model->p_order_id;
                }
                $rs = $g_model->saveAll($goods_data);
                if(!$rs){
                    Db::rollback();
                    return $this->error($g_model->getError());
                }
            }
            Db::commit();
            return $this->success('保存成功', '');
        }else{
            //供应商
            $supplier = Supplier::getAll();
            $this->assign('supplier', $supplier);
            $this->assign('p_order_sn', $this->_createNo('P'));
            $this->assign('payment_type', config('erp.payment_type'));
            return parent::add();
        }

    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $postData = $this->request->post();
            $p_order_id = $postData['p_order_id'];
            $goodsId = $postData['goodsId'];
            $pGoodsId = $postData['pGoodsId'];
            $goodsPrice = $postData['goodsPrice'];
            $goodsBuyNum = $postData['goodsBuyNum'];
            $goodsTax = $postData['goodsTax'];
//            $goodsAmount = $postData['goodsAmount'];
            $result = $this->validate($postData, $this->hisiValidate);
            if ($result !== true) {
                return $this->error($result);
            }
            Db::startTrans();

            //加商品
            $old_data = PurchaseOrderGoods::where(['p_order_id'=>$p_order_id])->column('p_goods_id');
            $new_data = [];//new
//            $update_data = [];
            foreach ($pGoodsId as $key => $gid){
                if($goodsBuyNum[$key] <= 0){
                    Db::rollback();
                    return $this->error('采购商品数量不能为0');
                }
                $goods = Goods::get($goodsId[$key]);
                $unit = Unit::get($goods['unit_id']);
                $temp_data = [
                    'goods_id' => $goodsId[$key],
                    'goods_name' => $goods['goods_name'],
                    'goods_number' => $goods['goods_number'],
                    'goods_spec' => $goods['goods_spec'],
                    'goods_unit' => $unit['unit_name'],
                    'p_goods_buy_num' => $goodsBuyNum[$key],
                    'p_goods_price' => $goodsPrice[$key],
                    'p_goods_tax' => $goodsTax[$key],
                    'p_goods_amount' => $goodsBuyNum[$key]*$goodsPrice[$key]+$goodsTax[$key],
                    'p_goods_info' => ''
                ];
                $k = array_search($gid, $old_data);
                if($k !== false){
//                    $update_data[$gid] = $temp_data;
                    unset($old_data[$k]);
                    $g_model = new PurchaseOrderGoods();
                    $rs = $g_model->save($temp_data, ['p_goods_id'=> $gid]);
                    if(!$rs){
                        Db::rollback();
                        return $this->error($g_model->getError());
                    }
                }else{
                    $temp_data['p_order_id'] = $p_order_id;
                    $new_data[] = $temp_data;
                }


            }
            if(!empty($new_data)){
                $model = new PurchaseOrderGoods();
                $rs = $model->saveAll($new_data);
                if(!$rs){
                    Db::rollback();
                    return $this->error($model->getError());
                }
            }
            if(!empty($old_data)){
                $del_rs = PurchaseOrderGoods::destroy(function($query)use($old_data){
                    $query->where('p_goods_id','in', $old_data);
                });
                if(!$del_rs){
                    Db::rollback();
                    return $this->error('删除旧产品失败');
                }
            }
            $model = new PurchaseOrder();
            $goods_list = PurchaseOrderGoods::where(['p_order_id'=>$p_order_id])->column('p_goods_id,p_goods_buy_num,p_goods_price,p_goods_tax,p_goods_amount');
            $p_order_amount = 0;
            $p_order_tax_amount = 0;
            $p_order_goods_amount = 0;
            foreach ($goods_list as $item){
                $p_order_amount += $item['p_goods_amount'];
                $p_order_tax_amount += $item['p_goods_tax'];
                $p_order_goods_amount += $item['p_goods_buy_num']*$item['p_goods_price'];
            }
            $where = ['p_order_id'=>$p_order_id];
            $up_data = $postData;
            $up_data['p_order_amount'] = $p_order_amount;
            $up_data['p_order_tax_amount'] = $p_order_tax_amount;
            $up_data['p_order_goods_amount'] = $p_order_goods_amount;
            $rs = $model->save($up_data, $where);
            if (!$rs) {
                Db::rollback();
                return $this->error($model->getError());
            }
            Db::commit();
            return $this->success('编辑成功', '');
        }else {
            $supplier = Supplier::getAll();
            $this->assign('supplier', $supplier);
            $this->assign('payment_type', $this->payment_type);
            return parent::edit();
        }
    }

    public function detail(){
        if(!$this->request->isPost()){
            $supplier = Supplier::getAll();
            $this->assign('supplier', $supplier);
            $this->assign('payment_type', config('erp.payment_type'));
            $this->template = 'detail';
            //操作记录
            $log = PurchaseOperLog::where('p_order_id','=',$this->request->param('p_order_id'))->order('oper_log_id','asc')->select();
            $this->assign('oper_log', $log);
            return parent::edit();
        }
    }

    public function del()
    {
        $id = $this->request->param('id/d');
        $article = PurchaseOrder::get($id,'goods');
        $rs = $article->together('goods')->delete();
        if(!$rs){
            return $this->error('删除失败');
        }
        return $this->success('删除成功', '');
    }

    public function audit(){
        $id = $this->request->param('p_order_id/d');
        $article = PurchaseOrder::with('goods')->get($id);
        if($article['p_order_state'] === 0){
            if(empty($article['goods'])){
                return $this->error('该采购单没有商品');
            }
            $model = new PurchaseOrder();
            $rs = $model->save(['p_order_state'=>1],['p_order_id'=> $id]);
            if(!$rs){
                return $this->error($model->getError());
            }
            PurchaseOperLog::addPurchaseOperLog(1, $id,'已审核');//1、已审核
            return $this->success('审核成功', '');
        }else{
            return $this->error('状态错误');
        }

    }
    //撤销审核
    public function cacelAudit(){
        $id = $this->request->param('p_order_id/d');
        $article = PurchaseOrder::get($id);
        if($article && $article['p_order_state'] === 1){
            $model = new PurchaseOrder();
            $rs = $model->update(['p_order_state'=>0],['p_order_id'=> $id]);
            if(!$rs){
                return $this->error($model->getError());
            }
            PurchaseOperLog::addPurchaseOperLog(0, $id,'取消审核');//0、取消审核
            return $this->success('撤销审核成功', '');
        }else{
            return $this->error('状态错误');
        }
    }
    //入库
    public function stockin(){
        if(!$this->request->isPost()){
            $id = $this->request->param('p_order_id/d');
            $this->assign('warehouse_order_sn', $this->_createNo('W'));
            $warehouse = Warehouse::getAll();
            $this->assign('warehouse', $warehouse);
            $purchase_order = PurchaseOrder::get($id, 'supplier');
            $this->assign('purchase_order', $purchase_order);
            $this->assign('payment_type', config('erp.payment_type'));
            $warehouse_order_state = [
                2 => '验货完成等待入库',
                3 => '验货完成直接入库',
            ];
            $this->assign('warehouse_order_state', $warehouse_order_state);
            return $this->fetch();
        }else{

            $postData = $this->request->post();
            $p_order_id = $postData['p_order_id'];
            $p_order = PurchaseOrder::with('supplier')->get($p_order_id);
            if(!$p_order || $p_order['p_order_state'] != 1){
                return $this->error('该订单状态不符或者订单不存在！');
            }

            $model = new PurchaseWarehouseOrder();
            $model->startTrans();
            //addWarehouseOrder
            $postData['warehouse_order_payment_type'] = $p_order['payment_type'];
            $postData['warehouse_order_goods_amount'] = $p_order['p_order_goods_amount'];
            $postData['warehouse_order_tax'] = $p_order['p_order_tax_amount'];
            $postData['warehouse_order_amount'] = $p_order['p_order_amount'];
            $rs = $model->save($postData);
            if(!$rs){
                $model->rollback();
                return $this->error($model->getError());
            }
            //updateOrderState
            //addPurchaseOperLog
            //addWarehouseOrder
            if($postData['warehouse_order_state'] == 3){
                //采购订单中的商品
                $orderGoods = PurchaseOrderGoods::getByOrderId($p_order_id);
                //addWarehouseOrderGoods
                $warehouse_order_id = $model->warehouse_order_id;
                $p_w_o_g_model = new PurchaseWarehouseOrderGoods();
                $rs = $p_w_o_g_model->addWarehouseOrderGoods($warehouse_order_id, $orderGoods, $postData['warehouse_id']);
                if(!$rs){
                    $model->rollback();
                    return $this->error($p_w_o_g_model->getError());
                }
                //addPurchaseGoodsPriceLog
                $rs = PurchaseGoodsPriceLog::addPurchaseGoodsPriceLog($orderGoods, $p_order_id, time());
                if(!$rs){
                    $model->rollback();
                    return $this->error('商品历史价格记录插入失败');
                }
                //加应付账款记录
                $f_payable_model = new FinancePayable();
                $insert_data = [
                    'warehouse_order_id' => $warehouse_order_id,//入库单号
                    'p_order_id' => $model['p_order_id'],//采购订单id
                    'p_order_sn' => $p_order['p_order_sn'],//采购订单号
                    'supplier_id' => $p_order['supplier_id'],//
                    'supplier_name' => $p_order->supplier->supplier_name,//
                    'payment_code' => $p_order['payment_type'],//支付方式code:付款方式：1、应付账款2、现金付款3、预付款
                    'payment_amount' => $p_order['p_order_amount'],//采购支付金额
                    'finish_amount' => $p_order['payment_type']==1?0:$p_order['p_order_amount'],//采购已经支付金额
                ];
                $rs = $f_payable_model->save($insert_data);
                if(!$rs){
                    $model->rollback();
                    return $this->error($f_payable_model->getError());
                }
            }
            //updateOrderState：p_order_state
            $rs = PurchaseOrder::update(['p_order_state'=>$postData['warehouse_order_state']],['p_order_id'=> $p_order_id]);
            if(!$rs){
                $model->rollback();
                return $this->error('采购订单状态更新失败');
            }
            $state = $postData['warehouse_order_state'];
            PurchaseOperLog::addPurchaseOperLog($state, $p_order_id, $state==2?'待入库':'已入库');//3、直接入库
            $model->commit();
            return $this->success('验货入库成功',url('/erp/Purchase/orders'));
        }

    }

    /**
     * 待入库单入库
     */
    public function insertWarehouse(){
        $warehouse_order_id = $this->request->param('warehouse_order_id/d');
        $warehouse_order = PurchaseWarehouseOrder::get($warehouse_order_id);
        if(!$warehouse_order || $warehouse_order['warehouse_order_state'] != 2){
            return $this->error('该入库单状态不符或者入库单不存在！');
        }
        Db::startTrans();
        $state = 3;
        $rs = PurchaseWarehouseOrder::update(['warehouse_order_state'=>$state],['warehouse_order_id'=>$warehouse_order_id]);
        if(!$rs){
            Db::rollback();
            return $this->error('入库单状态更新失败');
        }
        //采购订单中的商品
        $orderGoods = PurchaseOrderGoods::getByOrderId($warehouse_order['p_order_id']);
        $p_w_o_g_model = new PurchaseWarehouseOrderGoods();
        $rs = $p_w_o_g_model->addWarehouseOrderGoods($warehouse_order_id, $orderGoods, $warehouse_order['warehouse_id']);
        if(!$rs){
            Db::rollback();
            return $this->error($p_w_o_g_model->getError());
        }
        //addPurchaseGoodsPriceLog
        $rs = PurchaseGoodsPriceLog::addPurchaseGoodsPriceLog($orderGoods, $warehouse_order['p_order_id'], time());
        if(!$rs){
            Db::rollback();
            return $this->error('商品历史价格记录插入失败');
        }
        //加应付账款记录
        $f_payable_model = new FinancePayable();
        $p_order = PurchaseOrder::with('supplier')->get($warehouse_order['p_order_id']);
        $insert_data = [
            'warehouse_order_id' => $warehouse_order_id,//入库单号
            'p_order_id' => $warehouse_order['p_order_id'],//采购订单id
            'p_order_sn' => $p_order['p_order_sn'],//采购订单号
            'supplier_id' => $p_order['supplier_id'],//
            'supplier_name' => $p_order->supplier->supplier_name,//
            'payment_code' => $p_order['payment_type'],//支付方式code:付款方式：1、应付账款2、现金付款3、预付款
            'payment_amount' => $p_order['p_order_amount'],//采购支付金额
            'finish_amount' => $p_order['payment_type']==1?0:$p_order['p_order_amount'],//采购已经支付金额
            'admin_id' => ADMIN_ID
        ];
        $rs = $f_payable_model->save($insert_data);
        if(!$rs){
            Db::rollback();
            return $this->error($f_payable_model->getError());
        }
        $rs = PurchaseOrder::update(['p_order_state' => $state], ['p_order_id' => $warehouse_order['p_order_id']]);
        if(!$rs){
            Db::rollback();
            return $this->error('采购单状态更新失败');
        }
        PurchaseOperLog::addPurchaseOperLog($state, $warehouse_order['p_order_id'],'已入库');//3、已入库
        Db::commit();
        return $this->success('采购入库成功', '');
    }

    public function warehouseOrder(){
        if ($this->request->isAjax()) {

            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');

            if ($keyword) {
                $where[] = ['p_order_sn', 'like', '%'.$keyword.'%'];
            }
            $goods = PurchaseWarehouseOrder::with(['purchase'=>function($query){
                $query->with('supplier');
            }])->where($where)->page($page)->limit($limit)->order('warehouse_order_id','desc')->select();
            foreach ($goods as &$good){
                $good['p_order_state'] = $this->p_order_state[$good['warehouse_order_state']];
                $good['supplier_name'] = $good['purchase']['supplier']['supplier_name'];
                $good['payment_type'] = config('erp.payment_type')[$good['warehouse_order_payment_type']];
            }
            $data['data']   = $goods;
            $data['count']  = PurchaseWarehouseOrder::where($where)->count('p_order_id');
            $data['code']   = 0;
            return json($data);

        }


        // 分组切换类型 0无需分组切换，1单个分组，2多个分组切换[无链接]，3多个分组切换[有链接]，具体请看application/example/view/layout.html
        $this->assign('hisiTabType', 0);
        $this->assign('hisiTabData', '');
        return $this->fetch();
    }
    //取消入库
    public function cancelWarehouse(){
        $warehouse_order_id = $this->request->param('warehouse_order_id/d');
        $warehouse_order = PurchaseWarehouseOrder::get($warehouse_order_id);
        if(!$warehouse_order || $warehouse_order['warehouse_order_state'] != 2){
            $this->error('该入库单状态不符或者入库单不存在！');
        }
        //1、删除入库单 2、更改采购单状态
        Db::startTrans();
        $state = 1;
        $rs = PurchaseOrder::update(['p_order_state' => $state], ['p_order_id' => $warehouse_order['p_order_id']]);
        if(!$rs){
            Db::rollback();
            return $this->error('采购单状态更新失败');
        }
        PurchaseOperLog::addPurchaseOperLog($state, $warehouse_order['p_order_id'],'取消入库');//取消入库 1、取消入库
        $rs = PurchaseWarehouseOrder::destroy(['warehouse_order_id'=>$warehouse_order_id]);
        if(!$rs){
            Db::rollback();
            return $this->error('采购单入库取消失败');
        }
        Db::commit();
        return $this->success('入库单取消成功');
    }



    function write_static_cache($cache_name, $caches)
    {

        $cache_file_path = ROOT_PATH . '/' . $cache_name . '.txt';
        $content = date('Y-m-d H:i:s').": $caches;\r\n";
        file_put_contents($cache_file_path, $content, FILE_APPEND);
    }



}