<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class SalesOrder extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'sales_order_id';

    public function goods()
    {
        return $this->hasMany('SalesOrderGoods','sales_order_id','sales_order_id');
    }

    public function customer(){
        return $this->hasOne('Customer', 'customer_id', 'customer_id');
    }

    public function logs(){
        return $this->hasMany('SalesOperLog','sales_order_id','sales_order_id');
    }

    public function getSalesOrderGoodsAmountAttr($value){
        return number_format($value,2,".","");
    }
    public function getSalesOrderTaxAmountAttr($value){
        return number_format($value,2,".","");
    }
    public function getSalesOrderAmountAttr($value){
        return number_format($value,2,".","");
    }
}