<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class SalesOrderGoods extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'sales_goods_id';

    public function warehouseGoods(){
        return $this->hasMany('WarehouseGoods','goods_id','goods_id');
    }

    public function getSalesGoodsPriceAttr($value)
    {

        return number_format($value,2,".","");
    }

    public function getSalesGoodsAmountAttr($value){
        return number_format($value,2,".","");
    }
}