<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class PurchaseWarehouseOrder extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'warehouse_order_id';

    public function goods()
    {
        return $this->hasMany('PurchaseWarehouseOrderGoods','warehouse_order_id','warehouse_order_id');
    }

    /**
     * 采购单
     */
    public function purchase(){
        return $this->hasOne('PurchaseOrder', 'p_order_id','p_order_id');
    }

    public function warehouse(){
        return $this->hasOne('Warehouse','warehouse_id','warehouse_id');
    }
}