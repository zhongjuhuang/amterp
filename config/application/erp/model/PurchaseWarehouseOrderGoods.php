<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class PurchaseWarehouseOrderGoods extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'warehouse_order_goods_id';


    public function addWarehouseOrderGoods($warehouse_order_id, $orderGoods, $warehouse_id){
        if(!empty($orderGoods)){
            $inserData = [];
            $goods_id_stock = [];
            $goods_model = new Goods();
            foreach ($orderGoods as $goods){
                $tmp = [
                    'warehouse_order_id' => $warehouse_order_id,
                    'warehouse_id' => $warehouse_id,
                    'p_order_id' => $goods['p_order_id'],
                    'warehouse_goods_buy_num' => $goods['p_goods_buy_num'],
                    'warehouse_goods_price' => $goods['p_goods_price'],
                    'warehouse_goods_tax' => $goods['p_goods_tax'],
                    'warehouse_goods_amount' => $goods['p_goods_amount'],
                    'goods_id' => $goods['goods_id'],
                    'goods_name' => $goods['goods_name'],
                    'goods_number' => $goods['goods_number'],
                    'goods_spec' => $goods['goods_spec'],
                    'goods_unit' => $goods['goods_unit'],
                ];
                $inserData[] = $tmp;
                $goods_id_stock[$goods['goods_id']] = $goods['p_goods_buy_num'];
                $rs = $goods_model->where('goods_id','=',$goods['goods_id'])->setInc('goods_stock', $goods['p_goods_buy_num']);
                if(!$rs){
                    $this->error = '商品总库存更新失败';
                    return false;
                }
            }
            $purchase = new PurchaseWarehouseOrderGoods();
            $rs = $purchase->saveAll($inserData);
            if($rs){
                //加库存:1、加仓库库存
                $w_goods_model = new WarehouseGoods();
                $goods_list = $w_goods_model
                    ->where('warehouse_id', '=', $warehouse_id)
                    ->where('goods_id', 'in', array_keys($goods_id_stock))
                    ->select();
                foreach($goods_list as $goods){
                    $stock = $goods_id_stock[$goods['goods_id']];
                    $rs = $w_goods_model->where('warehouse_goods_id','=',$goods['warehouse_goods_id'])
                        ->setInc('warehouse_goods_stock', $stock);
                    if(!$rs){
                        $this->error = '商品仓库库存更新失败';
                        return false;
                    }
                    unset($goods_id_stock[$goods['goods_id']]);
                }
                if(!empty($goods_id_stock)){
                    $w_goods_insert_data = [];
                    foreach ($goods_id_stock as $id => $stock){
                        $w_goods_insert_data[] = [
                            'warehouse_id' => $warehouse_id,
                            'goods_id' => $id,
                            'warehouse_goods_stock' => $stock,
                        ];
                    }
                    $rs = $w_goods_model->saveAll($w_goods_insert_data);
                    if(!$rs){
                        $this->error = '商品仓库库存添加失败';
                    }
                    return $rs;

                }
                //加库存:2、加总库存

            }else{
                $this->error = '入库单商品添加失败';
                return false;
            }
            return true;
        }else{
            $this->error = '入库单商品为空';
            return false;
        }

    }
}