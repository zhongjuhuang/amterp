<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/29
 * Time: 11:38 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class SalesSendOrder extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'send_order_id';

    public function salesOrder(){
        return $this->hasOne('SalesOrder', 'sales_order_id', 'sales_order_id');
    }
}