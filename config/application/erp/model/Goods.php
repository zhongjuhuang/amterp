<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class Goods extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'goods_id';

    public function brand()
    {
        return $this->hasOne('Brand','brand_id','brand_id');
    }

    public function unit()
    {
        return $this->hasOne('Unit','unit_id','unit_id');
    }

    public function category()
    {
        return $this->hasOne('GoodsCategory','goods_category_id','goods_category_id');
    }

    public function warehouseGoods(){
        return $this->hasMany('WarehouseGoods','goods_id','goods_id');
    }

    public function sendWarehouseGoods(){
        return $this->hasMany('SalesSendWarehouseGoods','goods_id','goods_id');
    }


}