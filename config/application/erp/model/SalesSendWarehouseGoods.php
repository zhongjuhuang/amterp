<?php
/**
 * 订单发货仓库商品出库表
 * User: zhongjuhuang
 * Date: 2020/5/29
 * Time: 11:39 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class SalesSendWarehouseGoods extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'send_warehouse_goods_id';

    public function warehouse(){
        return $this->hasOne('Warehouse','warehouse_id','warehouse_id');
    }
}