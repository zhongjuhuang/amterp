<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:22 AM
 */

namespace app\erp\model;


use think\Model;
use think\model\concern\SoftDelete;

class Warehouse extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    use SoftDelete;
    protected $deleteTime = 'dtime';
    protected $pk = 'warehouse_id';

    public function goods()
    {
        return $this->hasMany('WarehouseGoods','warehouse_id','warehouse_id');
    }


    public static function getAll($map = []){
        $data = self::where($map)->column('warehouse_id,warehouse_code,warehouse_name');
        return array_values($data);
    }
}