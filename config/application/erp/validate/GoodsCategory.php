<?php
namespace app\erp\validate;

use think\Validate;
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/7
 * Time: 11:35 AM
 */
class GoodsCategory extends Validate
{
    //定义验证规则
    protected $rule = [
        'goods_category_name|分类名称' => 'require',
        'goods_category_code|分类编码' => 'require',
    ];
}