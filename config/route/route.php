﻿<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::domain('mdemo', 'site');
//Route::rule('/', '/Index/index');
Route::domain('mdemo', function () {
    // 动态注册域名的路由规则
    Route::rule('/', '/Index/index');
});

Route::rule('/', '/admin.php/system/index/index');