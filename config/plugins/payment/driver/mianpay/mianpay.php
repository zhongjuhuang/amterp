<?php
namespace plugins\payment\driver\mianpay;
use plugins\payment\model\PluginsPaymentLog as PaymentLogModel;
use think\Controller;
class mianpay extends Controller{

   

    public function __construct($config) {
        $this->config = array (    
                //应用ID,您的APPID。
                'app_id' => $config['app_id'],
                'app_scret' => $config['app_scret'],
                'notify_url'=>$config['notify_url'],
                'return_url'=>$config['return_url']
                
        );   
    }
    public function pay($data)
    {
        //构造要请求的参数数组，无需改动
        $parameter = array(
            "app_key" => trim($this->config['app_id']),
            "type" => trim($data['type']),
            "notify_url"    => get_domain().'/plugins/payment/notice/notify',
            "return_url"    => get_domain().'/plugins/payment/notice/returns',
            "out_trade_no"  => $data['out_trade_no'],
            "total_fee" => ($data['total_amount']*100),//转换分
            "subject"   => $data['body']
        );
        ksort($parameter);
        reset($parameter);
        $parameter['sign']=$this->getSign($parameter,trim($this->config['app_scret']));
        $url='https://pay.quanlaibang.com/api/submit?'.$this->createLinkstringUrlencode($parameter);
        header("Location: $url");
    }
    public function getSign($data,$appsecret){
            ksort($data);
            $need = [];
            foreach ($data as $key => $value) {
                if (! $value || $key == 'sign') {
                    continue;
                }
                $need[] = "{$key}={$value}";
            }
            $string = implode('&', $need).$appsecret;
            return strtoupper(md5($string));
    }
    public function createLinkstringUrlencode($para) {
            $arg  = "";
            foreach ($para as $key => $val) {
                $arg.=$key."=".urlencode($val)."&";
            }
            //去掉最后一个&字符
            $arg = substr($arg,0,-1);
            
            //如果存在转义字符，那么去掉转义
            if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
            
            return $arg;
    }

    public function notify_validation($data)
    {
        $trade_no=(new PaymentLogModel)->where(['out_trade_no'=>$data['out_trade_no']])->value('trade_no');
        $payId = $data['out_trade_no'];//商户订单号
        $param = $data['attach'];//创建订单的时候传入的参数
        $price = $data['total_fee'];//订单金额
        $sign = $data['sign'];
        $datas['out_trade_no']=$data['out_trade_no'];
        $datas['order_sn']=$data['order_sn'];
        $datas['total_fee']=$data['total_fee'];
        $datas['status']=$data['status'];
        $datas['type']=$data['type'];
        $datas['attach']=$data['attach'];
        $datas['sign']=$data['sign'];
        $_sign =  $this->getSign($datas,$this->config['app_scret']);
        if ($_sign != $sign) {
            return false;
        }
        if ($data['status']!=='9') {
            return false;
        }
        $total_amount=number_format($price/100,2 ,".", "");//返回的是分需要转换元
        if (!(new PaymentLogModel)->where(['out_trade_no'=>$payId,'status'=>1,'value'=>$total_amount])->update(['status'=>2,'trade_no'=>$data['order_sn']])) {
            return false;
        }
        return true;
    }
}