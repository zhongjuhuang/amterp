<?php
namespace plugins\payment\model;
use think\Model;
class PluginsPaymentLog extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    public function hasPayment()
    {
        return $this->hasOne('plugins\payment\model\PluginsPayment', 'code', 'code');
    }
    public function hasUser()
    {
        return $this->hasOne('app\user\model\User', 'id', 'member_id');
    }
}