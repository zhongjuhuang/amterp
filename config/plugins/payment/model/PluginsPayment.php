<?php
namespace plugins\payment\model;

use think\Model;
use think\Db;

class PluginsPayment extends Model
{
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'mtime';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;

    public static function lists($member_id = 0)
    {
        $result = self::where('status', 1)->column('id,code,title,icon,intro');
        if ($member_id) {
            $data = [];
            foreach ($result as $k => $v) {
                $v['bind'] = 0;
                $oauth_id = Db::name('plugins_payment_log')->where(['code' => $v['code'], 'member_id' => $member_id])->value('id');
                if ($oauth_id) {
                    $v['bind'] = $oauth_id;
                }
                $data[] = $v;
            }
            return $data;
        }
        return $result;
    }
}