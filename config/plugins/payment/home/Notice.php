<?php
namespace plugins\payment\home;
use app\common\controller\Common;
use plugins\payment\model\PluginsPayment as PaymentModel;
use plugins\payment\model\PluginsPaymentLog as PaymentLogModel;
defined('IN_SYSTEM') or die('Access Denied');

class Notice extends Common
{

    public  function notify()
    {
        $data=$_POST;
        $this->type=(new PaymentLogModel)->where('out_trade_no',$data['out_trade_no'])->value('code');
        if (!$this->type) {
            echo "error_sign";//sign校验不通过
            exit();
        }
        $res = PaymentModel::where(['code' => $this->type, 'status' => 1])->find();
        if (!$res) {
            echo "error_sign";//sign校验不通过
            exit();
        }
        if (!$res['config']) {
            echo "error_sign";//sign校验不通过
            exit();
        }
        $this->config = json_decode($res['config'], 1);
        $class = "\\plugins\\payment\\driver\\mianpay\\mianpay";
        $class=new $class($this->config);
        if ($class->notify_validation($data)) {
            //可以在这进行会员充值等操作
            //
            //
            //
            echo 'success';die;//付款成功
        }
        echo 'error_sign';die;//付款失败
    }
    public  function returns()
    {
        $data=$_GET;
        $this->type=(new PaymentLogModel)->where('out_trade_no',$data['out_trade_no'])->value('code');
        if (!$this->type) {
            echo "error_sign";//sign校验不通过
            exit();
        }
        $res = PaymentModel::where(['code' => $this->type, 'status' => 1])->find();
        if (!$res) {
            echo "error_sign";//sign校验不通过
            exit();
        }
        if (!$res['config']) {
            echo "error_sign";//sign校验不通过
            exit();
        }
        $this->config = json_decode($res['config'], 1);
        $class = "\\plugins\\payment\\driver\\mianpay\\mianpay";
        $class=new $class($this->config);
        if ($class->notify_validation($data)) {
            //可以在这进行会员充值等操作
            //
            //
            //
            echo 'success';die;//付款成功
        }
        echo 'error_sign';die;//付款失败
    }
}