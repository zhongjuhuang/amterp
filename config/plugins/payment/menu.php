<?php
return [
  [
    'pid'           => 5,
    'title' => '在线支付',
    'icon' => 'aicon ai-shezhi',
    'module' => 'system',
    'url' => 'system/plugins/run',
    'param' => '_a=index&_c=index&_p=payment',
    'target' => '_self',
    'debug' => 0,
    'system' => 0,
    'nav' => 1,
    'sort' => 0,
    'childs' => [
      [
        'title' => '配置',
        'icon' => '',
        'module' => 'system',
        'url' => 'system/plugins/run',
        'param' => '_a=config&_c=index&_p=paymen',
        'target' => '_self',
        'debug' => 0,
        'system' => 0,
        'nav' => 1,
        'sort' => 0,
      ],
    ],
  ],
];