/*
 sql安装文件
*/
# ------------------------------------------------------------

DROP TABLE IF EXISTS `db_plugins_payment`;

CREATE TABLE `db_plugins_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '支付名称',
  `code` varchar(20) NOT NULL DEFAULT '' COMMENT '编码',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  `intro` varchar(200) NOT NULL DEFAULT '' COMMENT '简介',
  `applies` varchar(20) NOT NULL DEFAULT 'PC' COMMENT '适用终端',
  `config` text NOT NULL COMMENT '配置',
  `sort` int(3) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0停用，1启用)',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0',
  `mtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='[插件] 第三方支付';

# ------------------------------------------------------------

DROP TABLE IF EXISTS `db_plugins_payment_log`;

CREATE TABLE `db_plugins_payment_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户标示',
  `out_trade_no` varchar(256) DEFAULT '' COMMENT '商户订单号',
  `trade_no` varchar(256) DEFAULT '' COMMENT '支付平台交易号',
  `code` varchar(30) NOT NULL DEFAULT '' COMMENT '支付方式',
  `type` varchar(255) NOT NULL DEFAULT '' COMMENT '类型',
  `note` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `img` varchar(500) NOT NULL DEFAULT '' COMMENT '二维码生成参数',
  `value` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '交易金额',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0支付失败 1待支付 2付款成功)',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;