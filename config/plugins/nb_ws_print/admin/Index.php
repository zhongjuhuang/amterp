<?php
namespace plugins\nb_ws_print\admin;
use app\common\controller\Common;
use app\system\model\SystemLog as LogModel;
defined('IN_SYSTEM') or die('Access Denied');

class Index extends Common
{
    protected $hisiTable = 'SystemLog';

    /**
     * 日志首页
     */
    public function index()
    {
        if ($this->request->isAjax()) {

            $where  = $data = [];
            $page   = $this->request->param('page/d', 1);
            $limit  = $this->request->param('limit/d', 15);
            $uid    = $this->request->param('uid/d');
            
            if ($uid) {
                $where['uid'] = $uid;
            }

            $data['data']   = LogModel::with('user')->where($where)->page($page)->order('mtime desc')->limit($limit)->select();
            $data['count']  = LogModel::where($where)->count('id');
            $data['code']   = 0;

            return json($data);

        }

        return $this->fetch();
    }
    public function _print(){
        if ($this->request->isAjax()) {

            $where  = $data = [];
            $page   = $this->request->param('page/d', 1);
            $limit  = $this->request->param('limit/d', 15);
            $uid    = $this->request->param('uid/d');
            
            if ($uid) {
                $where['uid'] = $uid;
            }

            $data['data']   = LogModel::with('user')->where($where)->page($page)->order('mtime desc')->limit($limit)->select();
            $data['count']  = LogModel::where($where)->count('id');
            $data['code']   = 0;

            return json($data);

        }
    }
}