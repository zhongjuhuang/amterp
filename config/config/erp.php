<?php
/**
 * Created by PhpStorm.
 * User: zhongjuhuang
 * Date: 2020/5/28
 * Time: 10:35 AM
 */
return [
    'payment_type' => [
        //付款方式：1、应付账款2、现金付款3、预付款
        1 => '应付账款',
        2 => '现金付款',
        3 => '预付款',
    ],
    //收款方式
    'RECEIVABLE_CODE' => [
        1 => '应收账款',
        2 => '现金收款',
        3 => '预存款',
    ],
    //销售状态
    'SALES_ORDER_STATE' => [
        0 => '待确认',
        1 => '已确认',
        6 => '发货出库',
        12 => '确认收货'
    ],
];