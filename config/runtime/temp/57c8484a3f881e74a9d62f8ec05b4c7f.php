<?php /*a:6:{s:87:"/Users/zhongjuhuang/Documents/258.com/hisiphp/application/erp/view/purchase/detail.html";i:1590479589;s:81:"/Users/zhongjuhuang/Documents/258.com/hisiphp/application/system/view/layout.html";i:1587354666;s:87:"/Users/zhongjuhuang/Documents/258.com/hisiphp/application/system/view/block/header.html";i:1587354666;s:85:"/Users/zhongjuhuang/Documents/258.com/hisiphp/application/system/view/block/menu.html";i:1590454795;s:86:"/Users/zhongjuhuang/Documents/258.com/hisiphp/application/system/view/block/layui.html";i:1587354666;s:87:"/Users/zhongjuhuang/Documents/258.com/hisiphp/application/system/view/block/footer.html";i:1587804362;}*/ ?>
<?php if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo htmlentities($hisiCurMenu['title']); ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="hisi-theme-<?php echo cookie('hisi_admin_theme'); ?> pb50">
<?php else: ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if($hisiCurMenu['url'] == 'system/index/index'): ?>管理控制台<?php else: ?><?php echo htmlentities($hisiCurMenu['title']); ?><?php endif; ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="layui-layout-body hisi-theme-<?php echo cookie('hisi_admin_theme'); ?>">
<?php 
$ca = strtolower(request()->controller().'/'.request()->action());
 ?>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header" style="z-index:999!important;">
    <div class="fl header-logo"><img style="height: 66px" src="<?php echo config('base.site_logo'); ?>"><!--后台管理中心--></div>
    <div class="fl header-fold"><a href="javascript:;" title="打开/关闭左侧导航" class="aicon ai-shouqicaidan" id="foldSwitch"></a></div>
    <ul class="layui-nav fl nobg main-nav">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $vo['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $vo['id'] == 3)): ?>
           <li class="layui-nav-item layui-this">
            <?php else: ?>
            <li class="layui-nav-item">
            <?php endif; ?> 
            <a href="javascript:;"><?php echo htmlentities($vo['title']); ?></a></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
    <ul class="layui-nav fr nobg head-info">
        <!--<li class="layui-nav-item">
            <a href="/" target="_blank" class="aicon ai-ai-home" title="前台"></a>
        </li>
        <li class="layui-nav-item">
            <a href="javascript:void(0);" class="aicon ai-qingchu" id="hisi-clear-cache" title="清缓存"></a>
        </li>
        <li class="layui-nav-item">
        <a href="javascript:void(0);" class="aicon ai-suo" id="lockScreen" title="锁屏"></a>
        </li>
        <li class="layui-nav-item">
            <a href="<?php echo url('system/user/setTheme'); ?>" id="hisi-theme-setting" class="aicon ai-theme"></a>
        </li>
        <li class="layui-nav-item hisi-lang">
            <a href="javascript:void(0);"><i class="layui-icon layui-icon-website"></i></a>
            <dl class="layui-nav-child">
                <?php if(is_array($languages) || $languages instanceof \think\Collection || $languages instanceof \think\Paginator): $i = 0; $__LIST__ = $languages;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if($vo['pack']): ?>
                <dd><a href="<?php echo url('system/index/index'); ?>?lang=<?php echo htmlentities($vo['code']); ?>"><?php echo htmlentities($vo['name']); ?></a></dd>
                <?php endif; ?>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                <dd>
                    <a data-id="000" class="admin-nav-item top-nav-item" href="<?php echo url('system/language/index'); ?>">语言包管理</a>
                </dd>
            </dl>
        </li>-->
        <li class="layui-nav-item">
            <a href="javascript:void(0);"><?php echo htmlentities($login['nick']); ?>&nbsp;&nbsp;</a>
            <dl class="layui-nav-child">
                <dd>
                    <a data-id="00" class="admin-nav-item top-nav-item" href="<?php echo url('system/user/info'); ?>">个人设置</a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/user/iframe'); ?>" class="hisi-ajax" refresh="true"><?php echo input('cookie.hisi_iframe') ? '单页布局' : '框架布局'; ?></a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/publics/logout'); ?>">退出登陆</a>
                </dd>
            </dl>
        </li>
    </ul>
</div>
<div class="layui-side layui-bg-black" id="switchNav">
    <div class="layui-side-scroll">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $v['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $v['id'] == 3)): ?>
        <ul class="layui-nav layui-nav-tree">
        <?php else: ?>
        <ul class="layui-nav layui-nav-tree" style="display:none;">
        <?php endif; if((isset($v['childs']))): if(is_array($v['childs']) || $v['childs'] instanceof \think\Collection || $v['childs'] instanceof \think\Paginator): $kk = 0; $__LIST__ = $v['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($kk % 2 );++$kk;?>
            <li class="layui-nav-item <?php if($kk == 1): ?>layui-nav-itemed<?php endif; ?>">
                <a href="javascript:;"><i class="<?php echo htmlentities($vv['icon']); ?>"></i><?php echo htmlentities($vv['title']); ?><span class="layui-nav-more"></span></a>
                <dl class="layui-nav-child">
                    <?php if($vv['title'] == '快捷菜单'): ?>
                        <dd>
                            <a class="admin-nav-item" data-id="0" href="<?php echo input('cookie.hisi_iframe') ? url('system/index/welcome') : url('system/index/index'); ?>"><i class="aicon ai-shouye"></i> 后台首页</a>
                        </dd>
                        <?php if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a><i data-href="<?php echo url('system/menu/del?id='.$vvv['id']); ?>" class="layui-icon j-del-menu">&#xe640;</i>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; else: if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </dl>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        <?php endif; ?>
        </ul>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<script type="text/html" id="hisi-theme-tpl">
    <ul class="hisi-themes">
        <?php $_result=session('hisi_admin_themes');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <li data-theme="<?php echo htmlentities($vo); ?>" class="hisi-theme-item-<?php echo htmlentities($vo); ?>"></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
</script>
<script type="text/html" id="hisi-clear-cache-tpl">
    <form class="layui-form" style="padding:10px 0 0 30px;" action="<?php echo url('system/index/clear'); ?>" method="post">
        <div class="layui-form-item">
            <input type="checkbox" name="cache" value="1" title="数据缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="log" value="1" title="日志缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="temp" value="1" title="模板缓存" />
        </div>
        <div class="layui-form-item">
            <button class="layui-btn layui-btn-normal" lay-submit="" lay-filter="formSubmit">执行删除</button>
        </div>
    </form>
</script>
    <div class="layui-body" id="switchBody">
        <ul class="bread-crumbs">
            <?php if(is_array($hisiBreadcrumb) || $hisiBreadcrumb instanceof \think\Collection || $hisiBreadcrumb instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiBreadcrumb;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if($key > 0 && $i != count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="<?php echo url($v['url'].'?'.$v['param']); ?>"><?php echo htmlentities($v['title']); ?></a></li>
                <?php elseif($i == count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php else: ?>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php endif; ?>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <li><a href="<?php echo url('system/menu/quick?id='.$hisiCurMenu['id']); ?>" title="添加到首页快捷菜单" class="j-ajax">[+]</a></li>
        </ul>
        <div style="padding:0 10px;" class="mcolor"><?php echo runhook('system_admin_tips'); ?></div>
            <div class="page-body">
<?php endif; switch($hisiTabType): case "1": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <li class="layui-this">
                        <a href="javascript:;" id="curTitle"><?php echo $hisiCurMenu['title']; ?></a>
                    </li>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        <form class="layui-form" action="<?php echo url(); ?>" method="post" id="editForm">
    <div class="layui-form-item">
        <label class="layui-form-label w100">采购单号</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-p_order_sn" name="p_order_sn" lay-verify="required" autocomplete="off" value="<?php echo isset($p_order_sn)?$p_order_sn:''; ?>">
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商</label>
        <div class="layui-input-inline w200">
            <select name="supplier_id" class="field-supplier_id" type="select" lay-verify="required" lay-filter="supplier_id" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($supplier as $key=>$value): ?>
                <option value="<?php echo htmlentities($value['supplier_id']); ?>"><?php echo htmlentities($value['supplier_name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款方式</label>
        <div class="layui-input-inline w200">
            <select name="payment_type" class="field-payment_type" type="select" lay-verify="required" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($payment_type as $key=>$value): ?>
                <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商联系人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_contacts" name="supplier_contacts" lay-verify="" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">供应商手机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_phone" name="supplier_phone" lay-verify="mobile" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商座机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_telephone" name="supplier_telephone" lay-verify="telephone" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-inline w600">
            <textarea name="p_order_info" placeholder="" class="layui-textarea field-p_order_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>采购商品</legend>
    </fieldset>

    <table class="layui-hide" id="test" lay-filter="test"></table>
    <?php if(!empty($oper_log)): ?>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>操作记录</legend>
    </fieldset>
    <table class="layui-hide" id="log" lay-filter="test"></table>
    <?php endif; ?>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-p_order_id" name="p_order_id">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <a href="<?php echo url('orders'); ?>" class="layui-btn layui-btn-primary ml10">返回</a>
        </div>
    </div>
</form>
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form'], function() {
        var  $ = layui.jquery, layer = layui.layer, form = layui.form;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);

    });
    var child_idx;
    var tableIns;
    var tableContent = new Array();
    //    tableContent = [{goods_id: 2, goods_name: 2, goods_number: 1, goods_spec: "反渗透滤芯", goods_price: 0,goods_recommend_price:0,
    //        category_name:'ddd',brand_name:'rer',goods_stock:1}]
    layui.use('table', function(){
        var table = layui.table,
            $ = layui.jquery, layer = layui.layer;
        //温馨提示：默认由前端自动合计当前行数据。从 layui 2.5.6 开始： 若接口直接返回了合计行数据，则优先读取接口合计行数据。
        //详见：https://www.layui.com/doc/modules/table.html#totalRow
        tableIns = table.render({
            elem: '#test'
//            ,url:"<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>"
//            ,toolbar: '#toolbarDemo'
            ,data : tableContent
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : tableContent
            ,cols: [[
//                {type: 'checkbox', fixed: 'left'}
//                ,{field:'id', title:'ID', width:80, fixed: 'left', unresize: true}
                {field:'goods_number', title:'商品编号', edit: 'text', templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        var tmp_html = ' <input type="hidden" name="goodsId[]" value="'+res.goods_id+'">'
                            +res.goods_number
                            +' <input type="hidden" name="pGoodsId[]" value="'+(res.p_goods_id?res.p_goods_id:"")+'">';
                        return tmp_html;
                    }
                }}//,totalRowText: '采购金额合计:'
                ,{field:'goods_name', title:'商品名称', edit: 'text'}
                ,{field:'goods_spec', title:'商品规格'}
                ,{field:'goods_unit', title:'单位', edit: 'text'}
                ,{field:'p_goods_price', title:'采购单价', sort: true}
                ,{field:'p_goods_buy_num', title:'采购数量', sort: true}
                ,{field:'p_goods_tax', title:'税金', sort: true}
                ,{field:'p_goods_amount', title:'采购总价',width:200, sort: true,templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        return res.p_goods_amount;
                    }
                },totalRow: true}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });
        var logTableContent = JSON.parse( '<?php echo json_encode($oper_log); ?>' );
        table.render({ //操作日志表
            elem: '#log'
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : logTableContent
            ,cols: [[
                {field:'oper', title:'状态',templet:function (res) {
                    if(res.oper){
                        return res.oper;
                    }
                    if(res.order_state==1){ //1、取消入库
                        return '取消入库';//0 取消审核，1 已审核，2待入库3 已入库
                    }else if(res.order_state==2){
                        return '待入库';
                    }else if(res.order_state==3){ //3
                        return '已入库';
                    }
                }}
                ,{field:'oper_user', title:'操作人'}
                ,{field:'ctime', title:'操作时间'}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });


        //ajax获取数据然后动态赋值
        $.ajax({
            url: "<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>",
            type: 'POST',
            data: { },
            dataType: 'json',
            asyn:false,
            beforeSend: function () {

            },
            success: function (result) {
                if(result['code'] == 0){
                    tableContent = result['data'];
                    tableIns.reload({data:tableContent});
                }
            },
            error: function (error) {
                // parent.layer.close(layLoad);
                layer.msg('系统错误：' + error.status, {shift: 6});
            }
        });


        $('#add_goods').click(function (e) {


            child_idx = layer.open({
                type: 2 //此处以iframe举例
                ,title: '选择商品'
                ,area: [$(window).width()-390+'px', $(window).height()-200+'px']
                ,shade: 0
                ,maxmin: true
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: "<?php echo url('selectGoods'); ?>?hisi_iframe=yes"
                ,btn: ['选择商品添加', '关闭'] //只是为了演示
                ,yes: function(){
                    //父页面给子弹窗页面赋值
//                    $("#layui-layer-iframe" + child_idx).contents().find("input").val("11111111");
                    //父页面调用子弹窗的 js方法
                    var contentWindow = $("#layui-layer-iframe" + child_idx)[0].contentWindow;
                    contentWindow.addAllGoods();
                    layer.closeAll();
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,zIndex: layer.zIndex //重点1
                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });


    });

</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; case "2": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $k = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;if(($k == 1)): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; ?>
                            <a href="javascript:;" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <form class="layui-form" action="<?php echo url(); ?>" method="post" id="editForm">
    <div class="layui-form-item">
        <label class="layui-form-label w100">采购单号</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-p_order_sn" name="p_order_sn" lay-verify="required" autocomplete="off" value="<?php echo isset($p_order_sn)?$p_order_sn:''; ?>">
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商</label>
        <div class="layui-input-inline w200">
            <select name="supplier_id" class="field-supplier_id" type="select" lay-verify="required" lay-filter="supplier_id" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($supplier as $key=>$value): ?>
                <option value="<?php echo htmlentities($value['supplier_id']); ?>"><?php echo htmlentities($value['supplier_name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款方式</label>
        <div class="layui-input-inline w200">
            <select name="payment_type" class="field-payment_type" type="select" lay-verify="required" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($payment_type as $key=>$value): ?>
                <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商联系人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_contacts" name="supplier_contacts" lay-verify="" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">供应商手机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_phone" name="supplier_phone" lay-verify="mobile" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商座机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_telephone" name="supplier_telephone" lay-verify="telephone" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-inline w600">
            <textarea name="p_order_info" placeholder="" class="layui-textarea field-p_order_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>采购商品</legend>
    </fieldset>

    <table class="layui-hide" id="test" lay-filter="test"></table>
    <?php if(!empty($oper_log)): ?>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>操作记录</legend>
    </fieldset>
    <table class="layui-hide" id="log" lay-filter="test"></table>
    <?php endif; ?>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-p_order_id" name="p_order_id">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <a href="<?php echo url('orders'); ?>" class="layui-btn layui-btn-primary ml10">返回</a>
        </div>
    </div>
</form>
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form'], function() {
        var  $ = layui.jquery, layer = layui.layer, form = layui.form;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);

    });
    var child_idx;
    var tableIns;
    var tableContent = new Array();
    //    tableContent = [{goods_id: 2, goods_name: 2, goods_number: 1, goods_spec: "反渗透滤芯", goods_price: 0,goods_recommend_price:0,
    //        category_name:'ddd',brand_name:'rer',goods_stock:1}]
    layui.use('table', function(){
        var table = layui.table,
            $ = layui.jquery, layer = layui.layer;
        //温馨提示：默认由前端自动合计当前行数据。从 layui 2.5.6 开始： 若接口直接返回了合计行数据，则优先读取接口合计行数据。
        //详见：https://www.layui.com/doc/modules/table.html#totalRow
        tableIns = table.render({
            elem: '#test'
//            ,url:"<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>"
//            ,toolbar: '#toolbarDemo'
            ,data : tableContent
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : tableContent
            ,cols: [[
//                {type: 'checkbox', fixed: 'left'}
//                ,{field:'id', title:'ID', width:80, fixed: 'left', unresize: true}
                {field:'goods_number', title:'商品编号', edit: 'text', templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        var tmp_html = ' <input type="hidden" name="goodsId[]" value="'+res.goods_id+'">'
                            +res.goods_number
                            +' <input type="hidden" name="pGoodsId[]" value="'+(res.p_goods_id?res.p_goods_id:"")+'">';
                        return tmp_html;
                    }
                }}//,totalRowText: '采购金额合计:'
                ,{field:'goods_name', title:'商品名称', edit: 'text'}
                ,{field:'goods_spec', title:'商品规格'}
                ,{field:'goods_unit', title:'单位', edit: 'text'}
                ,{field:'p_goods_price', title:'采购单价', sort: true}
                ,{field:'p_goods_buy_num', title:'采购数量', sort: true}
                ,{field:'p_goods_tax', title:'税金', sort: true}
                ,{field:'p_goods_amount', title:'采购总价',width:200, sort: true,templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        return res.p_goods_amount;
                    }
                },totalRow: true}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });
        var logTableContent = JSON.parse( '<?php echo json_encode($oper_log); ?>' );
        table.render({ //操作日志表
            elem: '#log'
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : logTableContent
            ,cols: [[
                {field:'oper', title:'状态',templet:function (res) {
                    if(res.oper){
                        return res.oper;
                    }
                    if(res.order_state==1){ //1、取消入库
                        return '取消入库';//0 取消审核，1 已审核，2待入库3 已入库
                    }else if(res.order_state==2){
                        return '待入库';
                    }else if(res.order_state==3){ //3
                        return '已入库';
                    }
                }}
                ,{field:'oper_user', title:'操作人'}
                ,{field:'ctime', title:'操作时间'}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });


        //ajax获取数据然后动态赋值
        $.ajax({
            url: "<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>",
            type: 'POST',
            data: { },
            dataType: 'json',
            asyn:false,
            beforeSend: function () {

            },
            success: function (result) {
                if(result['code'] == 0){
                    tableContent = result['data'];
                    tableIns.reload({data:tableContent});
                }
            },
            error: function (error) {
                // parent.layer.close(layLoad);
                layer.msg('系统错误：' + error.status, {shift: 6});
            }
        });


        $('#add_goods').click(function (e) {


            child_idx = layer.open({
                type: 2 //此处以iframe举例
                ,title: '选择商品'
                ,area: [$(window).width()-390+'px', $(window).height()-200+'px']
                ,shade: 0
                ,maxmin: true
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: "<?php echo url('selectGoods'); ?>?hisi_iframe=yes"
                ,btn: ['选择商品添加', '关闭'] //只是为了演示
                ,yes: function(){
                    //父页面给子弹窗页面赋值
//                    $("#layui-layer-iframe" + child_idx).contents().find("input").val("11111111");
                    //父页面调用子弹窗的 js方法
                    var contentWindow = $("#layui-layer-iframe" + child_idx)[0].contentWindow;
                    contentWindow.addAllGoods();
                    layer.closeAll();
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,zIndex: layer.zIndex //重点1
                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });


    });

</script>
                </div>
            </div>
        </div>
    <?php break; case "3": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;
                            $hisiTabData['current'] = isset($hisiTabData['current']) ? $hisiTabData['current'] : '';
                         if(($vo['url'] == $hisiCurMenu['url'] or (url($vo['url']) == $hisiTabData['current']))): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; if((strpos($vo['url'], 'http'))): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" target="_blank"><?php echo $vo['title']; ?></a>
                            <?php elseif((strpos($vo['url'], config('sys.admin_path')) !== false)): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php else: ?>
                                <a href="<?php echo url($vo['url']); ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        <form class="layui-form" action="<?php echo url(); ?>" method="post" id="editForm">
    <div class="layui-form-item">
        <label class="layui-form-label w100">采购单号</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-p_order_sn" name="p_order_sn" lay-verify="required" autocomplete="off" value="<?php echo isset($p_order_sn)?$p_order_sn:''; ?>">
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商</label>
        <div class="layui-input-inline w200">
            <select name="supplier_id" class="field-supplier_id" type="select" lay-verify="required" lay-filter="supplier_id" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($supplier as $key=>$value): ?>
                <option value="<?php echo htmlentities($value['supplier_id']); ?>"><?php echo htmlentities($value['supplier_name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款方式</label>
        <div class="layui-input-inline w200">
            <select name="payment_type" class="field-payment_type" type="select" lay-verify="required" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($payment_type as $key=>$value): ?>
                <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商联系人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_contacts" name="supplier_contacts" lay-verify="" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">供应商手机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_phone" name="supplier_phone" lay-verify="mobile" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商座机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_telephone" name="supplier_telephone" lay-verify="telephone" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-inline w600">
            <textarea name="p_order_info" placeholder="" class="layui-textarea field-p_order_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>采购商品</legend>
    </fieldset>

    <table class="layui-hide" id="test" lay-filter="test"></table>
    <?php if(!empty($oper_log)): ?>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>操作记录</legend>
    </fieldset>
    <table class="layui-hide" id="log" lay-filter="test"></table>
    <?php endif; ?>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-p_order_id" name="p_order_id">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <a href="<?php echo url('orders'); ?>" class="layui-btn layui-btn-primary ml10">返回</a>
        </div>
    </div>
</form>
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form'], function() {
        var  $ = layui.jquery, layer = layui.layer, form = layui.form;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);

    });
    var child_idx;
    var tableIns;
    var tableContent = new Array();
    //    tableContent = [{goods_id: 2, goods_name: 2, goods_number: 1, goods_spec: "反渗透滤芯", goods_price: 0,goods_recommend_price:0,
    //        category_name:'ddd',brand_name:'rer',goods_stock:1}]
    layui.use('table', function(){
        var table = layui.table,
            $ = layui.jquery, layer = layui.layer;
        //温馨提示：默认由前端自动合计当前行数据。从 layui 2.5.6 开始： 若接口直接返回了合计行数据，则优先读取接口合计行数据。
        //详见：https://www.layui.com/doc/modules/table.html#totalRow
        tableIns = table.render({
            elem: '#test'
//            ,url:"<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>"
//            ,toolbar: '#toolbarDemo'
            ,data : tableContent
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : tableContent
            ,cols: [[
//                {type: 'checkbox', fixed: 'left'}
//                ,{field:'id', title:'ID', width:80, fixed: 'left', unresize: true}
                {field:'goods_number', title:'商品编号', edit: 'text', templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        var tmp_html = ' <input type="hidden" name="goodsId[]" value="'+res.goods_id+'">'
                            +res.goods_number
                            +' <input type="hidden" name="pGoodsId[]" value="'+(res.p_goods_id?res.p_goods_id:"")+'">';
                        return tmp_html;
                    }
                }}//,totalRowText: '采购金额合计:'
                ,{field:'goods_name', title:'商品名称', edit: 'text'}
                ,{field:'goods_spec', title:'商品规格'}
                ,{field:'goods_unit', title:'单位', edit: 'text'}
                ,{field:'p_goods_price', title:'采购单价', sort: true}
                ,{field:'p_goods_buy_num', title:'采购数量', sort: true}
                ,{field:'p_goods_tax', title:'税金', sort: true}
                ,{field:'p_goods_amount', title:'采购总价',width:200, sort: true,templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        return res.p_goods_amount;
                    }
                },totalRow: true}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });
        var logTableContent = JSON.parse( '<?php echo json_encode($oper_log); ?>' );
        table.render({ //操作日志表
            elem: '#log'
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : logTableContent
            ,cols: [[
                {field:'oper', title:'状态',templet:function (res) {
                    if(res.oper){
                        return res.oper;
                    }
                    if(res.order_state==1){ //1、取消入库
                        return '取消入库';//0 取消审核，1 已审核，2待入库3 已入库
                    }else if(res.order_state==2){
                        return '待入库';
                    }else if(res.order_state==3){ //3
                        return '已入库';
                    }
                }}
                ,{field:'oper_user', title:'操作人'}
                ,{field:'ctime', title:'操作时间'}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });


        //ajax获取数据然后动态赋值
        $.ajax({
            url: "<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>",
            type: 'POST',
            data: { },
            dataType: 'json',
            asyn:false,
            beforeSend: function () {

            },
            success: function (result) {
                if(result['code'] == 0){
                    tableContent = result['data'];
                    tableIns.reload({data:tableContent});
                }
            },
            error: function (error) {
                // parent.layer.close(layLoad);
                layer.msg('系统错误：' + error.status, {shift: 6});
            }
        });


        $('#add_goods').click(function (e) {


            child_idx = layer.open({
                type: 2 //此处以iframe举例
                ,title: '选择商品'
                ,area: [$(window).width()-390+'px', $(window).height()-200+'px']
                ,shade: 0
                ,maxmin: true
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: "<?php echo url('selectGoods'); ?>?hisi_iframe=yes"
                ,btn: ['选择商品添加', '关闭'] //只是为了演示
                ,yes: function(){
                    //父页面给子弹窗页面赋值
//                    $("#layui-layer-iframe" + child_idx).contents().find("input").val("11111111");
                    //父页面调用子弹窗的 js方法
                    var contentWindow = $("#layui-layer-iframe" + child_idx)[0].contentWindow;
                    contentWindow.addAllGoods();
                    layer.closeAll();
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,zIndex: layer.zIndex //重点1
                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });


    });

</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; default: ?>
        
        <div class="page-tab-content">
            <form class="layui-form" action="<?php echo url(); ?>" method="post" id="editForm">
    <div class="layui-form-item">
        <label class="layui-form-label w100">采购单号</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-p_order_sn" name="p_order_sn" lay-verify="required" autocomplete="off" value="<?php echo isset($p_order_sn)?$p_order_sn:''; ?>">
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商</label>
        <div class="layui-input-inline w200">
            <select name="supplier_id" class="field-supplier_id" type="select" lay-verify="required" lay-filter="supplier_id" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($supplier as $key=>$value): ?>
                <option value="<?php echo htmlentities($value['supplier_id']); ?>"><?php echo htmlentities($value['supplier_name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款方式</label>
        <div class="layui-input-inline w200">
            <select name="payment_type" class="field-payment_type" type="select" lay-verify="required" readonly="readonly">
                <option value="">请选择</option>
                <?php foreach($payment_type as $key=>$value): ?>
                <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-form-mid layui-word-aux">必填项</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商联系人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_contacts" name="supplier_contacts" lay-verify="" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">供应商手机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_phone" name="supplier_phone" lay-verify="mobile" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">供应商座机</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-supplier_telephone" name="supplier_telephone" lay-verify="telephone" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-inline w600">
            <textarea name="p_order_info" placeholder="" class="layui-textarea field-p_order_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>采购商品</legend>
    </fieldset>

    <table class="layui-hide" id="test" lay-filter="test"></table>
    <?php if(!empty($oper_log)): ?>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>操作记录</legend>
    </fieldset>
    <table class="layui-hide" id="log" lay-filter="test"></table>
    <?php endif; ?>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-p_order_id" name="p_order_id">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <a href="<?php echo url('orders'); ?>" class="layui-btn layui-btn-primary ml10">返回</a>
        </div>
    </div>
</form>
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form'], function() {
        var  $ = layui.jquery, layer = layui.layer, form = layui.form;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);

    });
    var child_idx;
    var tableIns;
    var tableContent = new Array();
    //    tableContent = [{goods_id: 2, goods_name: 2, goods_number: 1, goods_spec: "反渗透滤芯", goods_price: 0,goods_recommend_price:0,
    //        category_name:'ddd',brand_name:'rer',goods_stock:1}]
    layui.use('table', function(){
        var table = layui.table,
            $ = layui.jquery, layer = layui.layer;
        //温馨提示：默认由前端自动合计当前行数据。从 layui 2.5.6 开始： 若接口直接返回了合计行数据，则优先读取接口合计行数据。
        //详见：https://www.layui.com/doc/modules/table.html#totalRow
        tableIns = table.render({
            elem: '#test'
//            ,url:"<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>"
//            ,toolbar: '#toolbarDemo'
            ,data : tableContent
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : tableContent
            ,cols: [[
//                {type: 'checkbox', fixed: 'left'}
//                ,{field:'id', title:'ID', width:80, fixed: 'left', unresize: true}
                {field:'goods_number', title:'商品编号', edit: 'text', templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        var tmp_html = ' <input type="hidden" name="goodsId[]" value="'+res.goods_id+'">'
                            +res.goods_number
                            +' <input type="hidden" name="pGoodsId[]" value="'+(res.p_goods_id?res.p_goods_id:"")+'">';
                        return tmp_html;
                    }
                }}//,totalRowText: '采购金额合计:'
                ,{field:'goods_name', title:'商品名称', edit: 'text'}
                ,{field:'goods_spec', title:'商品规格'}
                ,{field:'goods_unit', title:'单位', edit: 'text'}
                ,{field:'p_goods_price', title:'采购单价', sort: true}
                ,{field:'p_goods_buy_num', title:'采购数量', sort: true}
                ,{field:'p_goods_tax', title:'税金', sort: true}
                ,{field:'p_goods_amount', title:'采购总价',width:200, sort: true,templet: function(res){
                    if(!res.goods_id){
                        return '采购金额合计:<span id="order-amount">'+res.p_goods_amount+'</span>';
                    }else{
                        return res.p_goods_amount;
                    }
                },totalRow: true}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });
        var logTableContent = JSON.parse( '<?php echo json_encode($oper_log); ?>' );
        table.render({ //操作日志表
            elem: '#log'
            ,title: '采购商品'
            ,totalRow: true
            // 拿对象数组tableContent中的数据作为原始数据渲染数据表格
            ,data : logTableContent
            ,cols: [[
                {field:'oper', title:'状态',templet:function (res) {
                    if(res.oper){
                        return res.oper;
                    }
                    if(res.order_state==1){ //1、取消入库
                        return '取消入库';//0 取消审核，1 已审核，2待入库3 已入库
                    }else if(res.order_state==2){
                        return '待入库';
                    }else if(res.order_state==3){ //3
                        return '已入库';
                    }
                }}
                ,{field:'oper_user', title:'操作人'}
                ,{field:'ctime', title:'操作时间'}
            ]]
            ,page: false
            ,done : function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                console.log(res);
                //得到当前页码
                console.log(curr);
                //得到数据总量
                console.log(count);
            }
        });


        //ajax获取数据然后动态赋值
        $.ajax({
            url: "<?php echo url('getProducts'); ?>?p_order_id=<?php echo !empty($formData) ? htmlentities($formData['p_order_id']) : ''; ?>",
            type: 'POST',
            data: { },
            dataType: 'json',
            asyn:false,
            beforeSend: function () {

            },
            success: function (result) {
                if(result['code'] == 0){
                    tableContent = result['data'];
                    tableIns.reload({data:tableContent});
                }
            },
            error: function (error) {
                // parent.layer.close(layLoad);
                layer.msg('系统错误：' + error.status, {shift: 6});
            }
        });


        $('#add_goods').click(function (e) {


            child_idx = layer.open({
                type: 2 //此处以iframe举例
                ,title: '选择商品'
                ,area: [$(window).width()-390+'px', $(window).height()-200+'px']
                ,shade: 0
                ,maxmin: true
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: "<?php echo url('selectGoods'); ?>?hisi_iframe=yes"
                ,btn: ['选择商品添加', '关闭'] //只是为了演示
                ,yes: function(){
                    //父页面给子弹窗页面赋值
//                    $("#layui-layer-iframe" + child_idx).contents().find("input").val("11111111");
                    //父页面调用子弹窗的 js方法
                    var contentWindow = $("#layui-layer-iframe" + child_idx)[0].contentWindow;
                    contentWindow.addAllGoods();
                    layer.closeAll();
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,zIndex: layer.zIndex //重点1
                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });


    });

</script>
        </div>
<?php endswitch; if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
</body>
</html>
<?php else: ?>
        </div>
    </div>
    <!--<div class="layui-footer footer">
        <span class="fl">Powered by <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.name'); ?></a> v<?php echo config('hisiphp.version'); ?></span>
        <span class="fr"> © 2018-2020 <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.copyright'); ?></a> All Rights Reserved.</span>
    </div>-->
</div>
</body>
</html>
<?php endif; ?>