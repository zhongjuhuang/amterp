<?php /*a:6:{s:60:"D:\wwwroot\hisiphp\application\erp\view\finance\receive.html";i:1590992909;s:54:"D:\wwwroot\hisiphp\application\system\view\layout.html";i:1590637517;s:60:"D:\wwwroot\hisiphp\application\system\view\block\header.html";i:1590637519;s:58:"D:\wwwroot\hisiphp\application\system\view\block\menu.html";i:1590637518;s:59:"D:\wwwroot\hisiphp\application\system\view\block\layui.html";i:1590637518;s:60:"D:\wwwroot\hisiphp\application\system\view\block\footer.html";i:1590637519;}*/ ?>
<?php if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo htmlentities($hisiCurMenu['title']); ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="hisi-theme-<?php echo cookie('hisi_admin_theme'); ?> pb50">
<?php else: ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if($hisiCurMenu['url'] == 'system/index/index'): ?>管理控制台<?php else: ?><?php echo htmlentities($hisiCurMenu['title']); ?><?php endif; ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="layui-layout-body hisi-theme-<?php echo cookie('hisi_admin_theme'); ?>">
<?php 
$ca = strtolower(request()->controller().'/'.request()->action());
 ?>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header" style="z-index:999!important;">
    <div class="fl header-logo"><img style="height: 66px" src="<?php echo config('base.site_logo'); ?>"><!--后台管理中心--></div>
    <div class="fl header-fold"><a href="javascript:;" title="打开/关闭左侧导航" class="aicon ai-shouqicaidan" id="foldSwitch"></a></div>
    <ul class="layui-nav fl nobg main-nav">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $vo['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $vo['id'] == 3)): ?>
           <li class="layui-nav-item layui-this">
            <?php else: ?>
            <li class="layui-nav-item">
            <?php endif; ?> 
            <a href="javascript:;"><?php echo htmlentities($vo['title']); ?></a></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
    <ul class="layui-nav fr nobg head-info">
        <!--<li class="layui-nav-item">
            <a href="/" target="_blank" class="aicon ai-ai-home" title="前台"></a>
        </li>
        <li class="layui-nav-item">
            <a href="javascript:void(0);" class="aicon ai-qingchu" id="hisi-clear-cache" title="清缓存"></a>
        </li>
        <li class="layui-nav-item">
        <a href="javascript:void(0);" class="aicon ai-suo" id="lockScreen" title="锁屏"></a>
        </li>
        <li class="layui-nav-item">
            <a href="<?php echo url('system/user/setTheme'); ?>" id="hisi-theme-setting" class="aicon ai-theme"></a>
        </li>
        <li class="layui-nav-item hisi-lang">
            <a href="javascript:void(0);"><i class="layui-icon layui-icon-website"></i></a>
            <dl class="layui-nav-child">
                <?php if(is_array($languages) || $languages instanceof \think\Collection || $languages instanceof \think\Paginator): $i = 0; $__LIST__ = $languages;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if($vo['pack']): ?>
                <dd><a href="<?php echo url('system/index/index'); ?>?lang=<?php echo htmlentities($vo['code']); ?>"><?php echo htmlentities($vo['name']); ?></a></dd>
                <?php endif; ?>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                <dd>
                    <a data-id="000" class="admin-nav-item top-nav-item" href="<?php echo url('system/language/index'); ?>">语言包管理</a>
                </dd>
            </dl>
        </li>-->
        <li class="layui-nav-item">
            <a href="javascript:void(0);"><?php echo htmlentities($login['nick']); ?>&nbsp;&nbsp;</a>
            <dl class="layui-nav-child">
                <dd>
                    <a data-id="00" class="admin-nav-item top-nav-item" href="<?php echo url('system/user/info'); ?>">个人设置</a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/user/iframe'); ?>" class="hisi-ajax" refresh="true"><?php echo input('cookie.hisi_iframe') ? '单页布局' : '框架布局'; ?></a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/publics/logout'); ?>">退出登陆</a>
                </dd>
            </dl>
        </li>
    </ul>
</div>
<div class="layui-side layui-bg-black" id="switchNav">
    <div class="layui-side-scroll">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $v['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $v['id'] == 3)): ?>
        <ul class="layui-nav layui-nav-tree">
        <?php else: ?>
        <ul class="layui-nav layui-nav-tree" style="display:none;">
        <?php endif; if((isset($v['childs']))): if(is_array($v['childs']) || $v['childs'] instanceof \think\Collection || $v['childs'] instanceof \think\Paginator): $kk = 0; $__LIST__ = $v['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($kk % 2 );++$kk;?>
            <li class="layui-nav-item <?php if($kk == 1): ?>layui-nav-itemed<?php endif; ?>">
                <a href="javascript:;"><i class="<?php echo htmlentities($vv['icon']); ?>"></i><?php echo htmlentities($vv['title']); ?><span class="layui-nav-more"></span></a>
                <dl class="layui-nav-child">
                    <?php if($vv['title'] == '快捷菜单'): ?>
                        <dd>
                            <a class="admin-nav-item" data-id="0" href="<?php echo input('cookie.hisi_iframe') ? url('system/index/welcome') : url('system/index/index'); ?>"><i class="aicon ai-shouye"></i> 后台首页</a>
                        </dd>
                        <?php if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a><i data-href="<?php echo url('system/menu/del?id='.$vvv['id']); ?>" class="layui-icon j-del-menu">&#xe640;</i>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; else: if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </dl>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        <?php endif; ?>
        </ul>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<script type="text/html" id="hisi-theme-tpl">
    <ul class="hisi-themes">
        <?php $_result=session('hisi_admin_themes');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <li data-theme="<?php echo htmlentities($vo); ?>" class="hisi-theme-item-<?php echo htmlentities($vo); ?>"></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
</script>
<script type="text/html" id="hisi-clear-cache-tpl">
    <form class="layui-form" style="padding:10px 0 0 30px;" action="<?php echo url('system/index/clear'); ?>" method="post">
        <div class="layui-form-item">
            <input type="checkbox" name="cache" value="1" title="数据缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="log" value="1" title="日志缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="temp" value="1" title="模板缓存" />
        </div>
        <div class="layui-form-item">
            <button class="layui-btn layui-btn-normal" lay-submit="" lay-filter="formSubmit">执行删除</button>
        </div>
    </form>
</script>
    <div class="layui-body" id="switchBody">
        <ul class="bread-crumbs">
            <?php if(is_array($hisiBreadcrumb) || $hisiBreadcrumb instanceof \think\Collection || $hisiBreadcrumb instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiBreadcrumb;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if($key > 0 && $i != count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="<?php echo url($v['url'].'?'.$v['param']); ?>"><?php echo htmlentities($v['title']); ?></a></li>
                <?php elseif($i == count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php else: ?>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php endif; ?>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <li><a href="<?php echo url('system/menu/quick?id='.$hisiCurMenu['id']); ?>" title="添加到首页快捷菜单" class="j-ajax">[+]</a></li>
        </ul>
        <div style="padding:0 10px;" class="mcolor"><?php echo runhook('system_admin_tips'); ?></div>
            <div class="page-body">
<?php endif; switch($hisiTabType): case "1": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <li class="layui-this">
                        <a href="javascript:;" id="curTitle"><?php echo $hisiCurMenu['title']; ?></a>
                    </li>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>收款处理</legend>
    </fieldset>
    <div class="layui-form-item">
        <label class="layui-form-label w200">销售单号:<?php echo htmlentities($receivable['sales_order_sn']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">应收金额:¥<?php echo htmlentities($receivable['receivable_amount']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">已收金额:<span style="color:#5FB878">¥<?php echo htmlentities($receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">未收金额:<span class="red">¥<?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>

    </div>
<form class="layui-form" action="" method="post" id="signupForm">
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款金额</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_amount" name="receivable_log_amount" lay-verify="required" autocomplete="off" placeholder="">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款日期</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input" lay-verify="required" name="receivable_log_time" id="pay_log_paytime" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo date('Y-m-d H:i:s'); ?>">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_user" name="receivable_log_user" lay-verify="required" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red"></i>付款凭证</label>
        <div class="layui-input-inline w200">
            <button type="button" class="layui-btn" id="test2">上传凭证</button>
            <!--或者-->
            <input id="receivable_file" name="receivable_file" class="form-control field-receivable_file" type="text"
                   style="width: 0px;height: 0px;padding: 0px" value="<?php echo !empty($formData) ? htmlentities($formData['receivable_file']) : ''; ?>"> <!-- 图片地址隐藏域-->
            <img src="" style="width: 50px" id="upload_pic" >
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款备注</label>
        <div class="layui-input-inline w600">
            <textarea name="receivable_info" placeholder="" class="layui-textarea field-receivable_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-receivable_id" name="receivable_id" value="<?php echo htmlentities($receivable['receivable_id']); ?>">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <button class="layui-btn" lay-submit lay-filter="formSub">提交</button>
            <a href="<?php echo url('receivable'); ?>" class="layui-btn layui-btn-primary ml10">取消</a>
        </div>
    </div>
</form>
<!--放大的显示图片-->
<img alt="" style="display:none" id="displayImg" src="" />
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form','laydate','upload'], function() {
        var  $ = layui.jquery,
            layer = layui.layer,
            form = layui.form,
            laydate = layui.laydate,
            upload = layui.upload;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);
        //日期时间选择器
        laydate.render({
            elem: '#pay_log_paytime'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
//普通图片上传
        var uploadInst = upload.render({
            elem: '#test2'
            , url: '<?php echo url("system/annex/upload?water=&thumb=&from=&group="); ?>'
            , before: function (obj) {
                layLoad = parent.layer.load(1, {
                    shade: [0.5, '#000']
                });
                //预读本地文件示例，不支持ie8
                /* obj.preview(function (index, file, result) {
                 $('#demo1').attr('src', result); //图片链接（base64）
                 });*/
            }
            , done: function (res) {
                if (res.code == 0) {
                    parent.layer.close(layLoad);
                    return parent.layer.msg('错误：' + res.msg, {shift: 6});
                }
                $("#receivable_file").val(res.data.file);
                $("#upload_pic").attr('src',res.data.file);
                $("#upload_pic").attr('onclick','amplificationImg("'+res.data.file+'")');

                parent.layer.close(layLoad);

                $(".choose").show();
                layer.msg('上传成功');

                //上传成功
            }
            , error: function () {
                //演示失败状态，并实现重传
                parent.layer.close(layLoad);
//                    uploadInst.upload();
                layer.msg('上传失败，请重试');
            }
        });
        //监听提交
        form.on('submit(formSub)', function(data){

            var receivable_log_amount = $('[name="receivable_log_amount"]').val();
            if (receivable_log_amount == '') {
                layer.msg("付款金额不能为空");
                return false;
            }
            var fix_amountTest=/^(([1-9]\d*)|\d)([\.]\d{0,2})?$/;
            console.log(receivable_log_amount,fix_amountTest.test(receivable_log_amount));
            if(fix_amountTest.test(receivable_log_amount) == false){
                layer.msg("请输入有效金额");
                return false;
            }

            if(receivable_log_amount><?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?>){
                layer.msg("付款金额不能大于未付金额");
                return false;
            }
            var layLoad;

            var serializeArray = $("#signupForm").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })

            $.ajax({
                url: "<?php echo url('receive'); ?>",
                type: 'POST',
                data: data,
                dataType: 'json',
                async: false,
                beforeSend: function () {
                    layLoad = parent.layer.load(1, {
                        shade: [0.5, '#000']
                    });
                },
                success: function (result) {
                    parent.layer.close(layLoad);
                    if (result.code != 1) {
                        parent.layer.msg('错误：' + result.msg, {shift: 6});
                        return;
                    }
                    parent.layer.msg('成功：' + result.msg, {shift: 6});
                    setTimeout(function () {
                        window.location.href='<?php echo url("receivable"); ?>'
                    },1000)

                },
                error: function (error) {
                    console.info(error);
                    parent.layer.close(layLoad);
                    parent.layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
            return false;
        });




    });
    //点击放大图片
    function amplificationImg( url) {
        $("#displayImg").attr("src", url);
        var height = $("#displayImg").height();//拿的图片原来宽高，建议自定义宽高
        var width = $("#displayImg").width();
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            shadeClose: true,
            area: [width + 'px', height + 'px'], //宽高
            content: "<img src=" + url + " />"
        });
    }
</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; case "2": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $k = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;if(($k == 1)): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; ?>
                            <a href="javascript:;" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>收款处理</legend>
    </fieldset>
    <div class="layui-form-item">
        <label class="layui-form-label w200">销售单号:<?php echo htmlentities($receivable['sales_order_sn']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">应收金额:¥<?php echo htmlentities($receivable['receivable_amount']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">已收金额:<span style="color:#5FB878">¥<?php echo htmlentities($receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">未收金额:<span class="red">¥<?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>

    </div>
<form class="layui-form" action="" method="post" id="signupForm">
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款金额</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_amount" name="receivable_log_amount" lay-verify="required" autocomplete="off" placeholder="">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款日期</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input" lay-verify="required" name="receivable_log_time" id="pay_log_paytime" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo date('Y-m-d H:i:s'); ?>">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_user" name="receivable_log_user" lay-verify="required" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red"></i>付款凭证</label>
        <div class="layui-input-inline w200">
            <button type="button" class="layui-btn" id="test2">上传凭证</button>
            <!--或者-->
            <input id="receivable_file" name="receivable_file" class="form-control field-receivable_file" type="text"
                   style="width: 0px;height: 0px;padding: 0px" value="<?php echo !empty($formData) ? htmlentities($formData['receivable_file']) : ''; ?>"> <!-- 图片地址隐藏域-->
            <img src="" style="width: 50px" id="upload_pic" >
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款备注</label>
        <div class="layui-input-inline w600">
            <textarea name="receivable_info" placeholder="" class="layui-textarea field-receivable_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-receivable_id" name="receivable_id" value="<?php echo htmlentities($receivable['receivable_id']); ?>">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <button class="layui-btn" lay-submit lay-filter="formSub">提交</button>
            <a href="<?php echo url('receivable'); ?>" class="layui-btn layui-btn-primary ml10">取消</a>
        </div>
    </div>
</form>
<!--放大的显示图片-->
<img alt="" style="display:none" id="displayImg" src="" />
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form','laydate','upload'], function() {
        var  $ = layui.jquery,
            layer = layui.layer,
            form = layui.form,
            laydate = layui.laydate,
            upload = layui.upload;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);
        //日期时间选择器
        laydate.render({
            elem: '#pay_log_paytime'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
//普通图片上传
        var uploadInst = upload.render({
            elem: '#test2'
            , url: '<?php echo url("system/annex/upload?water=&thumb=&from=&group="); ?>'
            , before: function (obj) {
                layLoad = parent.layer.load(1, {
                    shade: [0.5, '#000']
                });
                //预读本地文件示例，不支持ie8
                /* obj.preview(function (index, file, result) {
                 $('#demo1').attr('src', result); //图片链接（base64）
                 });*/
            }
            , done: function (res) {
                if (res.code == 0) {
                    parent.layer.close(layLoad);
                    return parent.layer.msg('错误：' + res.msg, {shift: 6});
                }
                $("#receivable_file").val(res.data.file);
                $("#upload_pic").attr('src',res.data.file);
                $("#upload_pic").attr('onclick','amplificationImg("'+res.data.file+'")');

                parent.layer.close(layLoad);

                $(".choose").show();
                layer.msg('上传成功');

                //上传成功
            }
            , error: function () {
                //演示失败状态，并实现重传
                parent.layer.close(layLoad);
//                    uploadInst.upload();
                layer.msg('上传失败，请重试');
            }
        });
        //监听提交
        form.on('submit(formSub)', function(data){

            var receivable_log_amount = $('[name="receivable_log_amount"]').val();
            if (receivable_log_amount == '') {
                layer.msg("付款金额不能为空");
                return false;
            }
            var fix_amountTest=/^(([1-9]\d*)|\d)([\.]\d{0,2})?$/;
            console.log(receivable_log_amount,fix_amountTest.test(receivable_log_amount));
            if(fix_amountTest.test(receivable_log_amount) == false){
                layer.msg("请输入有效金额");
                return false;
            }

            if(receivable_log_amount><?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?>){
                layer.msg("付款金额不能大于未付金额");
                return false;
            }
            var layLoad;

            var serializeArray = $("#signupForm").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })

            $.ajax({
                url: "<?php echo url('receive'); ?>",
                type: 'POST',
                data: data,
                dataType: 'json',
                async: false,
                beforeSend: function () {
                    layLoad = parent.layer.load(1, {
                        shade: [0.5, '#000']
                    });
                },
                success: function (result) {
                    parent.layer.close(layLoad);
                    if (result.code != 1) {
                        parent.layer.msg('错误：' + result.msg, {shift: 6});
                        return;
                    }
                    parent.layer.msg('成功：' + result.msg, {shift: 6});
                    setTimeout(function () {
                        window.location.href='<?php echo url("receivable"); ?>'
                    },1000)

                },
                error: function (error) {
                    console.info(error);
                    parent.layer.close(layLoad);
                    parent.layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
            return false;
        });




    });
    //点击放大图片
    function amplificationImg( url) {
        $("#displayImg").attr("src", url);
        var height = $("#displayImg").height();//拿的图片原来宽高，建议自定义宽高
        var width = $("#displayImg").width();
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            shadeClose: true,
            area: [width + 'px', height + 'px'], //宽高
            content: "<img src=" + url + " />"
        });
    }
</script>
                </div>
            </div>
        </div>
    <?php break; case "3": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;
                            $hisiTabData['current'] = isset($hisiTabData['current']) ? $hisiTabData['current'] : '';
                         if(($vo['url'] == $hisiCurMenu['url'] or (url($vo['url']) == $hisiTabData['current']))): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; if((strpos($vo['url'], 'http'))): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" target="_blank"><?php echo $vo['title']; ?></a>
                            <?php elseif((strpos($vo['url'], config('sys.admin_path')) !== false)): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php else: ?>
                                <a href="<?php echo url($vo['url']); ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>收款处理</legend>
    </fieldset>
    <div class="layui-form-item">
        <label class="layui-form-label w200">销售单号:<?php echo htmlentities($receivable['sales_order_sn']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">应收金额:¥<?php echo htmlentities($receivable['receivable_amount']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">已收金额:<span style="color:#5FB878">¥<?php echo htmlentities($receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">未收金额:<span class="red">¥<?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>

    </div>
<form class="layui-form" action="" method="post" id="signupForm">
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款金额</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_amount" name="receivable_log_amount" lay-verify="required" autocomplete="off" placeholder="">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款日期</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input" lay-verify="required" name="receivable_log_time" id="pay_log_paytime" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo date('Y-m-d H:i:s'); ?>">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_user" name="receivable_log_user" lay-verify="required" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red"></i>付款凭证</label>
        <div class="layui-input-inline w200">
            <button type="button" class="layui-btn" id="test2">上传凭证</button>
            <!--或者-->
            <input id="receivable_file" name="receivable_file" class="form-control field-receivable_file" type="text"
                   style="width: 0px;height: 0px;padding: 0px" value="<?php echo !empty($formData) ? htmlentities($formData['receivable_file']) : ''; ?>"> <!-- 图片地址隐藏域-->
            <img src="" style="width: 50px" id="upload_pic" >
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款备注</label>
        <div class="layui-input-inline w600">
            <textarea name="receivable_info" placeholder="" class="layui-textarea field-receivable_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-receivable_id" name="receivable_id" value="<?php echo htmlentities($receivable['receivable_id']); ?>">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <button class="layui-btn" lay-submit lay-filter="formSub">提交</button>
            <a href="<?php echo url('receivable'); ?>" class="layui-btn layui-btn-primary ml10">取消</a>
        </div>
    </div>
</form>
<!--放大的显示图片-->
<img alt="" style="display:none" id="displayImg" src="" />
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form','laydate','upload'], function() {
        var  $ = layui.jquery,
            layer = layui.layer,
            form = layui.form,
            laydate = layui.laydate,
            upload = layui.upload;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);
        //日期时间选择器
        laydate.render({
            elem: '#pay_log_paytime'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
//普通图片上传
        var uploadInst = upload.render({
            elem: '#test2'
            , url: '<?php echo url("system/annex/upload?water=&thumb=&from=&group="); ?>'
            , before: function (obj) {
                layLoad = parent.layer.load(1, {
                    shade: [0.5, '#000']
                });
                //预读本地文件示例，不支持ie8
                /* obj.preview(function (index, file, result) {
                 $('#demo1').attr('src', result); //图片链接（base64）
                 });*/
            }
            , done: function (res) {
                if (res.code == 0) {
                    parent.layer.close(layLoad);
                    return parent.layer.msg('错误：' + res.msg, {shift: 6});
                }
                $("#receivable_file").val(res.data.file);
                $("#upload_pic").attr('src',res.data.file);
                $("#upload_pic").attr('onclick','amplificationImg("'+res.data.file+'")');

                parent.layer.close(layLoad);

                $(".choose").show();
                layer.msg('上传成功');

                //上传成功
            }
            , error: function () {
                //演示失败状态，并实现重传
                parent.layer.close(layLoad);
//                    uploadInst.upload();
                layer.msg('上传失败，请重试');
            }
        });
        //监听提交
        form.on('submit(formSub)', function(data){

            var receivable_log_amount = $('[name="receivable_log_amount"]').val();
            if (receivable_log_amount == '') {
                layer.msg("付款金额不能为空");
                return false;
            }
            var fix_amountTest=/^(([1-9]\d*)|\d)([\.]\d{0,2})?$/;
            console.log(receivable_log_amount,fix_amountTest.test(receivable_log_amount));
            if(fix_amountTest.test(receivable_log_amount) == false){
                layer.msg("请输入有效金额");
                return false;
            }

            if(receivable_log_amount><?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?>){
                layer.msg("付款金额不能大于未付金额");
                return false;
            }
            var layLoad;

            var serializeArray = $("#signupForm").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })

            $.ajax({
                url: "<?php echo url('receive'); ?>",
                type: 'POST',
                data: data,
                dataType: 'json',
                async: false,
                beforeSend: function () {
                    layLoad = parent.layer.load(1, {
                        shade: [0.5, '#000']
                    });
                },
                success: function (result) {
                    parent.layer.close(layLoad);
                    if (result.code != 1) {
                        parent.layer.msg('错误：' + result.msg, {shift: 6});
                        return;
                    }
                    parent.layer.msg('成功：' + result.msg, {shift: 6});
                    setTimeout(function () {
                        window.location.href='<?php echo url("receivable"); ?>'
                    },1000)

                },
                error: function (error) {
                    console.info(error);
                    parent.layer.close(layLoad);
                    parent.layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
            return false;
        });




    });
    //点击放大图片
    function amplificationImg( url) {
        $("#displayImg").attr("src", url);
        var height = $("#displayImg").height();//拿的图片原来宽高，建议自定义宽高
        var width = $("#displayImg").width();
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            shadeClose: true,
            area: [width + 'px', height + 'px'], //宽高
            content: "<img src=" + url + " />"
        });
    }
</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; default: ?>
        
        <div class="page-tab-content">
            
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>收款处理</legend>
    </fieldset>
    <div class="layui-form-item">
        <label class="layui-form-label w200">销售单号:<?php echo htmlentities($receivable['sales_order_sn']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">应收金额:¥<?php echo htmlentities($receivable['receivable_amount']); ?></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">已收金额:<span style="color:#5FB878">¥<?php echo htmlentities($receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>
        <label class="layui-form-label w200">未收金额:<span class="red">¥<?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?></span></label>
        <div class="layui-input-inline w200">

        </div>

    </div>
<form class="layui-form" action="" method="post" id="signupForm">
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款金额</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_amount" name="receivable_log_amount" lay-verify="required" autocomplete="off" placeholder="">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款日期</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input" lay-verify="required" name="receivable_log_time" id="pay_log_paytime" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo date('Y-m-d H:i:s'); ?>">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red">*</i>付款人</label>
        <div class="layui-input-inline w200">
            <input type="text" class="layui-input field-receivable_log_user" name="receivable_log_user" lay-verify="required" autocomplete="off" placeholder="">
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><i class="red"></i>付款凭证</label>
        <div class="layui-input-inline w200">
            <button type="button" class="layui-btn" id="test2">上传凭证</button>
            <!--或者-->
            <input id="receivable_file" name="receivable_file" class="form-control field-receivable_file" type="text"
                   style="width: 0px;height: 0px;padding: 0px" value="<?php echo !empty($formData) ? htmlentities($formData['receivable_file']) : ''; ?>"> <!-- 图片地址隐藏域-->
            <img src="" style="width: 50px" id="upload_pic" >
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">付款备注</label>
        <div class="layui-input-inline w600">
            <textarea name="receivable_info" placeholder="" class="layui-textarea field-receivable_info"></textarea>
        </div>
        <div class="layui-form-mid layui-word-aux"></div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" class="field-receivable_id" name="receivable_id" value="<?php echo htmlentities($receivable['receivable_id']); ?>">
            <input type="hidden" class="field-admin_id" name="admin_id" value="<?php echo ADMIN_ID; ?>">
            <button class="layui-btn" lay-submit lay-filter="formSub">提交</button>
            <a href="<?php echo url('receivable'); ?>" class="layui-btn layui-btn-primary ml10">取消</a>
        </div>
    </div>
</form>
<!--放大的显示图片-->
<img alt="" style="display:none" id="displayImg" src="" />
<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="javascript:;" onclick="delCell({{ d.goods_id }})" class="layui-btn layui-btn-xs layui-btn-danger">删除</a>
</script>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script>
    layui.use(['func','form','laydate','upload'], function() {
        var  $ = layui.jquery,
            layer = layui.layer,
            form = layui.form,
            laydate = layui.laydate,
            upload = layui.upload;
        // 编辑模式下表单自动赋值
        layui.func.assign(<?php echo json_encode($formData); ?>);
        //日期时间选择器
        laydate.render({
            elem: '#pay_log_paytime'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
//普通图片上传
        var uploadInst = upload.render({
            elem: '#test2'
            , url: '<?php echo url("system/annex/upload?water=&thumb=&from=&group="); ?>'
            , before: function (obj) {
                layLoad = parent.layer.load(1, {
                    shade: [0.5, '#000']
                });
                //预读本地文件示例，不支持ie8
                /* obj.preview(function (index, file, result) {
                 $('#demo1').attr('src', result); //图片链接（base64）
                 });*/
            }
            , done: function (res) {
                if (res.code == 0) {
                    parent.layer.close(layLoad);
                    return parent.layer.msg('错误：' + res.msg, {shift: 6});
                }
                $("#receivable_file").val(res.data.file);
                $("#upload_pic").attr('src',res.data.file);
                $("#upload_pic").attr('onclick','amplificationImg("'+res.data.file+'")');

                parent.layer.close(layLoad);

                $(".choose").show();
                layer.msg('上传成功');

                //上传成功
            }
            , error: function () {
                //演示失败状态，并实现重传
                parent.layer.close(layLoad);
//                    uploadInst.upload();
                layer.msg('上传失败，请重试');
            }
        });
        //监听提交
        form.on('submit(formSub)', function(data){

            var receivable_log_amount = $('[name="receivable_log_amount"]').val();
            if (receivable_log_amount == '') {
                layer.msg("付款金额不能为空");
                return false;
            }
            var fix_amountTest=/^(([1-9]\d*)|\d)([\.]\d{0,2})?$/;
            console.log(receivable_log_amount,fix_amountTest.test(receivable_log_amount));
            if(fix_amountTest.test(receivable_log_amount) == false){
                layer.msg("请输入有效金额");
                return false;
            }

            if(receivable_log_amount><?php echo htmlentities($receivable['receivable_amount']-$receivable['finish_amount']); ?>){
                layer.msg("付款金额不能大于未付金额");
                return false;
            }
            var layLoad;

            var serializeArray = $("#signupForm").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })

            $.ajax({
                url: "<?php echo url('receive'); ?>",
                type: 'POST',
                data: data,
                dataType: 'json',
                async: false,
                beforeSend: function () {
                    layLoad = parent.layer.load(1, {
                        shade: [0.5, '#000']
                    });
                },
                success: function (result) {
                    parent.layer.close(layLoad);
                    if (result.code != 1) {
                        parent.layer.msg('错误：' + result.msg, {shift: 6});
                        return;
                    }
                    parent.layer.msg('成功：' + result.msg, {shift: 6});
                    setTimeout(function () {
                        window.location.href='<?php echo url("receivable"); ?>'
                    },1000)

                },
                error: function (error) {
                    console.info(error);
                    parent.layer.close(layLoad);
                    parent.layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
            return false;
        });




    });
    //点击放大图片
    function amplificationImg( url) {
        $("#displayImg").attr("src", url);
        var height = $("#displayImg").height();//拿的图片原来宽高，建议自定义宽高
        var width = $("#displayImg").width();
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            shadeClose: true,
            area: [width + 'px', height + 'px'], //宽高
            content: "<img src=" + url + " />"
        });
    }
</script>
        </div>
<?php endswitch; if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
</body>
</html>
<?php else: ?>
        </div>
    </div>
    <!--<div class="layui-footer footer">
        <span class="fl">Powered by <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.name'); ?></a> v<?php echo config('hisiphp.version'); ?></span>
        <span class="fr"> © 2018-2020 <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.copyright'); ?></a> All Rights Reserved.</span>
    </div>-->
</div>
</body>
</html>
<?php endif; ?>