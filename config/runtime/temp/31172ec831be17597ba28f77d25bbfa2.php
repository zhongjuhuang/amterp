<?php /*a:6:{s:61:"D:\wwwroot\hisiphp\application\erp\view\sales\send_order.html";i:1590742673;s:54:"D:\wwwroot\hisiphp\application\system\view\layout.html";i:1590637517;s:60:"D:\wwwroot\hisiphp\application\system\view\block\header.html";i:1590637519;s:58:"D:\wwwroot\hisiphp\application\system\view\block\menu.html";i:1590637518;s:56:"D:\wwwroot\hisiphp\application\erp\view\block\layui.html";i:1587354666;s:60:"D:\wwwroot\hisiphp\application\system\view\block\footer.html";i:1590637519;}*/ ?>
<?php if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo htmlentities($hisiCurMenu['title']); ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="hisi-theme-<?php echo cookie('hisi_admin_theme'); ?> pb50">
<?php else: ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if($hisiCurMenu['url'] == 'system/index/index'): ?>管理控制台<?php else: ?><?php echo htmlentities($hisiCurMenu['title']); ?><?php endif; ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="layui-layout-body hisi-theme-<?php echo cookie('hisi_admin_theme'); ?>">
<?php 
$ca = strtolower(request()->controller().'/'.request()->action());
 ?>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header" style="z-index:999!important;">
    <div class="fl header-logo"><img style="height: 66px" src="<?php echo config('base.site_logo'); ?>"><!--后台管理中心--></div>
    <div class="fl header-fold"><a href="javascript:;" title="打开/关闭左侧导航" class="aicon ai-shouqicaidan" id="foldSwitch"></a></div>
    <ul class="layui-nav fl nobg main-nav">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $vo['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $vo['id'] == 3)): ?>
           <li class="layui-nav-item layui-this">
            <?php else: ?>
            <li class="layui-nav-item">
            <?php endif; ?> 
            <a href="javascript:;"><?php echo htmlentities($vo['title']); ?></a></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
    <ul class="layui-nav fr nobg head-info">
        <!--<li class="layui-nav-item">
            <a href="/" target="_blank" class="aicon ai-ai-home" title="前台"></a>
        </li>
        <li class="layui-nav-item">
            <a href="javascript:void(0);" class="aicon ai-qingchu" id="hisi-clear-cache" title="清缓存"></a>
        </li>
        <li class="layui-nav-item">
        <a href="javascript:void(0);" class="aicon ai-suo" id="lockScreen" title="锁屏"></a>
        </li>
        <li class="layui-nav-item">
            <a href="<?php echo url('system/user/setTheme'); ?>" id="hisi-theme-setting" class="aicon ai-theme"></a>
        </li>
        <li class="layui-nav-item hisi-lang">
            <a href="javascript:void(0);"><i class="layui-icon layui-icon-website"></i></a>
            <dl class="layui-nav-child">
                <?php if(is_array($languages) || $languages instanceof \think\Collection || $languages instanceof \think\Paginator): $i = 0; $__LIST__ = $languages;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if($vo['pack']): ?>
                <dd><a href="<?php echo url('system/index/index'); ?>?lang=<?php echo htmlentities($vo['code']); ?>"><?php echo htmlentities($vo['name']); ?></a></dd>
                <?php endif; ?>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                <dd>
                    <a data-id="000" class="admin-nav-item top-nav-item" href="<?php echo url('system/language/index'); ?>">语言包管理</a>
                </dd>
            </dl>
        </li>-->
        <li class="layui-nav-item">
            <a href="javascript:void(0);"><?php echo htmlentities($login['nick']); ?>&nbsp;&nbsp;</a>
            <dl class="layui-nav-child">
                <dd>
                    <a data-id="00" class="admin-nav-item top-nav-item" href="<?php echo url('system/user/info'); ?>">个人设置</a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/user/iframe'); ?>" class="hisi-ajax" refresh="true"><?php echo input('cookie.hisi_iframe') ? '单页布局' : '框架布局'; ?></a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/publics/logout'); ?>">退出登陆</a>
                </dd>
            </dl>
        </li>
    </ul>
</div>
<div class="layui-side layui-bg-black" id="switchNav">
    <div class="layui-side-scroll">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $v['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $v['id'] == 3)): ?>
        <ul class="layui-nav layui-nav-tree">
        <?php else: ?>
        <ul class="layui-nav layui-nav-tree" style="display:none;">
        <?php endif; if((isset($v['childs']))): if(is_array($v['childs']) || $v['childs'] instanceof \think\Collection || $v['childs'] instanceof \think\Paginator): $kk = 0; $__LIST__ = $v['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($kk % 2 );++$kk;?>
            <li class="layui-nav-item <?php if($kk == 1): ?>layui-nav-itemed<?php endif; ?>">
                <a href="javascript:;"><i class="<?php echo htmlentities($vv['icon']); ?>"></i><?php echo htmlentities($vv['title']); ?><span class="layui-nav-more"></span></a>
                <dl class="layui-nav-child">
                    <?php if($vv['title'] == '快捷菜单'): ?>
                        <dd>
                            <a class="admin-nav-item" data-id="0" href="<?php echo input('cookie.hisi_iframe') ? url('system/index/welcome') : url('system/index/index'); ?>"><i class="aicon ai-shouye"></i> 后台首页</a>
                        </dd>
                        <?php if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a><i data-href="<?php echo url('system/menu/del?id='.$vvv['id']); ?>" class="layui-icon j-del-menu">&#xe640;</i>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; else: if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </dl>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        <?php endif; ?>
        </ul>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<script type="text/html" id="hisi-theme-tpl">
    <ul class="hisi-themes">
        <?php $_result=session('hisi_admin_themes');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <li data-theme="<?php echo htmlentities($vo); ?>" class="hisi-theme-item-<?php echo htmlentities($vo); ?>"></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
</script>
<script type="text/html" id="hisi-clear-cache-tpl">
    <form class="layui-form" style="padding:10px 0 0 30px;" action="<?php echo url('system/index/clear'); ?>" method="post">
        <div class="layui-form-item">
            <input type="checkbox" name="cache" value="1" title="数据缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="log" value="1" title="日志缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="temp" value="1" title="模板缓存" />
        </div>
        <div class="layui-form-item">
            <button class="layui-btn layui-btn-normal" lay-submit="" lay-filter="formSubmit">执行删除</button>
        </div>
    </form>
</script>
    <div class="layui-body" id="switchBody">
        <ul class="bread-crumbs">
            <?php if(is_array($hisiBreadcrumb) || $hisiBreadcrumb instanceof \think\Collection || $hisiBreadcrumb instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiBreadcrumb;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if($key > 0 && $i != count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="<?php echo url($v['url'].'?'.$v['param']); ?>"><?php echo htmlentities($v['title']); ?></a></li>
                <?php elseif($i == count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php else: ?>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php endif; ?>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <li><a href="<?php echo url('system/menu/quick?id='.$hisiCurMenu['id']); ?>" title="添加到首页快捷菜单" class="j-ajax">[+]</a></li>
        </ul>
        <div style="padding:0 10px;" class="mcolor"><?php echo runhook('system_admin_tips'); ?></div>
            <div class="page-body">
<?php endif; switch($hisiTabType): case "1": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <li class="layui-this">
                        <a href="javascript:;" id="curTitle"><?php echo $hisiCurMenu['title']; ?></a>
                    </li>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        <div class="page-toolbar">
    <div class="page-filter fr">
        <form class="layui-form layui-form-pane" action="<?php echo url('orders'); ?>" method="get" id="hisi-table-search">
            <div class="layui-form-item">
                <label class="layui-form-label">搜索</label>
                <div class="layui-input-inline">
                    <input type="text" name="keyword" lay-verify="required" placeholder="输入关键词搜索" class="layui-input">
                </div>
            </div>
        </form>
    </div>
    <div class="layui-btn-group fl">
        <a href="<?php echo url('add'); ?>" class="layui-btn layui-btn-primary layui-icon layui-icon-add-circle-fine">&nbsp;添加</a>
    </div>
</div>
<table id="dataTable"></table>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>

<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">修改</a>
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">验货入库</a>
    <a href="<?php echo url('detail'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs">查看</a>
    <a name="audit" data-url="<?php echo url('audit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-warm">审核</a>
    <a href="<?php echo url('del'); ?>?id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-danger j-tr-del">删除</a>
</script>
<script type="text/html" title="金额模版" id="moneyTpl">
    ¥{{#
    var finish_amount = d.sales_order.sales_order_amount*1;
    return '￥'+finish_amount.toFixed(2);
    }}
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script type="text/javascript">
    layui.use(['table','jquery'], function() {
        var table = layui.table,
            layer = layui.layer;
        var sales_order_state = <?php echo json_encode(config('erp.SALES_ORDER_STATE')); ?>;
        var receivable_code = <?php echo json_encode(config('erp.RECEIVABLE_CODE')); ?>;
        table.render({
            elem: '#dataTable'
            ,url: "<?php echo url('sendOrder'); ?>" //数据接口
            ,page: true //开启分页
            ,skin: 'row'
            ,even: true
            ,limit: 20
            ,text: {
                none : '暂无相关数据'
            }
            ,cols: [[ //表头
//                {type:'checkbox'}
                {field: 'send_order_sn',width:150, title: '发货单号'}
                ,{field: 'customer_id', title: '客户',templet:function (res) {
                    return res.sales_order.customer.customer_name
                }}
                ,{field: 'customer_contacts', title: '联系人',templet:function(res){
                    return res.sales_order.customer_contacts;
                }}
                ,{field: 'customer_phone', title: '联系电话',templet:function(res){
                    return res.sales_order.customer_phone;
                }}
                ,{field: 'receivable_code', title: '收款方式',templet:function(res){
                    return receivable_code[res.sales_order.receivable_code];
                }}
                ,{title: '收款金额', templet: '#moneyTpl'}
                ,{field: 'sales_order_state', title: '状态',templet:function(res){
                    return sales_order_state[res.sales_order.sales_order_state];
                }}
//                ,{field: 'return_state',width: 100, title: '有无退货'}
//                ,{title: '操作',  templet: '#buttonTpl'}
                ,{field: 'p_order_id',width: 155,title: '操作',  templet: function (res) {
                    var html = '<a href="<?php echo url('sendDetail'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs">查看</a>';
                    if(res.sales_order.sales_order_state == 0){
                        //确认、删除
                    }else if(res.sales_order.sales_order_state == 1){
                        //查看、采购退货、发货出库
                    }else if(res.sales_order.sales_order_state == 6){ //发货出库
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
                        html += '<a data-url="<?php echo url('confirm'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs  layui-btn-danger confirm-tips" data-msg="客户确认已收货？" >确认收货</a>';
                    }else if(res.sales_order.sales_order_state == 12){ //确认收货
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
//                        html += '<a href="<?php echo url('insertWarehouse'); ?>?p_order_id='+res.p_order_id+'" class="layui-btn layui-btn-xs layui-btn-normal confirm-tips">验货入库</a>';
                    }
                    return html;
                }}
            ]]
        });
        $(document).on("click", '.confirm-tips', function () {
            var url = $(this).attr('data-url');
            var title = $(this).attr('data-msg');
            layer.open({
                type: 1
                ,title: '提示'
                ,area: ['300px', '200px']
                ,shade: 0
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: '<div style="padding: 10px 10px;">'+title+'</div>'
                ,btn: ['确定', '取消'] //只是为了演示
                ,yes: function(){
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: { },
                        dataType: 'json',
                        asyn:false,
                        beforeSend: function () {},
                        success: function (result) {
                            if(result['code'] == 1){
                                layer.msg(result.msg);
                                setTimeout(window.location.reload(),1000);

                            }else{
                                layer.msg(result.msg, {shift: 6});
                            }
                        },
                        error: function (error) {
                            layer.msg('系统错误：' + error.status, {shift: 6});
                        }
                    });
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });
    });

</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; case "2": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $k = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;if(($k == 1)): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; ?>
                            <a href="javascript:;" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="page-toolbar">
    <div class="page-filter fr">
        <form class="layui-form layui-form-pane" action="<?php echo url('orders'); ?>" method="get" id="hisi-table-search">
            <div class="layui-form-item">
                <label class="layui-form-label">搜索</label>
                <div class="layui-input-inline">
                    <input type="text" name="keyword" lay-verify="required" placeholder="输入关键词搜索" class="layui-input">
                </div>
            </div>
        </form>
    </div>
    <div class="layui-btn-group fl">
        <a href="<?php echo url('add'); ?>" class="layui-btn layui-btn-primary layui-icon layui-icon-add-circle-fine">&nbsp;添加</a>
    </div>
</div>
<table id="dataTable"></table>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>

<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">修改</a>
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">验货入库</a>
    <a href="<?php echo url('detail'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs">查看</a>
    <a name="audit" data-url="<?php echo url('audit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-warm">审核</a>
    <a href="<?php echo url('del'); ?>?id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-danger j-tr-del">删除</a>
</script>
<script type="text/html" title="金额模版" id="moneyTpl">
    ¥{{#
    var finish_amount = d.sales_order.sales_order_amount*1;
    return '￥'+finish_amount.toFixed(2);
    }}
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script type="text/javascript">
    layui.use(['table','jquery'], function() {
        var table = layui.table,
            layer = layui.layer;
        var sales_order_state = <?php echo json_encode(config('erp.SALES_ORDER_STATE')); ?>;
        var receivable_code = <?php echo json_encode(config('erp.RECEIVABLE_CODE')); ?>;
        table.render({
            elem: '#dataTable'
            ,url: "<?php echo url('sendOrder'); ?>" //数据接口
            ,page: true //开启分页
            ,skin: 'row'
            ,even: true
            ,limit: 20
            ,text: {
                none : '暂无相关数据'
            }
            ,cols: [[ //表头
//                {type:'checkbox'}
                {field: 'send_order_sn',width:150, title: '发货单号'}
                ,{field: 'customer_id', title: '客户',templet:function (res) {
                    return res.sales_order.customer.customer_name
                }}
                ,{field: 'customer_contacts', title: '联系人',templet:function(res){
                    return res.sales_order.customer_contacts;
                }}
                ,{field: 'customer_phone', title: '联系电话',templet:function(res){
                    return res.sales_order.customer_phone;
                }}
                ,{field: 'receivable_code', title: '收款方式',templet:function(res){
                    return receivable_code[res.sales_order.receivable_code];
                }}
                ,{title: '收款金额', templet: '#moneyTpl'}
                ,{field: 'sales_order_state', title: '状态',templet:function(res){
                    return sales_order_state[res.sales_order.sales_order_state];
                }}
//                ,{field: 'return_state',width: 100, title: '有无退货'}
//                ,{title: '操作',  templet: '#buttonTpl'}
                ,{field: 'p_order_id',width: 155,title: '操作',  templet: function (res) {
                    var html = '<a href="<?php echo url('sendDetail'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs">查看</a>';
                    if(res.sales_order.sales_order_state == 0){
                        //确认、删除
                    }else if(res.sales_order.sales_order_state == 1){
                        //查看、采购退货、发货出库
                    }else if(res.sales_order.sales_order_state == 6){ //发货出库
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
                        html += '<a data-url="<?php echo url('confirm'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs  layui-btn-danger confirm-tips" data-msg="客户确认已收货？" >确认收货</a>';
                    }else if(res.sales_order.sales_order_state == 12){ //确认收货
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
//                        html += '<a href="<?php echo url('insertWarehouse'); ?>?p_order_id='+res.p_order_id+'" class="layui-btn layui-btn-xs layui-btn-normal confirm-tips">验货入库</a>';
                    }
                    return html;
                }}
            ]]
        });
        $(document).on("click", '.confirm-tips', function () {
            var url = $(this).attr('data-url');
            var title = $(this).attr('data-msg');
            layer.open({
                type: 1
                ,title: '提示'
                ,area: ['300px', '200px']
                ,shade: 0
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: '<div style="padding: 10px 10px;">'+title+'</div>'
                ,btn: ['确定', '取消'] //只是为了演示
                ,yes: function(){
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: { },
                        dataType: 'json',
                        asyn:false,
                        beforeSend: function () {},
                        success: function (result) {
                            if(result['code'] == 1){
                                layer.msg(result.msg);
                                setTimeout(window.location.reload(),1000);

                            }else{
                                layer.msg(result.msg, {shift: 6});
                            }
                        },
                        error: function (error) {
                            layer.msg('系统错误：' + error.status, {shift: 6});
                        }
                    });
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });
    });

</script>
                </div>
            </div>
        </div>
    <?php break; case "3": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;
                            $hisiTabData['current'] = isset($hisiTabData['current']) ? $hisiTabData['current'] : '';
                         if(($vo['url'] == $hisiCurMenu['url'] or (url($vo['url']) == $hisiTabData['current']))): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; if((strpos($vo['url'], 'http'))): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" target="_blank"><?php echo $vo['title']; ?></a>
                            <?php elseif((strpos($vo['url'], config('sys.admin_path')) !== false)): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php else: ?>
                                <a href="<?php echo url($vo['url']); ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        <div class="page-toolbar">
    <div class="page-filter fr">
        <form class="layui-form layui-form-pane" action="<?php echo url('orders'); ?>" method="get" id="hisi-table-search">
            <div class="layui-form-item">
                <label class="layui-form-label">搜索</label>
                <div class="layui-input-inline">
                    <input type="text" name="keyword" lay-verify="required" placeholder="输入关键词搜索" class="layui-input">
                </div>
            </div>
        </form>
    </div>
    <div class="layui-btn-group fl">
        <a href="<?php echo url('add'); ?>" class="layui-btn layui-btn-primary layui-icon layui-icon-add-circle-fine">&nbsp;添加</a>
    </div>
</div>
<table id="dataTable"></table>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>

<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">修改</a>
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">验货入库</a>
    <a href="<?php echo url('detail'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs">查看</a>
    <a name="audit" data-url="<?php echo url('audit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-warm">审核</a>
    <a href="<?php echo url('del'); ?>?id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-danger j-tr-del">删除</a>
</script>
<script type="text/html" title="金额模版" id="moneyTpl">
    ¥{{#
    var finish_amount = d.sales_order.sales_order_amount*1;
    return '￥'+finish_amount.toFixed(2);
    }}
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script type="text/javascript">
    layui.use(['table','jquery'], function() {
        var table = layui.table,
            layer = layui.layer;
        var sales_order_state = <?php echo json_encode(config('erp.SALES_ORDER_STATE')); ?>;
        var receivable_code = <?php echo json_encode(config('erp.RECEIVABLE_CODE')); ?>;
        table.render({
            elem: '#dataTable'
            ,url: "<?php echo url('sendOrder'); ?>" //数据接口
            ,page: true //开启分页
            ,skin: 'row'
            ,even: true
            ,limit: 20
            ,text: {
                none : '暂无相关数据'
            }
            ,cols: [[ //表头
//                {type:'checkbox'}
                {field: 'send_order_sn',width:150, title: '发货单号'}
                ,{field: 'customer_id', title: '客户',templet:function (res) {
                    return res.sales_order.customer.customer_name
                }}
                ,{field: 'customer_contacts', title: '联系人',templet:function(res){
                    return res.sales_order.customer_contacts;
                }}
                ,{field: 'customer_phone', title: '联系电话',templet:function(res){
                    return res.sales_order.customer_phone;
                }}
                ,{field: 'receivable_code', title: '收款方式',templet:function(res){
                    return receivable_code[res.sales_order.receivable_code];
                }}
                ,{title: '收款金额', templet: '#moneyTpl'}
                ,{field: 'sales_order_state', title: '状态',templet:function(res){
                    return sales_order_state[res.sales_order.sales_order_state];
                }}
//                ,{field: 'return_state',width: 100, title: '有无退货'}
//                ,{title: '操作',  templet: '#buttonTpl'}
                ,{field: 'p_order_id',width: 155,title: '操作',  templet: function (res) {
                    var html = '<a href="<?php echo url('sendDetail'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs">查看</a>';
                    if(res.sales_order.sales_order_state == 0){
                        //确认、删除
                    }else if(res.sales_order.sales_order_state == 1){
                        //查看、采购退货、发货出库
                    }else if(res.sales_order.sales_order_state == 6){ //发货出库
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
                        html += '<a data-url="<?php echo url('confirm'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs  layui-btn-danger confirm-tips" data-msg="客户确认已收货？" >确认收货</a>';
                    }else if(res.sales_order.sales_order_state == 12){ //确认收货
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
//                        html += '<a href="<?php echo url('insertWarehouse'); ?>?p_order_id='+res.p_order_id+'" class="layui-btn layui-btn-xs layui-btn-normal confirm-tips">验货入库</a>';
                    }
                    return html;
                }}
            ]]
        });
        $(document).on("click", '.confirm-tips', function () {
            var url = $(this).attr('data-url');
            var title = $(this).attr('data-msg');
            layer.open({
                type: 1
                ,title: '提示'
                ,area: ['300px', '200px']
                ,shade: 0
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: '<div style="padding: 10px 10px;">'+title+'</div>'
                ,btn: ['确定', '取消'] //只是为了演示
                ,yes: function(){
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: { },
                        dataType: 'json',
                        asyn:false,
                        beforeSend: function () {},
                        success: function (result) {
                            if(result['code'] == 1){
                                layer.msg(result.msg);
                                setTimeout(window.location.reload(),1000);

                            }else{
                                layer.msg(result.msg, {shift: 6});
                            }
                        },
                        error: function (error) {
                            layer.msg('系统错误：' + error.status, {shift: 6});
                        }
                    });
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });
    });

</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; default: ?>
        
        <div class="page-tab-content">
            <div class="page-toolbar">
    <div class="page-filter fr">
        <form class="layui-form layui-form-pane" action="<?php echo url('orders'); ?>" method="get" id="hisi-table-search">
            <div class="layui-form-item">
                <label class="layui-form-label">搜索</label>
                <div class="layui-input-inline">
                    <input type="text" name="keyword" lay-verify="required" placeholder="输入关键词搜索" class="layui-input">
                </div>
            </div>
        </form>
    </div>
    <div class="layui-btn-group fl">
        <a href="<?php echo url('add'); ?>" class="layui-btn layui-btn-primary layui-icon layui-icon-add-circle-fine">&nbsp;添加</a>
    </div>
</div>
<table id="dataTable"></table>
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>

<script type="text/html" title="操作按钮模板" id="buttonTpl">
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">修改</a>
    <a href="<?php echo url('edit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-normal">验货入库</a>
    <a href="<?php echo url('detail'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs">查看</a>
    <a name="audit" data-url="<?php echo url('audit'); ?>?p_order_id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-warm">审核</a>
    <a href="<?php echo url('del'); ?>?id={{ d.p_order_id }}" class="layui-btn layui-btn-xs layui-btn-danger j-tr-del">删除</a>
</script>
<script type="text/html" title="金额模版" id="moneyTpl">
    ¥{{#
    var finish_amount = d.sales_order.sales_order_amount*1;
    return '￥'+finish_amount.toFixed(2);
    }}
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script type="text/javascript">
    layui.use(['table','jquery'], function() {
        var table = layui.table,
            layer = layui.layer;
        var sales_order_state = <?php echo json_encode(config('erp.SALES_ORDER_STATE')); ?>;
        var receivable_code = <?php echo json_encode(config('erp.RECEIVABLE_CODE')); ?>;
        table.render({
            elem: '#dataTable'
            ,url: "<?php echo url('sendOrder'); ?>" //数据接口
            ,page: true //开启分页
            ,skin: 'row'
            ,even: true
            ,limit: 20
            ,text: {
                none : '暂无相关数据'
            }
            ,cols: [[ //表头
//                {type:'checkbox'}
                {field: 'send_order_sn',width:150, title: '发货单号'}
                ,{field: 'customer_id', title: '客户',templet:function (res) {
                    return res.sales_order.customer.customer_name
                }}
                ,{field: 'customer_contacts', title: '联系人',templet:function(res){
                    return res.sales_order.customer_contacts;
                }}
                ,{field: 'customer_phone', title: '联系电话',templet:function(res){
                    return res.sales_order.customer_phone;
                }}
                ,{field: 'receivable_code', title: '收款方式',templet:function(res){
                    return receivable_code[res.sales_order.receivable_code];
                }}
                ,{title: '收款金额', templet: '#moneyTpl'}
                ,{field: 'sales_order_state', title: '状态',templet:function(res){
                    return sales_order_state[res.sales_order.sales_order_state];
                }}
//                ,{field: 'return_state',width: 100, title: '有无退货'}
//                ,{title: '操作',  templet: '#buttonTpl'}
                ,{field: 'p_order_id',width: 155,title: '操作',  templet: function (res) {
                    var html = '<a href="<?php echo url('sendDetail'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs">查看</a>';
                    if(res.sales_order.sales_order_state == 0){
                        //确认、删除
                    }else if(res.sales_order.sales_order_state == 1){
                        //查看、采购退货、发货出库
                    }else if(res.sales_order.sales_order_state == 6){ //发货出库
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
                        html += '<a data-url="<?php echo url('confirm'); ?>?sales_order_id='+res.sales_order_id+'" class="layui-btn layui-btn-xs  layui-btn-danger confirm-tips" data-msg="客户确认已收货？" >确认收货</a>';
                    }else if(res.sales_order.sales_order_state == 12){ //确认收货
                        //查看、入库、取消（验货完成等待入库）   or    查看（验货完成直接入库）
//                        html += '<a href="<?php echo url('insertWarehouse'); ?>?p_order_id='+res.p_order_id+'" class="layui-btn layui-btn-xs layui-btn-normal confirm-tips">验货入库</a>';
                    }
                    return html;
                }}
            ]]
        });
        $(document).on("click", '.confirm-tips', function () {
            var url = $(this).attr('data-url');
            var title = $(this).attr('data-msg');
            layer.open({
                type: 1
                ,title: '提示'
                ,area: ['300px', '200px']
                ,shade: 0
                ,offset: [ //为了演示，随机坐标
                    '100px'
                ]
                ,content: '<div style="padding: 10px 10px;">'+title+'</div>'
                ,btn: ['确定', '取消'] //只是为了演示
                ,yes: function(){
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: { },
                        dataType: 'json',
                        asyn:false,
                        beforeSend: function () {},
                        success: function (result) {
                            if(result['code'] == 1){
                                layer.msg(result.msg);
                                setTimeout(window.location.reload(),1000);

                            }else{
                                layer.msg(result.msg, {shift: 6});
                            }
                        },
                        error: function (error) {
                            layer.msg('系统错误：' + error.status, {shift: 6});
                        }
                    });
                }
                ,btn2: function(){
                    layer.closeAll();
                }

                ,success: function(layero){
                    layer.setTop(layero); //重点2
                    console.log('success',layero);
                }
            });
        });
    });

</script>
        </div>
<?php endswitch; if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
</body>
</html>
<?php else: ?>
        </div>
    </div>
    <!--<div class="layui-footer footer">
        <span class="fl">Powered by <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.name'); ?></a> v<?php echo config('hisiphp.version'); ?></span>
        <span class="fr"> © 2018-2020 <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.copyright'); ?></a> All Rights Reserved.</span>
    </div>-->
</div>
</body>
</html>
<?php endif; ?>