<?php /*a:6:{s:65:"D:\wwwroot\hisiphp\application\site\view\history_data\echart.html";i:1590637513;s:54:"D:\wwwroot\hisiphp\application\system\view\layout.html";i:1590637517;s:60:"D:\wwwroot\hisiphp\application\system\view\block\header.html";i:1590637519;s:58:"D:\wwwroot\hisiphp\application\system\view\block\menu.html";i:1590637518;s:57:"D:\wwwroot\hisiphp\application\site\view\block\layui.html";i:1590637510;s:60:"D:\wwwroot\hisiphp\application\system\view\block\footer.html";i:1590637519;}*/ ?>
<?php if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo htmlentities($hisiCurMenu['title']); ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="hisi-theme-<?php echo cookie('hisi_admin_theme'); ?> pb50">
<?php else: ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if($hisiCurMenu['url'] == 'system/index/index'): ?>管理控制台<?php else: ?><?php echo htmlentities($hisiCurMenu['title']); ?><?php endif; ?> -  Powered by <?php echo config('hisiphp.name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="/static/js/layui/css/layui.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/theme.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/system/css/style.css?v=<?php echo config('hisiphp.version'); ?>" media="all">
    <link rel="stylesheet" href="/static/fonts/typicons/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <link rel="stylesheet" href="/static/fonts/font-awesome/min.css?v=<?php echo config('hisiphp.version'); ?>">
    <?php echo $hisiHead; ?>
</head>
<body class="layui-layout-body hisi-theme-<?php echo cookie('hisi_admin_theme'); ?>">
<?php 
$ca = strtolower(request()->controller().'/'.request()->action());
 ?>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header" style="z-index:999!important;">
    <div class="fl header-logo"><img style="height: 66px" src="<?php echo config('base.site_logo'); ?>"><!--后台管理中心--></div>
    <div class="fl header-fold"><a href="javascript:;" title="打开/关闭左侧导航" class="aicon ai-shouqicaidan" id="foldSwitch"></a></div>
    <ul class="layui-nav fl nobg main-nav">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $vo['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $vo['id'] == 3)): ?>
           <li class="layui-nav-item layui-this">
            <?php else: ?>
            <li class="layui-nav-item">
            <?php endif; ?> 
            <a href="javascript:;"><?php echo htmlentities($vo['title']); ?></a></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
    <ul class="layui-nav fr nobg head-info">
        <!--<li class="layui-nav-item">
            <a href="/" target="_blank" class="aicon ai-ai-home" title="前台"></a>
        </li>
        <li class="layui-nav-item">
            <a href="javascript:void(0);" class="aicon ai-qingchu" id="hisi-clear-cache" title="清缓存"></a>
        </li>
        <li class="layui-nav-item">
        <a href="javascript:void(0);" class="aicon ai-suo" id="lockScreen" title="锁屏"></a>
        </li>
        <li class="layui-nav-item">
            <a href="<?php echo url('system/user/setTheme'); ?>" id="hisi-theme-setting" class="aicon ai-theme"></a>
        </li>
        <li class="layui-nav-item hisi-lang">
            <a href="javascript:void(0);"><i class="layui-icon layui-icon-website"></i></a>
            <dl class="layui-nav-child">
                <?php if(is_array($languages) || $languages instanceof \think\Collection || $languages instanceof \think\Paginator): $i = 0; $__LIST__ = $languages;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if($vo['pack']): ?>
                <dd><a href="<?php echo url('system/index/index'); ?>?lang=<?php echo htmlentities($vo['code']); ?>"><?php echo htmlentities($vo['name']); ?></a></dd>
                <?php endif; ?>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                <dd>
                    <a data-id="000" class="admin-nav-item top-nav-item" href="<?php echo url('system/language/index'); ?>">语言包管理</a>
                </dd>
            </dl>
        </li>-->
        <li class="layui-nav-item">
            <a href="javascript:void(0);"><?php echo htmlentities($login['nick']); ?>&nbsp;&nbsp;</a>
            <dl class="layui-nav-child">
                <dd>
                    <a data-id="00" class="admin-nav-item top-nav-item" href="<?php echo url('system/user/info'); ?>">个人设置</a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/user/iframe'); ?>" class="hisi-ajax" refresh="true"><?php echo input('cookie.hisi_iframe') ? '单页布局' : '框架布局'; ?></a>
                </dd>
                <dd>
                    <a href="<?php echo url('system/publics/logout'); ?>">退出登陆</a>
                </dd>
            </dl>
        </li>
    </ul>
</div>
<div class="layui-side layui-bg-black" id="switchNav">
    <div class="layui-side-scroll">
        <?php if(is_array($hisiMenus) || $hisiMenus instanceof \think\Collection || $hisiMenus instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiMenus;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if(($hisiCurParents['pid'] == $v['id'] and $ca != 'plugins/run') or ($ca == 'plugins/run' and $v['id'] == 3)): ?>
        <ul class="layui-nav layui-nav-tree">
        <?php else: ?>
        <ul class="layui-nav layui-nav-tree" style="display:none;">
        <?php endif; if((isset($v['childs']))): if(is_array($v['childs']) || $v['childs'] instanceof \think\Collection || $v['childs'] instanceof \think\Paginator): $kk = 0; $__LIST__ = $v['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($kk % 2 );++$kk;?>
            <li class="layui-nav-item <?php if($kk == 1): ?>layui-nav-itemed<?php endif; ?>">
                <a href="javascript:;"><i class="<?php echo htmlentities($vv['icon']); ?>"></i><?php echo htmlentities($vv['title']); ?><span class="layui-nav-more"></span></a>
                <dl class="layui-nav-child">
                    <?php if($vv['title'] == '快捷菜单'): ?>
                        <dd>
                            <a class="admin-nav-item" data-id="0" href="<?php echo input('cookie.hisi_iframe') ? url('system/index/welcome') : url('system/index/index'); ?>"><i class="aicon ai-shouye"></i> 后台首页</a>
                        </dd>
                        <?php if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a><i data-href="<?php echo url('system/menu/del?id='.$vvv['id']); ?>" class="layui-icon j-del-menu">&#xe640;</i>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; else: if((isset($vv['childs']))): if(is_array($vv['childs']) || $vv['childs'] instanceof \think\Collection || $vv['childs'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['childs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
                        <dd>
                            <a class="admin-nav-item" data-id="<?php echo htmlentities($vvv['id']); ?>" href="<?php if(strpos('http', $vvv['url']) === false): ?><?php echo url($vvv['url'], $vvv['param']); else: ?><?php echo htmlentities($vvv['url']); ?><?php endif; ?>"><?php if(file_exists('.'.$vvv['icon'])): ?><img src="<?php echo htmlentities($vvv['icon']); ?>" width="16" height="16" /><?php else: ?><i class="<?php echo htmlentities($vvv['icon']); ?>"></i><?php endif; ?> <?php echo htmlentities($vvv['title']); ?></a>
                        </dd>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </dl>
            </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        <?php endif; ?>
        </ul>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<script type="text/html" id="hisi-theme-tpl">
    <ul class="hisi-themes">
        <?php $_result=session('hisi_admin_themes');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <li data-theme="<?php echo htmlentities($vo); ?>" class="hisi-theme-item-<?php echo htmlentities($vo); ?>"></li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
</script>
<script type="text/html" id="hisi-clear-cache-tpl">
    <form class="layui-form" style="padding:10px 0 0 30px;" action="<?php echo url('system/index/clear'); ?>" method="post">
        <div class="layui-form-item">
            <input type="checkbox" name="cache" value="1" title="数据缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="log" value="1" title="日志缓存" />
        </div>
        <div class="layui-form-item">
            <input type="checkbox" name="temp" value="1" title="模板缓存" />
        </div>
        <div class="layui-form-item">
            <button class="layui-btn layui-btn-normal" lay-submit="" lay-filter="formSubmit">执行删除</button>
        </div>
    </form>
</script>
    <div class="layui-body" id="switchBody">
        <ul class="bread-crumbs">
            <?php if(is_array($hisiBreadcrumb) || $hisiBreadcrumb instanceof \think\Collection || $hisiBreadcrumb instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiBreadcrumb;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if($key > 0 && $i != count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="<?php echo url($v['url'].'?'.$v['param']); ?>"><?php echo htmlentities($v['title']); ?></a></li>
                <?php elseif($i == count($hisiBreadcrumb)): ?>
                    <li>></li>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php else: ?>
                    <li><a href="javascript:void(0);"><?php echo htmlentities($v['title']); ?></a></li>
                <?php endif; ?>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <li><a href="<?php echo url('system/menu/quick?id='.$hisiCurMenu['id']); ?>" title="添加到首页快捷菜单" class="j-ajax">[+]</a></li>
        </ul>
        <div style="padding:0 10px;" class="mcolor"><?php echo runhook('system_admin_tips'); ?></div>
            <div class="page-body">
<?php endif; switch($hisiTabType): case "1": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <li class="layui-this">
                        <a href="javascript:;" id="curTitle"><?php echo $hisiCurMenu['title']; ?></a>
                    </li>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        <link rel="stylesheet" href="/static/layui-formSelects-master/dist/formSelects-v4.css">
<form class="layui-form layui-form-pane" action="<?php echo url(); ?>" method="get">
    <div class="">
        <div class="page-tab-content">


            <div class="layui-form-item">
                <label class="layui-form-label">时间范围</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="start_time" id="start_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['start_time']); ?>">
                </div>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="end_time" id="end_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['end_time']); ?>">
                </div>
                <label class="layui-form-label">监控站点</label>
                <div class="layui-input-inline">
                    <select name="monitor_id" class="field-monitor_id" type="select" lay-verify="">
                        <?php foreach($device_monitors as $key=>$value): ?>
                        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">区名</label>
                    <div class="layui-input-inline">
                        <select name="partition_type" class="partition_type i_change o_change" lay-search id="partition_type" xm-select="partition_type"
                                data-url="<?php echo url('site/DeviceMonitor/select_partition_type'); ?>" data-key="partition_type" data-value="partition_type"
                                data-del = "gateway_name,dev_name,var_name" data-need = ""
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">组名</label>
                    <div class="layui-input-inline">
                        <select name="gateway_name" class="i_change o_change" lay-search id="gateway_name" xm-select="gateway_name"
                                lay-filter="gateway" data-url="<?php echo url('site/DeviceMonitor/gateway_select'); ?>"  data-key="gateway_name" data-value="gateway_name"
                                data-del = "dev_name,var_name" data-need = "partition_type"
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">设备名</label>
                    <div class="layui-input-inline">
                        <select name="dev_name" class="i_change o_change" lay-search lay-filter="ins" xm-select="dev_name" data-key="dev_name" data-value="dev_name"
                                id="dev_name" data-url="<?php echo url('site/DeviceMonitor/instrument_select'); ?>"    data-del = "var_name" data-need = "partition_type,gateway_name">
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">变量名</label>
                    <div class="layui-input-inline w300">
                        <select lay-verify="" name="var_name" class="i_change" name="var_name" id="var_name" xm-select="var_name"  xm-select-radio  xm-select-search
                                data-url="<?php echo url('site/DeviceMonitor/varlist_select'); ?>" data-key="var_name" data-value="var_alias" data-need = "partition_type,gateway_name,dev_name">
                            <option value="">全选</option>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <button id="sub_btn" type="button" class="layui-btn layui-btn-normal" lay-submit="" lay-filter="*">搜索</button>
                </div>

            </div>

        </div>
    </div>
    <hr>
</form>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div class="layui-container">
    <div class="layui-row">
        <div class="layui-col-md12" id="main" style="height: 400px">

        </div>
    </div>
</div>
<!--<div id="main" style="width: 600px;height:400px;"></div>-->
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script src="/static/plugins/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 引入 ECharts 文件 -->
<script src="/static/plugins/echarts/echarts.min.js"></script>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    option = {};
    myChart.setOption(option);

</script>
<script type="text/javascript">
    layui.config({
        base : '/static/layui-formSelects-master/dist/'
    }).extend({
        formSelects: 'formSelects-v4'
    }).use(['formSelects','laydate','form','util'], function() {
        var $ = layui.jquery
            ,laydate = layui.laydate
            ,util = layui.util
            ,form = layui.form
            ,formSelects = layui.formSelects;

        //日期时间选择器
        laydate.render({
            elem: '#start_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        laydate.render({
            elem: '#end_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        //监听提交
        form.on('submit(*)', function(data){
            /*var var_name = formSelects.value('var_name', 'valStr');
            if (var_name == '') {

                parent.layer.msg('请选择具体变量!', {shift: 6});
                return false;
            }*/
            var serializeArray = $("form").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>",
                data: data,
                dataType: "json",
                success: function(data){
                    if(data['code'] == 0){
                        if(data['count'] == 0){
                            layer.msg('暂无数据');
                            option = {};
                            myChart.setOption(option);
                            return false;
                        }
                        var x_data = data['x_data']?data['x_data']:[];
                        var y_data = data['y_data'];
                        var legend = data['legend'];
                        option = {
                            title: {
                                text: '统计图'
                            },
                            tooltip: {
                                trigger: 'axis'
                            },
                            legend: {
//                                data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                                data: legend
                            },
                            grid: {
                                left: '3%',
                                right: '4%',
                                bottom: '3%',
                                containLabel: true
                            },
                            toolbox: {
                                feature: {
                                    saveAsImage: {}
                                }
                            },
                            xAxis: {
                                type: 'category',
                                boundaryGap: false,
//                                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                                data:x_data
                            },
                            yAxis: {
                                type: 'value'
                            },
                            series: y_data/*[
                                {
                                    name: '邮件营销',
                                    type: 'line',
                                    stack: '总量',
                                    data: [120, 132, 101, 134, 90, 230, 210]
                                },
                                {
                                    name: '联盟广告',
                                    type: 'line',
                                    stack: '总量',
                                    data: [220, 182, 191, 234, 290, 330, 310]
                                },
                            ]*/
                        };


                        // 使用刚指定的配置项和数据显示图表。
                        myChart.setOption(option);
                    }
                }
            });
        });

        $(".i_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');
            var key = $this.attr('data-key');
            var value = $this.attr('data-value');
            var url = $this.attr('data-url');
            select_I(demoId,key,value,url,{});
        })


        $(".o_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');

            o_change(demoId);
        })


        function o_change(demoId) {

            //分组变动  设备变动
            formSelects.on(demoId, function(id, vals, val, isAdd, isDisabled){


                var selected = [];
                $.each(vals,function (index,value) {
                    selected.push(value['value'])

                })
                var this_value = val.value;

                if(isAdd){
                    if(selected.indexOf(this_value)  == -1){
                        selected.push(this_value)
                    }

                }else{
                    var c = selected.indexOf(this_value)
                    if( c != -1){
                        selected.splice(c,1);
                    }
                }

                var s = selected.join(",");



                var $this = $("#"+demoId);

                var datadel = $this.attr('data-del');
                var datadel =datadel.split(',');




                $.each(datadel,function (index,value) {

                    var $this = $("#"+value);
                    var demoId_key = $this.attr('id');
                    var key = $this.attr('data-key');
                    var value_key = $this.attr('data-value');
                    var url = $this.attr('data-url');

                    var data_need = $this.attr('data-need');
                    data_need = data_need.split(',');

                    var data = {};
                    $.each(data_need,function (indexs,values) {

                        if(values == demoId){

                            if(s != ''){
                                data[values] = ['in',s];
                            }

                        }
                        else{
                            var v = formSelects.value(values, 'valStr');
                            if(v != ''){
                                data[values] = ['in',formSelects.value(values, 'valStr')];
                            }

                        }


                    });



                    select_I(demoId_key,key,value_key,url,data);

                })
            });

        }

        function select_I(demo,key,value,url,data) {
            formSelects.data(demo, 'local', {
                arr: []
            });
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    // layLoad = parent.layer.load(1, {
                    //     shade: [0.5, '#000']
                    // });

                },
                success: function (result) {

                    //parent.layer.close(layLoad);

                    if (result.status != 1) {
                        // parent.layer.msg(result.message, {shift: 6});
                        return false;
                    }
                    var dataarr = result.result;

                    //  var str = '<option value="">全选</option>';
                    var arrdata = [];
                    $.each(dataarr, function (ind, val) {
                        // str += '<option value="' + val.gateway_name + '">' + val.gateway_name + '</option>';
                        // $('#gateway_name').html(str);

                        var list = {};

                        list['name'] = val[value];
                        list['value'] = val[key];
                        arrdata.push(list);


                    })

                    console.log(arrdata);
                    formSelects.data(demo, 'local', {
                        arr: arrdata
                    });
                    //  formSelects.render();
                },
                error: function (error) {
                    // parent.layer.close(layLoad);
                    layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
        }
    });
</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; case "2": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $k = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;if(($k == 1)): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; ?>
                            <a href="javascript:;" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <link rel="stylesheet" href="/static/layui-formSelects-master/dist/formSelects-v4.css">
<form class="layui-form layui-form-pane" action="<?php echo url(); ?>" method="get">
    <div class="">
        <div class="page-tab-content">


            <div class="layui-form-item">
                <label class="layui-form-label">时间范围</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="start_time" id="start_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['start_time']); ?>">
                </div>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="end_time" id="end_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['end_time']); ?>">
                </div>
                <label class="layui-form-label">监控站点</label>
                <div class="layui-input-inline">
                    <select name="monitor_id" class="field-monitor_id" type="select" lay-verify="">
                        <?php foreach($device_monitors as $key=>$value): ?>
                        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">区名</label>
                    <div class="layui-input-inline">
                        <select name="partition_type" class="partition_type i_change o_change" lay-search id="partition_type" xm-select="partition_type"
                                data-url="<?php echo url('site/DeviceMonitor/select_partition_type'); ?>" data-key="partition_type" data-value="partition_type"
                                data-del = "gateway_name,dev_name,var_name" data-need = ""
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">组名</label>
                    <div class="layui-input-inline">
                        <select name="gateway_name" class="i_change o_change" lay-search id="gateway_name" xm-select="gateway_name"
                                lay-filter="gateway" data-url="<?php echo url('site/DeviceMonitor/gateway_select'); ?>"  data-key="gateway_name" data-value="gateway_name"
                                data-del = "dev_name,var_name" data-need = "partition_type"
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">设备名</label>
                    <div class="layui-input-inline">
                        <select name="dev_name" class="i_change o_change" lay-search lay-filter="ins" xm-select="dev_name" data-key="dev_name" data-value="dev_name"
                                id="dev_name" data-url="<?php echo url('site/DeviceMonitor/instrument_select'); ?>"    data-del = "var_name" data-need = "partition_type,gateway_name">
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">变量名</label>
                    <div class="layui-input-inline w300">
                        <select lay-verify="" name="var_name" class="i_change" name="var_name" id="var_name" xm-select="var_name"  xm-select-radio  xm-select-search
                                data-url="<?php echo url('site/DeviceMonitor/varlist_select'); ?>" data-key="var_name" data-value="var_alias" data-need = "partition_type,gateway_name,dev_name">
                            <option value="">全选</option>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <button id="sub_btn" type="button" class="layui-btn layui-btn-normal" lay-submit="" lay-filter="*">搜索</button>
                </div>

            </div>

        </div>
    </div>
    <hr>
</form>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div class="layui-container">
    <div class="layui-row">
        <div class="layui-col-md12" id="main" style="height: 400px">

        </div>
    </div>
</div>
<!--<div id="main" style="width: 600px;height:400px;"></div>-->
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script src="/static/plugins/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 引入 ECharts 文件 -->
<script src="/static/plugins/echarts/echarts.min.js"></script>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    option = {};
    myChart.setOption(option);

</script>
<script type="text/javascript">
    layui.config({
        base : '/static/layui-formSelects-master/dist/'
    }).extend({
        formSelects: 'formSelects-v4'
    }).use(['formSelects','laydate','form','util'], function() {
        var $ = layui.jquery
            ,laydate = layui.laydate
            ,util = layui.util
            ,form = layui.form
            ,formSelects = layui.formSelects;

        //日期时间选择器
        laydate.render({
            elem: '#start_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        laydate.render({
            elem: '#end_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        //监听提交
        form.on('submit(*)', function(data){
            /*var var_name = formSelects.value('var_name', 'valStr');
            if (var_name == '') {

                parent.layer.msg('请选择具体变量!', {shift: 6});
                return false;
            }*/
            var serializeArray = $("form").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>",
                data: data,
                dataType: "json",
                success: function(data){
                    if(data['code'] == 0){
                        if(data['count'] == 0){
                            layer.msg('暂无数据');
                            option = {};
                            myChart.setOption(option);
                            return false;
                        }
                        var x_data = data['x_data']?data['x_data']:[];
                        var y_data = data['y_data'];
                        var legend = data['legend'];
                        option = {
                            title: {
                                text: '统计图'
                            },
                            tooltip: {
                                trigger: 'axis'
                            },
                            legend: {
//                                data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                                data: legend
                            },
                            grid: {
                                left: '3%',
                                right: '4%',
                                bottom: '3%',
                                containLabel: true
                            },
                            toolbox: {
                                feature: {
                                    saveAsImage: {}
                                }
                            },
                            xAxis: {
                                type: 'category',
                                boundaryGap: false,
//                                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                                data:x_data
                            },
                            yAxis: {
                                type: 'value'
                            },
                            series: y_data/*[
                                {
                                    name: '邮件营销',
                                    type: 'line',
                                    stack: '总量',
                                    data: [120, 132, 101, 134, 90, 230, 210]
                                },
                                {
                                    name: '联盟广告',
                                    type: 'line',
                                    stack: '总量',
                                    data: [220, 182, 191, 234, 290, 330, 310]
                                },
                            ]*/
                        };


                        // 使用刚指定的配置项和数据显示图表。
                        myChart.setOption(option);
                    }
                }
            });
        });

        $(".i_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');
            var key = $this.attr('data-key');
            var value = $this.attr('data-value');
            var url = $this.attr('data-url');
            select_I(demoId,key,value,url,{});
        })


        $(".o_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');

            o_change(demoId);
        })


        function o_change(demoId) {

            //分组变动  设备变动
            formSelects.on(demoId, function(id, vals, val, isAdd, isDisabled){


                var selected = [];
                $.each(vals,function (index,value) {
                    selected.push(value['value'])

                })
                var this_value = val.value;

                if(isAdd){
                    if(selected.indexOf(this_value)  == -1){
                        selected.push(this_value)
                    }

                }else{
                    var c = selected.indexOf(this_value)
                    if( c != -1){
                        selected.splice(c,1);
                    }
                }

                var s = selected.join(",");



                var $this = $("#"+demoId);

                var datadel = $this.attr('data-del');
                var datadel =datadel.split(',');




                $.each(datadel,function (index,value) {

                    var $this = $("#"+value);
                    var demoId_key = $this.attr('id');
                    var key = $this.attr('data-key');
                    var value_key = $this.attr('data-value');
                    var url = $this.attr('data-url');

                    var data_need = $this.attr('data-need');
                    data_need = data_need.split(',');

                    var data = {};
                    $.each(data_need,function (indexs,values) {

                        if(values == demoId){

                            if(s != ''){
                                data[values] = ['in',s];
                            }

                        }
                        else{
                            var v = formSelects.value(values, 'valStr');
                            if(v != ''){
                                data[values] = ['in',formSelects.value(values, 'valStr')];
                            }

                        }


                    });



                    select_I(demoId_key,key,value_key,url,data);

                })
            });

        }

        function select_I(demo,key,value,url,data) {
            formSelects.data(demo, 'local', {
                arr: []
            });
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    // layLoad = parent.layer.load(1, {
                    //     shade: [0.5, '#000']
                    // });

                },
                success: function (result) {

                    //parent.layer.close(layLoad);

                    if (result.status != 1) {
                        // parent.layer.msg(result.message, {shift: 6});
                        return false;
                    }
                    var dataarr = result.result;

                    //  var str = '<option value="">全选</option>';
                    var arrdata = [];
                    $.each(dataarr, function (ind, val) {
                        // str += '<option value="' + val.gateway_name + '">' + val.gateway_name + '</option>';
                        // $('#gateway_name').html(str);

                        var list = {};

                        list['name'] = val[value];
                        list['value'] = val[key];
                        arrdata.push(list);


                    })

                    console.log(arrdata);
                    formSelects.data(demo, 'local', {
                        arr: arrdata
                    });
                    //  formSelects.render();
                },
                error: function (error) {
                    // parent.layer.close(layLoad);
                    layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
        }
    });
</script>
                </div>
            </div>
        </div>
    <?php break; case "3": ?>
        
        <div class="layui-card">
            <div class="layui-tab layui-tab-brief">
                <ul class="layui-tab-title">
                    <?php if(is_array($hisiTabData['menu']) || $hisiTabData['menu'] instanceof \think\Collection || $hisiTabData['menu'] instanceof \think\Paginator): $i = 0; $__LIST__ = $hisiTabData['menu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;
                            $hisiTabData['current'] = isset($hisiTabData['current']) ? $hisiTabData['current'] : '';
                         if(($vo['url'] == $hisiCurMenu['url'] or (url($vo['url']) == $hisiTabData['current']))): ?>
                            <li class="layui-this">
                        <?php else: ?>
                            <li>
                        <?php endif; if((strpos($vo['url'], 'http'))): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" target="_blank"><?php echo $vo['title']; ?></a>
                            <?php elseif((strpos($vo['url'], config('sys.admin_path')) !== false)): ?>
                                <a href="<?php echo htmlentities($vo['url']); ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php else: ?>
                                <a href="<?php echo url($vo['url']); ?>" class="<?php if((isset($vo['class']))): ?><?php echo htmlentities($vo['class']); ?><?php endif; ?>" id="<?php if((isset($vo['id']))): ?><?php echo htmlentities($vo['id']); ?><?php endif; ?>"><?php echo $vo['title']; ?></a>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div class="layui-tab-content page-tab-content">
                    <div class="layui-tab-item layui-show">
                        <link rel="stylesheet" href="/static/layui-formSelects-master/dist/formSelects-v4.css">
<form class="layui-form layui-form-pane" action="<?php echo url(); ?>" method="get">
    <div class="">
        <div class="page-tab-content">


            <div class="layui-form-item">
                <label class="layui-form-label">时间范围</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="start_time" id="start_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['start_time']); ?>">
                </div>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="end_time" id="end_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['end_time']); ?>">
                </div>
                <label class="layui-form-label">监控站点</label>
                <div class="layui-input-inline">
                    <select name="monitor_id" class="field-monitor_id" type="select" lay-verify="">
                        <?php foreach($device_monitors as $key=>$value): ?>
                        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">区名</label>
                    <div class="layui-input-inline">
                        <select name="partition_type" class="partition_type i_change o_change" lay-search id="partition_type" xm-select="partition_type"
                                data-url="<?php echo url('site/DeviceMonitor/select_partition_type'); ?>" data-key="partition_type" data-value="partition_type"
                                data-del = "gateway_name,dev_name,var_name" data-need = ""
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">组名</label>
                    <div class="layui-input-inline">
                        <select name="gateway_name" class="i_change o_change" lay-search id="gateway_name" xm-select="gateway_name"
                                lay-filter="gateway" data-url="<?php echo url('site/DeviceMonitor/gateway_select'); ?>"  data-key="gateway_name" data-value="gateway_name"
                                data-del = "dev_name,var_name" data-need = "partition_type"
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">设备名</label>
                    <div class="layui-input-inline">
                        <select name="dev_name" class="i_change o_change" lay-search lay-filter="ins" xm-select="dev_name" data-key="dev_name" data-value="dev_name"
                                id="dev_name" data-url="<?php echo url('site/DeviceMonitor/instrument_select'); ?>"    data-del = "var_name" data-need = "partition_type,gateway_name">
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">变量名</label>
                    <div class="layui-input-inline w300">
                        <select lay-verify="" name="var_name" class="i_change" name="var_name" id="var_name" xm-select="var_name"  xm-select-radio  xm-select-search
                                data-url="<?php echo url('site/DeviceMonitor/varlist_select'); ?>" data-key="var_name" data-value="var_alias" data-need = "partition_type,gateway_name,dev_name">
                            <option value="">全选</option>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <button id="sub_btn" type="button" class="layui-btn layui-btn-normal" lay-submit="" lay-filter="*">搜索</button>
                </div>

            </div>

        </div>
    </div>
    <hr>
</form>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div class="layui-container">
    <div class="layui-row">
        <div class="layui-col-md12" id="main" style="height: 400px">

        </div>
    </div>
</div>
<!--<div id="main" style="width: 600px;height:400px;"></div>-->
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script src="/static/plugins/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 引入 ECharts 文件 -->
<script src="/static/plugins/echarts/echarts.min.js"></script>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    option = {};
    myChart.setOption(option);

</script>
<script type="text/javascript">
    layui.config({
        base : '/static/layui-formSelects-master/dist/'
    }).extend({
        formSelects: 'formSelects-v4'
    }).use(['formSelects','laydate','form','util'], function() {
        var $ = layui.jquery
            ,laydate = layui.laydate
            ,util = layui.util
            ,form = layui.form
            ,formSelects = layui.formSelects;

        //日期时间选择器
        laydate.render({
            elem: '#start_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        laydate.render({
            elem: '#end_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        //监听提交
        form.on('submit(*)', function(data){
            /*var var_name = formSelects.value('var_name', 'valStr');
            if (var_name == '') {

                parent.layer.msg('请选择具体变量!', {shift: 6});
                return false;
            }*/
            var serializeArray = $("form").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>",
                data: data,
                dataType: "json",
                success: function(data){
                    if(data['code'] == 0){
                        if(data['count'] == 0){
                            layer.msg('暂无数据');
                            option = {};
                            myChart.setOption(option);
                            return false;
                        }
                        var x_data = data['x_data']?data['x_data']:[];
                        var y_data = data['y_data'];
                        var legend = data['legend'];
                        option = {
                            title: {
                                text: '统计图'
                            },
                            tooltip: {
                                trigger: 'axis'
                            },
                            legend: {
//                                data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                                data: legend
                            },
                            grid: {
                                left: '3%',
                                right: '4%',
                                bottom: '3%',
                                containLabel: true
                            },
                            toolbox: {
                                feature: {
                                    saveAsImage: {}
                                }
                            },
                            xAxis: {
                                type: 'category',
                                boundaryGap: false,
//                                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                                data:x_data
                            },
                            yAxis: {
                                type: 'value'
                            },
                            series: y_data/*[
                                {
                                    name: '邮件营销',
                                    type: 'line',
                                    stack: '总量',
                                    data: [120, 132, 101, 134, 90, 230, 210]
                                },
                                {
                                    name: '联盟广告',
                                    type: 'line',
                                    stack: '总量',
                                    data: [220, 182, 191, 234, 290, 330, 310]
                                },
                            ]*/
                        };


                        // 使用刚指定的配置项和数据显示图表。
                        myChart.setOption(option);
                    }
                }
            });
        });

        $(".i_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');
            var key = $this.attr('data-key');
            var value = $this.attr('data-value');
            var url = $this.attr('data-url');
            select_I(demoId,key,value,url,{});
        })


        $(".o_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');

            o_change(demoId);
        })


        function o_change(demoId) {

            //分组变动  设备变动
            formSelects.on(demoId, function(id, vals, val, isAdd, isDisabled){


                var selected = [];
                $.each(vals,function (index,value) {
                    selected.push(value['value'])

                })
                var this_value = val.value;

                if(isAdd){
                    if(selected.indexOf(this_value)  == -1){
                        selected.push(this_value)
                    }

                }else{
                    var c = selected.indexOf(this_value)
                    if( c != -1){
                        selected.splice(c,1);
                    }
                }

                var s = selected.join(",");



                var $this = $("#"+demoId);

                var datadel = $this.attr('data-del');
                var datadel =datadel.split(',');




                $.each(datadel,function (index,value) {

                    var $this = $("#"+value);
                    var demoId_key = $this.attr('id');
                    var key = $this.attr('data-key');
                    var value_key = $this.attr('data-value');
                    var url = $this.attr('data-url');

                    var data_need = $this.attr('data-need');
                    data_need = data_need.split(',');

                    var data = {};
                    $.each(data_need,function (indexs,values) {

                        if(values == demoId){

                            if(s != ''){
                                data[values] = ['in',s];
                            }

                        }
                        else{
                            var v = formSelects.value(values, 'valStr');
                            if(v != ''){
                                data[values] = ['in',formSelects.value(values, 'valStr')];
                            }

                        }


                    });



                    select_I(demoId_key,key,value_key,url,data);

                })
            });

        }

        function select_I(demo,key,value,url,data) {
            formSelects.data(demo, 'local', {
                arr: []
            });
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    // layLoad = parent.layer.load(1, {
                    //     shade: [0.5, '#000']
                    // });

                },
                success: function (result) {

                    //parent.layer.close(layLoad);

                    if (result.status != 1) {
                        // parent.layer.msg(result.message, {shift: 6});
                        return false;
                    }
                    var dataarr = result.result;

                    //  var str = '<option value="">全选</option>';
                    var arrdata = [];
                    $.each(dataarr, function (ind, val) {
                        // str += '<option value="' + val.gateway_name + '">' + val.gateway_name + '</option>';
                        // $('#gateway_name').html(str);

                        var list = {};

                        list['name'] = val[value];
                        list['value'] = val[key];
                        arrdata.push(list);


                    })

                    console.log(arrdata);
                    formSelects.data(demo, 'local', {
                        arr: arrdata
                    });
                    //  formSelects.render();
                },
                error: function (error) {
                    // parent.layer.close(layLoad);
                    layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
        }
    });
</script>
                    </div>
                </div>
            </div>
        </div>
    <?php break; default: ?>
        
        <div class="page-tab-content">
            <link rel="stylesheet" href="/static/layui-formSelects-master/dist/formSelects-v4.css">
<form class="layui-form layui-form-pane" action="<?php echo url(); ?>" method="get">
    <div class="">
        <div class="page-tab-content">


            <div class="layui-form-item">
                <label class="layui-form-label">时间范围</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="start_time" id="start_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['start_time']); ?>">
                </div>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="end_time" id="end_time" placeholder="yyyy-MM-dd HH:mm:ss" value="<?php echo htmlentities($formData['end_time']); ?>">
                </div>
                <label class="layui-form-label">监控站点</label>
                <div class="layui-input-inline">
                    <select name="monitor_id" class="field-monitor_id" type="select" lay-verify="">
                        <?php foreach($device_monitors as $key=>$value): ?>
                        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($value); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">区名</label>
                    <div class="layui-input-inline">
                        <select name="partition_type" class="partition_type i_change o_change" lay-search id="partition_type" xm-select="partition_type"
                                data-url="<?php echo url('site/DeviceMonitor/select_partition_type'); ?>" data-key="partition_type" data-value="partition_type"
                                data-del = "gateway_name,dev_name,var_name" data-need = ""
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">组名</label>
                    <div class="layui-input-inline">
                        <select name="gateway_name" class="i_change o_change" lay-search id="gateway_name" xm-select="gateway_name"
                                lay-filter="gateway" data-url="<?php echo url('site/DeviceMonitor/gateway_select'); ?>"  data-key="gateway_name" data-value="gateway_name"
                                data-del = "dev_name,var_name" data-need = "partition_type"
                        >
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">设备名</label>
                    <div class="layui-input-inline">
                        <select name="dev_name" class="i_change o_change" lay-search lay-filter="ins" xm-select="dev_name" data-key="dev_name" data-value="dev_name"
                                id="dev_name" data-url="<?php echo url('site/DeviceMonitor/instrument_select'); ?>"    data-del = "var_name" data-need = "partition_type,gateway_name">
                            <option value="">全选</option>

                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">变量名</label>
                    <div class="layui-input-inline w300">
                        <select lay-verify="" name="var_name" class="i_change" name="var_name" id="var_name" xm-select="var_name"  xm-select-radio  xm-select-search
                                data-url="<?php echo url('site/DeviceMonitor/varlist_select'); ?>" data-key="var_name" data-value="var_alias" data-need = "partition_type,gateway_name,dev_name">
                            <option value="">全选</option>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <button id="sub_btn" type="button" class="layui-btn layui-btn-normal" lay-submit="" lay-filter="*">搜索</button>
                </div>

            </div>

        </div>
    </div>
    <hr>
</form>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div class="layui-container">
    <div class="layui-row">
        <div class="layui-col-md12" id="main" style="height: 400px">

        </div>
    </div>
</div>
<!--<div id="main" style="width: 600px;height:400px;"></div>-->
<script src="/static/js/layui/layui.js?v=<?php echo config('hisiphp.version'); ?>"></script>
<script>
    var ADMIN_PATH = "<?php echo htmlentities($_SERVER['SCRIPT_NAME']); ?>", LAYUI_OFFSET = 60;
    layui.config({
    	base: '/static/system/js/',
        version: '<?php echo config("hisiphp.version"); ?>'
    }).use('global');
</script>
<script src="/static/js/jquery.2.1.4.min.js?v=2.1.4"></script>
<script src="/static/plugins/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 引入 ECharts 文件 -->
<script src="/static/plugins/echarts/echarts.min.js"></script>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    option = {};
    myChart.setOption(option);

</script>
<script type="text/javascript">
    layui.config({
        base : '/static/layui-formSelects-master/dist/'
    }).extend({
        formSelects: 'formSelects-v4'
    }).use(['formSelects','laydate','form','util'], function() {
        var $ = layui.jquery
            ,laydate = layui.laydate
            ,util = layui.util
            ,form = layui.form
            ,formSelects = layui.formSelects;

        //日期时间选择器
        laydate.render({
            elem: '#start_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        laydate.render({
            elem: '#end_time'
            ,type: 'datetime'
//            ,value: new Date()
            ,isInitValue: true
        });
        //监听提交
        form.on('submit(*)', function(data){
            /*var var_name = formSelects.value('var_name', 'valStr');
            if (var_name == '') {

                parent.layer.msg('请选择具体变量!', {shift: 6});
                return false;
            }*/
            var serializeArray = $("form").serializeArray();  // 自动获取数据

            var data = {};
            serializeArray.forEach(function (value, index) {
                data[value.name] = value.value;
            })
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>",
                data: data,
                dataType: "json",
                success: function(data){
                    if(data['code'] == 0){
                        if(data['count'] == 0){
                            layer.msg('暂无数据');
                            option = {};
                            myChart.setOption(option);
                            return false;
                        }
                        var x_data = data['x_data']?data['x_data']:[];
                        var y_data = data['y_data'];
                        var legend = data['legend'];
                        option = {
                            title: {
                                text: '统计图'
                            },
                            tooltip: {
                                trigger: 'axis'
                            },
                            legend: {
//                                data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
                                data: legend
                            },
                            grid: {
                                left: '3%',
                                right: '4%',
                                bottom: '3%',
                                containLabel: true
                            },
                            toolbox: {
                                feature: {
                                    saveAsImage: {}
                                }
                            },
                            xAxis: {
                                type: 'category',
                                boundaryGap: false,
//                                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                                data:x_data
                            },
                            yAxis: {
                                type: 'value'
                            },
                            series: y_data/*[
                                {
                                    name: '邮件营销',
                                    type: 'line',
                                    stack: '总量',
                                    data: [120, 132, 101, 134, 90, 230, 210]
                                },
                                {
                                    name: '联盟广告',
                                    type: 'line',
                                    stack: '总量',
                                    data: [220, 182, 191, 234, 290, 330, 310]
                                },
                            ]*/
                        };


                        // 使用刚指定的配置项和数据显示图表。
                        myChart.setOption(option);
                    }
                }
            });
        });

        $(".i_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');
            var key = $this.attr('data-key');
            var value = $this.attr('data-value');
            var url = $this.attr('data-url');
            select_I(demoId,key,value,url,{});
        })


        $(".o_change").each(function (index,value) {
            //console.log(this);
            var $this = $(this);
            var demoId = $this.attr('id');

            o_change(demoId);
        })


        function o_change(demoId) {

            //分组变动  设备变动
            formSelects.on(demoId, function(id, vals, val, isAdd, isDisabled){


                var selected = [];
                $.each(vals,function (index,value) {
                    selected.push(value['value'])

                })
                var this_value = val.value;

                if(isAdd){
                    if(selected.indexOf(this_value)  == -1){
                        selected.push(this_value)
                    }

                }else{
                    var c = selected.indexOf(this_value)
                    if( c != -1){
                        selected.splice(c,1);
                    }
                }

                var s = selected.join(",");



                var $this = $("#"+demoId);

                var datadel = $this.attr('data-del');
                var datadel =datadel.split(',');




                $.each(datadel,function (index,value) {

                    var $this = $("#"+value);
                    var demoId_key = $this.attr('id');
                    var key = $this.attr('data-key');
                    var value_key = $this.attr('data-value');
                    var url = $this.attr('data-url');

                    var data_need = $this.attr('data-need');
                    data_need = data_need.split(',');

                    var data = {};
                    $.each(data_need,function (indexs,values) {

                        if(values == demoId){

                            if(s != ''){
                                data[values] = ['in',s];
                            }

                        }
                        else{
                            var v = formSelects.value(values, 'valStr');
                            if(v != ''){
                                data[values] = ['in',formSelects.value(values, 'valStr')];
                            }

                        }


                    });



                    select_I(demoId_key,key,value_key,url,data);

                })
            });

        }

        function select_I(demo,key,value,url,data) {
            formSelects.data(demo, 'local', {
                arr: []
            });
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    // layLoad = parent.layer.load(1, {
                    //     shade: [0.5, '#000']
                    // });

                },
                success: function (result) {

                    //parent.layer.close(layLoad);

                    if (result.status != 1) {
                        // parent.layer.msg(result.message, {shift: 6});
                        return false;
                    }
                    var dataarr = result.result;

                    //  var str = '<option value="">全选</option>';
                    var arrdata = [];
                    $.each(dataarr, function (ind, val) {
                        // str += '<option value="' + val.gateway_name + '">' + val.gateway_name + '</option>';
                        // $('#gateway_name').html(str);

                        var list = {};

                        list['name'] = val[value];
                        list['value'] = val[key];
                        arrdata.push(list);


                    })

                    console.log(arrdata);
                    formSelects.data(demo, 'local', {
                        arr: arrdata
                    });
                    //  formSelects.render();
                },
                error: function (error) {
                    // parent.layer.close(layLoad);
                    layer.msg('系统错误：' + error.status, {shift: 6});
                }
            });
        }
    });
</script>
        </div>
<?php endswitch; if(input('param.hisi_iframe') || cookie('hisi_iframe')): ?>
</body>
</html>
<?php else: ?>
        </div>
    </div>
    <!--<div class="layui-footer footer">
        <span class="fl">Powered by <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.name'); ?></a> v<?php echo config('hisiphp.version'); ?></span>
        <span class="fr"> © 2018-2020 <a href="<?php echo config('hisiphp.url'); ?>" target="_blank"><?php echo config('hisiphp.copyright'); ?></a> All Rights Reserved.</span>
    </div>-->
</div>
</body>
</html>
<?php endif; ?>